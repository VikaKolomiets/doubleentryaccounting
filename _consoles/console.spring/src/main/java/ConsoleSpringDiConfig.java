import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.persistence.*;
import org.springframework.context.annotation.*;

@Configuration
public class ConsoleSpringDiConfig {

    @Bean
    public IXmlSerializer xmlSerializer() {
        return new XmlSerializer();
    }
    @Bean
    public IXmlStorage xmlStorage() {
        return new XmlStorage("C:\\\\\\\\Accounting\\\\ledger.xml");
    }
    @Bean
    public IXmlValidator xmlValidator() {
        return new XmlValidator();
    }
    @Bean
    public ILedgerFactory ledgerFactory() {
        return new XmlLedgerFactory(xmlStorage(), xmlValidator(), xmlSerializer());
    }
}

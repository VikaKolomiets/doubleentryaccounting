package units;

import doubleentryaccounting.common.models.enums.*;
import doubleentryaccounting.common.utils.helpers.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.persistence.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import java.math.*;
import java.time.*;
import java.util.*;

public class LedgerXmlCreator {

    //region Xml Example Data

    public final static String UahCurrency = "55bcacf2-6317-410e-ace7-e72de34a1c6c";
    public final static String UsdCurrency = "c5b2fc83-fc74-4d6b-92a1-e5c7bb59681b";
    public final static String EurCurrency = "ef66e2b9-4c35-49fa-b64d-658f28119b08";

    public final static String Mom = "e800358a-6d3a-42f7-84d6-0d0ff1f32b6f";
    public final static String Dad = "8feb8943-30c0-4c71-82c3-f93fe1543cfa";

    //endregion

    //region Main public function

    public static Document run() throws ParserConfigurationException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element root = doc.createElement(XmlTags.Ledger);
        doc.appendChild(root);

        Element projectSection = createProjectGroups(doc, root);
        createProjects(doc, projectSection);

        Element correspondentSection = createCorrespondentGroups(doc, root);
        createCorrespondents(doc, correspondentSection);

        Element categorySection = createCategoryGroups(doc, root);
        createCategories(doc, categorySection);

        Element currencySection = createCurrencies(doc, root);
        createCurrencyRates(doc, currencySection);

        Element accountSection = createAccountGroups(doc, root);
        createAccountSubGroups(doc, accountSection);
        createAccounts(doc, accountSection);

        Element templateSection = createTemplateGroups(doc, root);
        createTemplates(doc, templateSection);
        createTemplateEntries(doc, templateSection);

        Element transactionSection = createTransactions(doc, root);
        createTransactionEntries(doc, transactionSection);

        createAuxCollections(doc, root);
        createSystemConfig(doc, root);
        createUserConfig(doc, root);

        return doc;
    }

    //endregion

    //region Create Collections

    private static Element createProjectGroups(Document doc, Element root) {

        Element projectSection = doc.createElement(XmlTags.ProjectSection);
        root.appendChild(projectSection);

        projectSection.appendChild(createReferenceElement(doc, XmlTags.ProjectGroup, "14230dba-0153-4710-a322-2e33ae364820",
                "House", null, DateTimeHelper.createDateTime(2023, 1, 1, 10, 0, 0, 111), 1, true));
        projectSection.appendChild(createReferenceElement(doc, XmlTags.ProjectGroup, "42cac30c-3f1c-41e0-b504-8427c62f38ba",
                "Study", null, DateTimeHelper.createDateTime(2023, 1, 2, 10, 0, 0, 111), 3, true));
        projectSection.appendChild(createReferenceElement(doc, XmlTags.ProjectGroup, "21b3265d-aa7d-4b19-a39f-2f7fce2a8a71",
                "Health", null, DateTimeHelper.createDateTime(2023, 1, 2, 13, 10, 0, 111), 2, true));
        projectSection.appendChild(createReferenceElement(doc, XmlTags.ProjectGroup, "06ba51eb-a4b3-4bc1-9a9e-8cde15abcbb3",
                "Festival", null, DateTimeHelper.createDateTime(2023, 2, 12, 14, 5, 0, 0), 4, true));
        projectSection.appendChild(createReferenceElement(doc, XmlTags.ProjectGroup, "8e1544cb-300b-40dd-a405-ac2365877d58",
                "Other", null, DateTimeHelper.createDateTime(2023, 1, 1, 11, 5, 15, 111), 5, true));

        return projectSection;
    }

    private static void createProjects(Document doc, Element projectSection) {

        NodeList projectGroups = projectSection.getElementsByTagName(XmlTags.ProjectGroup);

        projectGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Project, "a82f5b43-f98c-44ba-ad7e-8743b7303474",
                "Apartment", "Rent, repair, equipments", DateTimeHelper.createDateTime(2022, 1, 2, 12, 0, 30, 111), 1, false));
        projectGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Project, "0479d8b2-5e68-4d01-993d-76dbee81a2a5",
                "HouseCredit", "monthly payment by credit", DateTimeHelper.createDateTime(2023, 1, 1, 12, 0, 30, 0), 2, true));
        projectGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Project, "ae496058-9dfc-4188-ad73-74a222cf198b",
                "University", "spend on education", DateTimeHelper.createDateTime(2023, 1, 2, 12, 15, 30, 111), 1, true));
        projectGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Project, "2489ab97-4f5e-4a69-ab51-150360747515",
                "ITcourses", "spend on education", DateTimeHelper.createDateTime(2023, 1, 2, 12, 15, 30, 111), 2, true));
        projectGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Project, "2d61cff8-20b9-4dc9-adb3-583d84c2e18a",
                "Pharmacy", null, DateTimeHelper.createDateTime(2023, 1, 1, 12, 15, 30, 111), 1, true));
        projectGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Project, "a7675628-e6b8-45e9-956f-a5c5202ef0ad",
                "Doctor", null, DateTimeHelper.createDateTime(2023, 1, 1, 12, 15, 30, 111), 2, true));
        projectGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Project, "2fa381f0-fe25-4397-8c37-15392b39b2a1",
                "Hospital", null, DateTimeHelper.createDateTime(2023, 1, 1, 12, 18, 30, 211), 3, true));
        projectGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Project, "8bc10d42-6f73-43e7-8240-8d3ffec333e8",
                "Vacation", null, DateTimeHelper.createDateTime(2023, 1, 1, 12, 30, 0, 111), 1, false));
        projectGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Project, "c12b2fa0-e99b-4d69-b682-5dbe4076fa21",
                "Party", "decor, presents", DateTimeHelper.createDateTime(2023, 1, 1, 12, 15, 30, 111), 2, false));
    }

    private static Element createCorrespondentGroups(Document doc, Element root) {

        Element correspondentSection = doc.createElement(XmlTags.CorrespondentSection);
        root.appendChild(correspondentSection);

        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "09f19ed5-1659-4e68-b47e-f6d4b863bb21",
                "We", "live together", DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 1, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "d4ff7639-a216-46d5-8a2e-63760f2b11eb",
                "Family", null, DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 2, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "25b7ef15-c59c-4094-9dac-4877d076140b",
                "Relatives", null, DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 6, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "70158f7f-0e15-4a41-8370-fbf8c4981647",
                "Friends", null, DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 5, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "e794d74c-b1de-459b-8bef-1fa5b7cf88b8",
                "Works", null, DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 7, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "074e7200-5883-4bcb-8451-060bfdd8a736",
                "Bank", "all animals", DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 4, true));
        correspondentSection.appendChild(createReferenceElement(doc, XmlTags.CorrespondentGroup, "42db2caa-796e-42b9-9011-a9af8a98e724",
                "Common", null, DateTimeHelper.createDateTime(2023, 1, 2, 12, 0, 30, 111), 3, true));

        return correspondentSection;
    }

    private static void createCorrespondents(Document doc, Element correspondentSection) {

        NodeList correspondentGroups = correspondentSection.getElementsByTagName(XmlTags.CorrespondentGroup);

        correspondentGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "e800358a-6d3a-42f7-84d6-0d0ff1f32b6f",
                "Mom", null, DateTimeHelper.createDateTime(2022, 10, 1, 12, 0, 30, 0), 1, false));
        correspondentGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "8feb8943-30c0-4c71-82c3-f93fe1543cfa",
                "Dad", null, DateTimeHelper.createDateTime(2022, 10, 1, 12, 0, 30, 0), 2, false));
        correspondentGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "7c7cb932-ee09-4048-a960-526fc9bd02cc",
                "Сhild", null, DateTimeHelper.createDateTime(2022, 10, 1, 12, 30, 30, 0), 1, false));
        correspondentGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "c59fac23-9b24-4278-8a0b-41f19de538a9",
                "grmLuba", null, DateTimeHelper.createDateTime(2022, 10, 1, 10, 30, 30, 0), 2, false));
        correspondentGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "8104a11d-1ce2-4959-a4dd-54a2a8884032",
                "grmKrava", null, DateTimeHelper.createDateTime(2023, 1, 1, 10, 0, 30, 0), 3, false));
        correspondentGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "11841ed0-5237-4736-94ba-b3303c0a4d85",
                "Vasya", null, DateTimeHelper.createDateTime(2023, 2, 1, 10, 0, 30, 0), 1, false));
        correspondentGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "1cc4bc8c-59ae-4933-a89d-0de0dfb19404",
                "Dasha", null, DateTimeHelper.createDateTime(2023, 1, 4, 10, 0, 30, 111), 2, false));
        correspondentGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "cde00b99-f1a6-4061-ae6f-13da767a60fe",
                "Sasha", null, DateTimeHelper.createDateTime(2023, 1, 1, 10, 0, 30, 0), 3, false));
        correspondentGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "db88bdcb-c350-4e89-b5de-5a861e8ae39d",
                "Oleg", null, DateTimeHelper.createDateTime(2023, 1, 4, 10, 0, 30, 0), 1, false));
        correspondentGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "7c71bdb3-ec39-416f-8743-ddf4a360ab0d",
                "Nicolya", null, DateTimeHelper.createDateTime(2023, 1, 5, 10, 0, 30, 0), 2, false));
        correspondentGroups.item(4).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "3b0c6076-db33-4d63-b24b-04afbae93352",
                "MyCompany", null, DateTimeHelper.createDateTime(2023, 1, 5, 10, 0, 30, 0), 1, false));
        correspondentGroups.item(4).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "636d9e81-5951-4fe8-bf52-3db26e90e784",
                "Freelince", null, DateTimeHelper.createDateTime(2023, 1, 6, 10, 0, 30, 111), 2, false));
        correspondentGroups.item(6).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "41393bd1-dd67-4735-9394-3f2424973575",
                "Aval", null, DateTimeHelper.createDateTime(2022, 12, 31, 10, 0, 30, 0), 1, false));
        correspondentGroups.item(6).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "1fbb532c-6634-4b11-a3de-90562016aff1",
                "Private", null, DateTimeHelper.createDateTime(2022, 12, 31, 10, 0, 30, 0), 2, false));
        correspondentGroups.item(6).appendChild(createReferenceElement(doc, XmlTags.Correspondent, "797c4236-b07d-4c72-895d-0b2a4ff61ca6",
                "Animals", null, DateTimeHelper.createDateTime(2022, 12, 31, 10, 0, 30, 0), 3, false));
    }

    private static Element createCategoryGroups(Document doc, Element root) {

        Element categorySection = doc.createElement(XmlTags.CategorySection);
        root.appendChild(categorySection);

        categorySection.appendChild(createReferenceElement(doc, XmlTags.CategoryGroup, "27fa9b1d-ba76-47dd-bcad-885c1bcd9c8e",
                "Goods", "life support", DateTimeHelper.createDateTime(2023, 1, 1, 12, 20, 20, 123), 1, false));
        categorySection.appendChild(createReferenceElement(doc, XmlTags.CategoryGroup, "bbfdb6ac-f6d1-4a51-aea0-32acef567e57",
                "Services", "price more than 15$", DateTimeHelper.createDateTime(2023, 1, 1, 12, 20, 20, 654), 2, false));
        categorySection.appendChild(createReferenceElement(doc, XmlTags.CategoryGroup, "67b80f52-6496-4e93-b5ae-35f26b6c2a3f",
                "Others", "consumables", DateTimeHelper.createDateTime(2023, 1, 1, 12, 20, 20, 17), 3, true));

        return categorySection;
    }

    private static void createCategories(Document doc, Element categorySection) {

        NodeList categoryGroups = categorySection.getElementsByTagName(XmlTags.CategoryGroup);

        categoryGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Category, "7a3ec88c-3084-4cc1-b519-4a491996de32",
                "Food", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 11, 211), 1, true));
        categoryGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Category, "3d4ade51-fb65-459a-8850-dc5a0c1d4ba0",
                "Clothes", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 12, 311), 2, false));
        categoryGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Category, "77d74297-2f11-48c8-a0d7-b6cdaffdd315",
                "Things", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 13, 141), 3, true));
        categoryGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Category, "ad3555fc-93e9-4843-a1fb-4df9855fb8fa",
                "BigThings", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 14, 189), 4, false));

        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "3ae34795-4f4c-4d22-a4f7-ed31db78ab7a",
                "Rent", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 101), 1, true));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "57c9dd71-b84a-435b-bf8f-6a28c4cb65f1",
                "Health", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 110), 2, true));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "7417a52c-573f-49c2-b3ca-ec0014807f1b",
                "Fun", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 112), 3, false));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "20a32c00-6bb1-4bc3-bc28-69181293b9db",
                "Study", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 121), 4, true));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "6721b3af-c197-4d85-97ec-1f332a8e35f5",
                "Care", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 113), 5, true));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "b9d32d00-60ec-4d59-adaf-d35266934396",
                "Transport", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 131), 6, false));
        categoryGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Category, "4ab2083c-1228-40cb-bfbe-f56c3fbc47d4",
                "Common", null, DateTimeHelper.createDateTime(2022, 1, 1, 12, 10, 18, 114), 7, false));

        categoryGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Category, "39ed8296-2389-496b-b29c-1f9b50fd301c",
                "Charity", null, DateTimeHelper.createDateTime(2022, 1, 1, 10, 10, 10, 128), 1, false));
        categoryGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Category, "95bd785b-6de4-48db-bb17-fd610a033956",
                "Present", null, DateTimeHelper.createDateTime(2022, 1, 1, 10, 15, 10, 135), 2, false));
        categoryGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Category, "6ca3dd9a-7995-45a8-bcf4-c39a9bcb8c66",
                "WriteOff", null, DateTimeHelper.createDateTime(2022, 1, 1, 10, 25, 10, 451), 3, true));
        categoryGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Category, "75b11429-4884-4832-8716-fce65d6a6424",
                "Other", null, DateTimeHelper.createDateTime(2022, 1, 1, 10, 35, 10, 161), 4, false));
    }

    private static Element createCurrencies(Document doc, Element root) {

        Element currencySection = doc.createElement(XmlTags.CurrencySection);
        root.appendChild(currencySection);

        currencySection.appendChild(createCurrency(doc, "55bcacf2-6317-410e-ace7-e72de34a1c6c",
                "UAH", "₴", DateTimeHelper.createDateTime(2023, 1, 1, 12, 0, 0, 120), 1, true));
        currencySection.appendChild(createCurrency(doc, "c5b2fc83-fc74-4d6b-92a1-e5c7bb59681b",
                "USD", "$", DateTimeHelper.createDateTime(2023, 1, 1, 12, 1, 0, 30), 2, true));
        currencySection.appendChild(createCurrency(doc, "ef66e2b9-4c35-49fa-b64d-658f28119b08",
                "EUR", "€", DateTimeHelper.createDateTime(2023, 1, 1, 12, 2, 0, 2), 3, true));
        currencySection.appendChild(createCurrency(doc, "2bf8bd6e-3c55-4964-b18f-6b393762314c",
                "GBP", "£", DateTimeHelper.createDateTime(2023, 1, 1, 12, 3, 0, 4), 4, false));

        return currencySection;
    }

    private static void createCurrencyRates(Document doc, Element currencySection) {

        NodeList currencies = currencySection.getElementsByTagName(XmlTags.Currency);

        currencies.item(1).appendChild(createRateCurrency(doc, "23670853-25a5-490b-8d47-763ff84b0c73",
                new BigDecimal("36.8200"), "USD from UAH", DateTimeHelper.createDateTime(2023, 1, 1, 12, 2, 0, 800), DateHelper.createDate(2023, 2, 1)));
        currencies.item(1).appendChild(createRateCurrency(doc, "8cd92039-cd35-4e31-80c6-0ca40cf802bb",
                new BigDecimal("36.8200"), "USD from UAH", DateTimeHelper.createDateTime(2023, 2, 1, 12, 2, 0, 25), DateHelper.createDate(2023, 2, 1)));
        currencies.item(2).appendChild(createRateCurrency(doc, "69e28e88-7464-4606-afbb-2dfcd2ffbb26",
                new BigDecimal("38.5100"), "EUR from UAH", DateTimeHelper.createDateTime(2023, 1, 1, 12, 2, 0, 21), DateHelper.createDate(2023, 2, 1)));
        currencies.item(2).appendChild(createRateCurrency(doc, "71607fb1-9ddf-4514-8f6f-6a48192be9a9",
                new BigDecimal("38.3900"), "EUR from UAH", DateTimeHelper.createDateTime(2023, 2, 1, 12, 2, 0, 15), DateHelper.createDate(2023, 2, 1)));
    }

    private static Element createAccountGroups(Document doc, Element root) {

        Element accountSection = doc.createElement(XmlTags.AccountSection);
        root.appendChild(accountSection);

        accountSection.appendChild(createReferenceElement(doc, XmlTags.AccountGroup, "64e26c6c-abd1-47e0-b2d7-f0b33daf1dbd",
                "Cash/Card", "operational data", DateTimeHelper.createDateTime(2022, 12, 20, 12, 10, 11, 258), 1, false));
        accountSection.appendChild(createReferenceElement(doc, XmlTags.AccountGroup, "3f48281a-9f2d-4470-87a4-c3e6c644cfe8",
                "Asset", "longterm money", DateTimeHelper.createDateTime(2022, 12, 20, 12, 15, 11, 250), 2, false));
        accountSection.appendChild(createReferenceElement(doc, XmlTags.AccountGroup, "af0ca938-8e53-4705-b9b8-c39b61353caa",
                "Debts", "money owed to/from others", DateTimeHelper.createDateTime(2022, 12, 20, 12, 20, 21, 158), 3, false));
        accountSection.appendChild(createReferenceElement(doc, XmlTags.AccountGroup, "87988560-2e4c-4f2b-895a-3aae1fa319e4",
                "Income/Outcome", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 30, 21, 158), 4, false));

        return accountSection;
    }

    private static void createAccountSubGroups(Document doc, Element accountSection) {

        NodeList accountGroups = accountSection.getElementsByTagName(XmlTags.AccountGroup);

        accountGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "bad603ba-89c4-4548-aa63-19e66b7c4c55",
                "Life", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 10, 15, 258), 1, true));
        accountGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "ef6cf3d4-0e7a-4f2e-8995-458b6006685d",
                "Health", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 11, 15, 258), 2, true));
        accountGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "04b508e7-06ba-4ea7-b282-d54504d656cd",
                "Study", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 12, 15, 258), 3, true));
        accountGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "074dac29-c58a-4cd7-bd90-829434a88033",
                "Service", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 13, 15, 258), 4, true));
        accountGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "34b23c50-9399-4073-956b-e3fe6ae914c1",
                "Other", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 14, 15, 258), 5, true));

        accountGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "8aae08e5-e9a6-4e7f-b56b-22ab0b95f5ac",
                "USD", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 20, 15, 258), 1, false));
        accountGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "14e734be-80ee-4adf-b0a8-79713e0b271e",
                "EUR", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 21, 15, 258), 2, false));
        accountGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "ff86e0b4-8a76-445f-883b-d0b53b22414b",
                "Other", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 22, 15, 258), 3, false));

        accountGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "ab43d696-2e0b-468f-9bdb-826cef87f650",
                "Person", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 33, 15, 258), 1, false));
        accountGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "4ea9ed64-daf8-4c35-8597-33b695e1bd82",
                "Legal", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 34, 15, 258), 2, false));

        accountGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "f01173bf-0710-48d6-8806-98cd8a67138c",
                "Cash", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 33, 15, 258), 1, true));
        accountGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "2e35a804-cc8c-4b45-8f86-f880a469dc17",
                "Card", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 34, 15, 258), 2, true));
        accountGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.AccountSubGroup, "db52f37e-82df-4866-9193-7c399302d4bd",
                "Work", null, DateTimeHelper.createDateTime(2022, 12, 20, 12, 35, 25, 250), 3, false));
    }

    private static void createAccounts(Document doc, Element accountSection) {

        ArrayList<Element> list = new ArrayList<>();

        NodeList accountGroups = accountSection.getElementsByTagName(XmlTags.AccountGroup);
        for (int i = 0; i < accountGroups.getLength(); i++) {
            Element accountGroup = (Element) accountGroups.item(i);
            NodeList accountSubGroups = accountGroup.getElementsByTagName(XmlTags.AccountSubGroup);
            for (int j = 0; j < accountSubGroups.getLength(); j++) {
                list.add((Element) accountSubGroups.item(j));
            }
        }

        list.get(0).appendChild(createAccount(doc, "2090834e-2bd7-41b7-a310-c14f8623f821",
                "Food", null, DateTimeHelper.createDateTime(2022, 12, 30, 12, 35, 25, 250), 1, UahCurrency,
                "7a3ec88c-3084-4cc1-b519-4a491996de32", null, null, true));
        list.get(0).appendChild(createAccount(doc, "9d2f433b-1aa1-4ddd-9bac-11d49348fde2",
                "Things", null, DateTimeHelper.createDateTime(2022, 11, 30, 12, 35, 15, 250), 2, UahCurrency,
                "77d74297-2f11-48c8-a0d7-b6cdaffdd315", null, null, true));
        list.get(1).appendChild(createAccount(doc, "a09054e7-554d-4451-9f7b-5b672893dd9c",
                "Doctor", null, DateTimeHelper.createDateTime(2022, 12, 29, 10, 5, 25, 260), 1, UahCurrency,
                "57c9dd71-b84a-435b-bf8f-6a28c4cb65f1", "c59fac23-9b24-4278-8a0b-41f19de538a9", null, true));
        list.get(2).appendChild(createAccount(doc, "3ca754b1-052b-4046-af06-e1d084a78386",
                "Swithing", null, DateTimeHelper.createDateTime(2023, 1, 20, 10, 5, 20, 160), 1, UahCurrency,
                "20a32c00-6bb1-4bc3-bc28-69181293b9db", Mom, "2489ab97-4f5e-4a69-ab51-150360747515", false));
        list.get(2).appendChild(createAccount(doc, "ebc2696f-1dfa-4350-a42b-46d4dadf6666",
                "UniverSon", null, DateTimeHelper.createDateTime(2022, 10, 15, 10, 5, 20, 0), 2, EurCurrency,
                "20a32c00-6bb1-4bc3-bc28-69181293b9db", "7c7cb932-ee09-4048-a960-526fc9bd02cc", "ae496058-9dfc-4188-ad73-74a222cf198b", false));
        list.get(5).appendChild(createAccount(doc, "aab9c906-61a4-4cf7-92fa-8ab8b478843b",
                "CellBank", null, DateTimeHelper.createDateTime(2022, 10, 15, 10, 5, 20, 0), 1, UsdCurrency,
                null, Dad, null, true));
        list.get(6).appendChild(createAccount(doc, "7069d840-9134-4c77-b92a-b9b9224829d8",
                "CellBank", null, DateTimeHelper.createDateTime(2022, 10, 15, 10, 15, 20, 111), 1, EurCurrency,
                null, Dad, null, true));
        list.get(8).appendChild(createAccount(doc, "1a6166e4-07ec-4cd0-8375-8cf56079038a",
                "Loan", null, DateTimeHelper.createDateTime(2022, 11, 15, 10, 15, 21, 121), 1, UsdCurrency,
                null, "1cc4bc8c-59ae-4933-a89d-0de0dfb19404", null, false));
        list.get(9).appendChild(createAccount(doc, "1cf5723d-00ac-45ca-9432-73f7c9851307",
                "CeditWork", null, DateTimeHelper.createDateTime(2022, 11, 15, 10, 15, 21, 121), 1, UahCurrency,
                null, "3b0c6076-db33-4d63-b24b-04afbae93352", null, false));
        list.get(9).appendChild(createAccount(doc, "fbaccc8f-0865-4942-993f-f671ef9789a0",
                "Tax", null, DateTimeHelper.createDateTime(2022, 11, 15, 10, 15, 21, 321), 2, UahCurrency,
                null, null, null, false));
        list.get(10).appendChild(createAccount(doc, "484d8b3e-9e22-459f-817f-25e38c74b0b4",
                "Dad", "CashDad", DateTimeHelper.createDateTime(2023, 1, 5, 12, 11, 21, 121), 1, UahCurrency,
                null, Dad, null, false));
        list.get(10).appendChild(createAccount(doc, "9297ff93-3a8d-40f9-b0c8-70cccf816fcb",
                "Mom", "CashMom", DateTimeHelper.createDateTime(2023, 1, 5, 12, 11, 21, 121), 2, UahCurrency,
                null, Mom, null, false));
        list.get(10).appendChild(createAccount(doc, "a03b1414-5785-44dc-8418-a87cb58cd7d6",
                "Dad", "CardDad", DateTimeHelper.createDateTime(2023, 1, 5, 12, 11, 21, 121), 3, UahCurrency,
                null, Dad, null, false));
        list.get(10).appendChild(createAccount(doc, "e2986e12-986f-470a-95b9-4fc9ca72abcc",
                "Mom", "CardMom", DateTimeHelper.createDateTime(2023, 1, 5, 12, 11, 21, 121), 4, UahCurrency,
                null, Mom, null, false));
        list.get(10).appendChild(createAccount(doc, "37fd6d89-66f7-4c89-b055-65cdbcb38433",
                "USD", "CashUSD", DateTimeHelper.createDateTime(2023, 1, 7, 12, 11, 21, 121), 5, UsdCurrency,
                null, null, null, false));
        list.get(10).appendChild(createAccount(doc, "40a5f7fa-809a-4dd4-9d8b-21e0e6f25a6d",
                "EUR", "CashEUR", DateTimeHelper.createDateTime(2023, 1, 8, 12, 11, 21, 121), 6, EurCurrency,
                null, null, null, false));
        list.get(11).appendChild(createAccount(doc, "31dad22b-d966-41da-8f63-9e96e15f740c",
                "Charity", null, DateTimeHelper.createDateTime(2023, 1, 5, 12, 11, 21, 121), 1, UahCurrency,
                "39ed8296-2389-496b-b29c-1f9b50fd301c", null, null, false));
        list.get(12).appendChild(createAccount(doc, "60ced6f6-647a-4823-b983-6aa4d63b5e15",
                "SalaryDad", null, DateTimeHelper.createDateTime(2022, 12, 5, 11, 11, 21, 121), 1, UahCurrency,
                null, Dad, null, false));
        list.get(12).appendChild(createAccount(doc, "d5f87136-715a-4bd6-a44d-2a1cc536fce0",
                "SalaryMom", null, DateTimeHelper.createDateTime(2022, 12, 5, 12, 11, 21, 121), 2, UahCurrency
                , null, Mom, null, false));
    }

    private static Element createTemplateGroups(Document doc, Element root) {

        Element templateSection = doc.createElement(XmlTags.TemplateSection);
        root.appendChild(templateSection);

        templateSection.appendChild(createReferenceElement(doc, XmlTags.TemplateGroup, "ffdddaf6-6fdc-4372-abf9-8ae61dd1bb42",
                "Dayly", null, DateTimeHelper.createDateTime(2022, 12, 2, 12, 0, 30, 111), 1, true));
        templateSection.appendChild(createReferenceElement(doc, XmlTags.TemplateGroup, "177bdb95-7aa7-41a6-9883-148eaa553ba6",
                "Weekly", null, DateTimeHelper.createDateTime(2022, 12, 2, 12, 10, 30, 122), 2, true));
        templateSection.appendChild(createReferenceElement(doc, XmlTags.TemplateGroup, "99036f36-1fdb-4ded-bfc6-1729c89fd7fa",
                "Quarterly", null, DateTimeHelper.createDateTime(2022, 12, 2, 12, 20, 30, 231), 3, true));
        templateSection.appendChild(createReferenceElement(doc, XmlTags.TemplateGroup, "527d8dd3-aa5f-40de-ae90-ec4ec3b92f3c",
                "Yearly", null, DateTimeHelper.createDateTime(2022, 12, 2, 12, 30, 30, 114), 4, true));
        templateSection.appendChild(createReferenceElement(doc, XmlTags.TemplateGroup, "527d8dd3-aa5f-40de-ae90-ec4ec3b92f3c",
                "Monthly", null, DateTimeHelper.createDateTime(2022, 12, 2, 12, 30, 30, 114), 4, true));

        return templateSection;
    }

    private static void createTemplates(Document doc, Element templateSection) {

        NodeList templateGroups = templateSection.getElementsByTagName(XmlTags.TemplateGroup);

        templateGroups.item(0).appendChild(createReferenceElement(doc, XmlTags.Template, "09032494-0b08-42e1-aebb-32da8548e275",
                "Ones", "goods by way", DateTimeHelper.createDateTime(2022, 12, 2, 10, 30, 30, 104), 1, true));
        templateGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Template, "d16b256c-6d72-402b-93fc-472262343b2f",
                "MarketMom", "goods on market", DateTimeHelper.createDateTime(2022, 12, 2, 12, 30, 30, 114), 1, true));
        templateGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Template, "7e36525c-27d5-4d88-8c04-9fddff0bcb00",
                "MarketDad", "goods on market", DateTimeHelper.createDateTime(2022, 12, 2, 12, 30, 30, 114), 2, true));
        templateGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Template, "6869c1ad-fcf0-4a5c-9992-3fb071c39588",
                "ShopCard", "goods in supermarket", DateTimeHelper.createDateTime(2022, 12, 2, 11, 35, 38, 184), 3, true));
        templateGroups.item(1).appendChild(createReferenceElement(doc, XmlTags.Template, "c358e5cc-206f-4df2-81c5-9acc4039b79e",
                "ShopCash", "goods in supermarket", DateTimeHelper.createDateTime(2022, 12, 2, 11, 35, 38, 222), 4, true));
        templateGroups.item(2).appendChild(createReferenceElement(doc, XmlTags.Template, "b206aac4-414c-4fcc-a051-f31f65b41710",
                "Tax", null, DateTimeHelper.createDateTime(2022, 12, 3, 13, 35, 38, 184), 1, false));
        templateGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Template, "04c3535b-fc0d-4112-9bab-912dadad7c56",
                "BuyUSD", null, DateTimeHelper.createDateTime(2023, 1, 7, 9, 5, 30, 128), 1, false));
        templateGroups.item(3).appendChild(createReferenceElement(doc, XmlTags.Template, "b4606aa6-84d4-47f3-8c29-d71ad9d5091a",
                "BuyEUR", null, DateTimeHelper.createDateTime(2023, 1, 7, 9, 5, 30, 128), 1, false));
    }

    private static void createTemplateEntries(Document doc, Element templateSection) {

        ArrayList<Element> list = new ArrayList<>();

        NodeList templateGroups = templateSection.getElementsByTagName(XmlTags.TemplateGroup);
        for (int i = 0; i < templateGroups.getLength(); i++) {
            Element templateGroup = (Element) templateGroups.item(i);
            NodeList templates = templateGroup.getElementsByTagName(XmlTags.Template);
            for (int j = 0; j < templates.getLength(); j++) {
                list.add((Element) templates.item(j));
            }
        }

        list.get(0).appendChild(createTemplateEntry(doc, "b844afe9-3efb-46ce-8b2b-064098f752a2", "9d2f433b-1aa1-4ddd-9bac-11d49348fde2", new BigDecimal("0")));
        list.get(0).appendChild(createTemplateEntry(doc, "185cf60c-ee4d-40c9-82ce-b66861645801", "31dad22b-d966-41da-8f63-9e96e15f740c", new BigDecimal("0")));

        list.get(1).appendChild(createTemplateEntry(doc, "01f5a776-2469-4f0e-8343-bd70d5ef9c73", "484d8b3e-9e22-459f-817f-25e38c74b0b4", new BigDecimal("0")));
        list.get(1).appendChild(createTemplateEntry(doc, "a018aa5a-0188-4f1a-b1cb-9ddd36806d58", "2090834e-2bd7-41b7-a310-c14f8623f821", new BigDecimal("0")));

        list.get(2).appendChild(createTemplateEntry(doc, "a610f420-872c-4f28-9b27-ebb34065c340", "9297ff93-3a8d-40f9-b0c8-70cccf816fcb", new BigDecimal("0")));
        list.get(2).appendChild(createTemplateEntry(doc, "ad0e9ba5-c2c5-404f-9efa-041d63cb29ac", "2090834e-2bd7-41b7-a310-c14f8623f821", new BigDecimal("0")));

        list.get(3).appendChild(createTemplateEntry(doc, "7ae19a01-bb5e-48f5-a4c2-dce2ea0fdbe6", "e2986e12-986f-470a-95b9-4fc9ca72abcc", new BigDecimal("0")));
        list.get(3).appendChild(createTemplateEntry(doc, "0ea9c7dc-3c52-4105-8302-7da6abb7bd55", "2090834e-2bd7-41b7-a310-c14f8623f821", new BigDecimal("0")));

        list.get(4).appendChild(createTemplateEntry(doc, "68bdb97a-eda4-4134-b6b3-c1329367ae04", "a03b1414-5785-44dc-8418-a87cb58cd7d6", new BigDecimal("0")));
        list.get(4).appendChild(createTemplateEntry(doc, "05b43344-49a4-4701-b2f0-de5a7bb6b0af", "2090834e-2bd7-41b7-a310-c14f8623f821", new BigDecimal("0")));
        list.get(4).appendChild(createTemplateEntry(doc, "b0b419ac-b2e9-44a6-868e-491a94d97020", "9d2f433b-1aa1-4ddd-9bac-11d49348fde2", new BigDecimal("0")));
        list.get(4).appendChild(createTemplateEntry(doc, "8c3d6a6b-9c11-4c61-825a-7cc9f0f6b74a", "31dad22b-d966-41da-8f63-9e96e15f740c", new BigDecimal("0")));

        list.get(5).appendChild(createTemplateEntry(doc, "9d5c2435-0078-4440-a5ac-3591a3d9f02f", "a03b1414-5785-44dc-8418-a87cb58cd7d6", new BigDecimal("-150.00")));
        list.get(5).appendChild(createTemplateEntry(doc, "88cb9836-993e-4f5e-aede-b381f249dd09", "fbaccc8f-0865-4942-993f-f671ef9789a0", new BigDecimal("5000.00")));

        list.get(6).appendChild(createTemplateEntry(doc, "a07e24fc-2163-41ed-9def-a2a2f52fc3ef", "484d8b3e-9e22-459f-817f-25e38c74b0b4", new BigDecimal("0")));
        list.get(6).appendChild(createTemplateEntry(doc, "a4e51b94-48d5-41e2-9903-cce4304c6199", "37fd6d89-66f7-4c89-b055-65cdbcb38433", new BigDecimal("0")));

        list.get(7).appendChild(createTemplateEntry(doc, "a347cf3a-afb1-4be3-be5f-8738ea57249c", "484d8b3e-9e22-459f-817f-25e38c74b0b4", new BigDecimal("0")));
        list.get(7).appendChild(createTemplateEntry(doc, "222e0185-dc20-4e81-b134-1f3748963043", "40a5f7fa-809a-4dd4-9d8b-21e0e6f25a6d", new BigDecimal("0")));
    }

    private static Element createTransactions(Document doc, Element root) {

        Element transactionSection = doc.createElement(XmlTags.TransactionSection);
        root.appendChild(transactionSection);

        transactionSection.appendChild(createTransaction(doc, "ee63649e-cda3-45a4-8563-15b941ed0e52", DateTimeHelper.createDateTime(2022, 12, 1, 1, 30, 30, 111),
                DateTimeHelper.createDateTime(2022, 12, 31, 12, 33, 33), TransactionState.Planned, "who"));
        transactionSection.appendChild(createTransaction(doc, "17de78ca-30ad-4342-b8e9-6019612b3281", DateTimeHelper.createDateTime(2022, 12, 1, 2, 40, 10, 222)
                , DateTimeHelper.createDateTime(2023, 1, 2, 11, 30, 31), TransactionState.Draft, "what"));
        transactionSection.appendChild(createTransaction(doc, "9fb308d6-eea6-458d-afec-94e438fa67af", DateTimeHelper.createDateTime(2022, 12, 1, 2, 40, 10, 333)
                , DateTimeHelper.createDateTime(2023, 1, 3, 10, 35, 33), TransactionState.Confirmed, "why"));
        transactionSection.appendChild(createTransaction(doc, "22df725e-4f48-4fc7-9487-c47bd3406643", DateTimeHelper.createDateTime(2022, 12, 1, 2, 40, 10, 444)
                , DateTimeHelper.createDateTime(2023, 1, 4, 9, 33, 34), TransactionState.Confirmed, "best"));
        transactionSection.appendChild(createTransaction(doc, "22df725e-4f48-4fc7-9487-c47bd3406643", DateTimeHelper.createDateTime(2022, 12, 2, 2, 40, 10, 555)
                , DateTimeHelper.createDateTime(2023, 2, 6, 9, 33, 34), TransactionState.Confirmed, null));
        transactionSection.appendChild(createTransaction(doc, "105b856f-ea87-44f5-bf74-5ee81dd830a3", DateTimeHelper.createDateTime(2023, 1, 1, 2, 40, 10, 888)
                , DateTimeHelper.createDateTime(2023, 2, 5, 9, 39, 25), TransactionState.Confirmed, "west"));
        transactionSection.appendChild(createTransaction(doc, "808ca85d-21e4-44f0-af85-48e0913cb9a4", DateTimeHelper.createDateTime(2023, 2, 7, 2, 40, 10, 777)
                , DateTimeHelper.createDateTime(2023, 2, 7, 9, 39, 25), TransactionState.NoValid, null));
        return transactionSection;
    }

    public static void createTransactionEntries(Document doc, Element transactionSection) {

        NodeList transactions = transactionSection.getElementsByTagName(XmlTags.Transaction);

        transactions.item(0).appendChild(createTransactionEntry(doc, "e54c6b9b-7953-445c-961d-78a5d72666f4", "484d8b3e-9e22-459f-817f-25e38c74b0b4",
                new BigDecimal("-250.02"), new BigDecimal("1")));
        transactions.item(0).appendChild(createTransactionEntry(doc, "2cd3c60c-174e-4eef-a3cc-049d7185d317", "2090834e-2bd7-41b7-a310-c14f8623f821",
                new BigDecimal("250.02"), new BigDecimal("1")));
        transactions.item(1).appendChild(createTransactionEntry(doc, "1fb529e5-6870-43e5-bf20-1a93e3fe45bc", "9297ff93-3a8d-40f9-b0c8-70cccf816fcb",
                new BigDecimal("-21.35"), new BigDecimal("1")));
        transactions.item(1).appendChild(createTransactionEntry(doc, "99bf9062-1d10-414e-a514-326fd46dfb92", "2090834e-2bd7-41b7-a310-c14f8623f821",
                new BigDecimal("21.35"), new BigDecimal("1")));
        transactions.item(2).appendChild(createTransactionEntry(doc, "1fa79540-a371-45b6-85e1-c5bfdb331210", "e2986e12-986f-470a-95b9-4fc9ca72abcc",
                new BigDecimal("-2158.36"), new BigDecimal("1")));
        transactions.item(2).appendChild(createTransactionEntry(doc, "77daa38a-6a21-4abf-ac07-9d010c583e26", "9d2f433b-1aa1-4ddd-9bac-11d49348fde2",
                new BigDecimal("58.36"), new BigDecimal("1")));
        transactions.item(2).appendChild(createTransactionEntry(doc, "01dd9d92-347c-4e2c-8c94-78ec52773c2b", "31dad22b-d966-41da-8f63-9e96e15f740c",
                new BigDecimal("109.50"), new BigDecimal("1")));
        transactions.item(2).appendChild(createTransactionEntry(doc, "121414f8-2031-48ac-93e6-54eb51d0f968", "2090834e-2bd7-41b7-a310-c14f8623f821",
                new BigDecimal("1990.50"), new BigDecimal("1")));
        transactions.item(3).appendChild(createTransactionEntry(doc, "007c9339-0e7c-433f-8795-578f4032a196", "a03b1414-5785-44dc-8418-a87cb58cd7d6",
                new BigDecimal("-15947.25"), new BigDecimal("1")));
        transactions.item(3).appendChild(createTransactionEntry(doc, "0d202963-2b0e-44ca-9c37-34688eba6386", "fbaccc8f-0865-4942-993f-f671ef9789a0",
                new BigDecimal("15947.25"), new BigDecimal("1")));
        transactions.item(4).appendChild(createTransactionEntry(doc, "2536e03e-0c44-4385-8c3f-6c7bedcacc00", "484d8b3e-9e22-459f-817f-25e38c74b0b4",
                new BigDecimal("-18410.00"), new BigDecimal("1")));
        transactions.item(4).appendChild(createTransactionEntry(doc, "59a5d6c0-6d3d-40b9-996e-d90ddf0dfc4a", "37fd6d89-66f7-4c89-b055-65cdbcb38433",
                new BigDecimal("500.00"), new BigDecimal("36.82")));
        transactions.item(5).appendChild(createTransactionEntry(doc, "3e1edc4a-9c43-44f5-ab3c-154e448e6d34", "484d8b3e-9e22-459f-817f-25e38c74b0b4",
                new BigDecimal("-19255.00"), new BigDecimal("1")));
        transactions.item(5).appendChild(createTransactionEntry(doc, "b0837f47-6831-4733-91fb-8111e458c781", "40a5f7fa-809a-4dd4-9d8b-21e0e6f25a6d",
                new BigDecimal("500.00"), new BigDecimal("38.51")));
    }

    private static void createAuxCollections(Document doc, Element root) {
        Element addedIdsSection = doc.createElement(XmlTags.AddedIds);
        root.appendChild(addedIdsSection);

        Element updatedIdsSection = doc.createElement(XmlTags.UpdatedIds);
        root.appendChild(updatedIdsSection);

        Element deletedIdsSection = doc.createElement(XmlTags.DeletedIds);
        root.appendChild(deletedIdsSection);
    }

    private static void createSystemConfig(Document doc, Element root) {
        Element systemConfigElement = doc.createElement(XmlTags.SystemConfig);
        root.appendChild(systemConfigElement);

        Element mainCurrencyElement = doc.createElement(XmlTags.MainCurrency);
        mainCurrencyElement.setTextContent("UAH");
        systemConfigElement.appendChild(mainCurrencyElement);

        Element minDateTimeElement = doc.createElement(XmlTags.MinDateTime);
        LocalDateTime minDateTime = DateTimeHelper.createDateTime(1970, 1, 1, 0, 0, 0);
        minDateTimeElement.setTextContent(DateTimeHelper.convertToString(minDateTime));
        systemConfigElement.appendChild(minDateTimeElement);

        Element maxDateTimeElement = doc.createElement(XmlTags.MaxDateTime);
        LocalDateTime maxDateTime = DateTimeHelper.createDateTime(2099, 12, 31, 23, 59, 59);
        maxDateTimeElement.setTextContent(DateTimeHelper.convertToString(maxDateTime));
        systemConfigElement.appendChild(maxDateTimeElement);
    }

    private static void createUserConfig(Document doc, Element root) {
        Element userConfigElement = doc.createElement(XmlTags.UserConfig);
        root.appendChild(userConfigElement);
    }

    //endregion

    //region Create Elements

    private static Element createReferenceElement(
            Document doc,
            String elementName,
            String id,
            String name,
            String description,
            LocalDateTime timeStamp,
            int order,
            boolean isFavorite) {
        Element element = doc.createElement(elementName);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.Name, name));
        if (!StringHelper.isStringNullOrEmpty(description)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Description, description));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, DateTimeHelper.convertToString(timeStamp)));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(order)));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(isFavorite)));

        return element;
    }

    private static Element createCurrency(
            Document doc,
            String id,
            String isoCode,
            String symbol,
            LocalDateTime timeStamp,
            int order,
            boolean isFavorite) {
        Element element = doc.createElement(XmlTags.Currency);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsoCode, isoCode));
        element.setAttributeNode(createAttribute(doc, XmlTags.Symbol, symbol));
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, DateTimeHelper.convertToString(timeStamp)));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(order)));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(isFavorite)));
        return element;
    }

    private static Element createRateCurrency(
            Document doc,
            String id,
            BigDecimal rate,
            String comment,
            LocalDateTime timeStamp,
            LocalDate date) {
        Element element = doc.createElement(XmlTags.CurrencyRate);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.Rate, BigDecimalHelper.convertToString(rate)));
        if (!StringHelper.isStringNullOrEmpty(comment)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Comment, comment));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, DateTimeHelper.convertToString(timeStamp)));
        element.setAttributeNode(createAttribute(doc, XmlTags.Date, DateHelper.convertToString(date)));
        return element;
    }

    private static Element createAccount(
            Document doc,
            String id,
            String name,
            String description,
            LocalDateTime timeStamp,
            int order,
            String currencyId,
            String categoryId,
            String correspondentId,
            String projectId,
            boolean isFavorite) {
        Element element = doc.createElement(XmlTags.Account);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.Name, name));
        if (!StringHelper.isStringNullOrEmpty(description)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Description, description));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, DateTimeHelper.convertToString(timeStamp)));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(order)));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(isFavorite)));
        element.setAttributeNode(createAttribute(doc, XmlTags.CurrencyId, currencyId));

        if (!StringHelper.isStringNullOrEmpty(correspondentId)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.CorrespondentId, correspondentId));
        }
        if (!StringHelper.isStringNullOrEmpty(categoryId)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.CategoryId, categoryId));
        }
        if (!StringHelper.isStringNullOrEmpty(projectId)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.ProjectId, projectId));
        }
        return element;
    }

    private static Element createTransaction(
            Document doc,
            String id,
            LocalDateTime timeStamp,
            LocalDateTime localDateTime,
            TransactionState state,
            String comment) {
        Element element = doc.createElement(XmlTags.Transaction);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, DateTimeHelper.convertToString(timeStamp)));
        element.setAttributeNode(createAttribute(doc, XmlTags.DateTime, DateTimeHelper.convertToString(localDateTime)));
        element.setAttributeNode(createAttribute(doc, XmlTags.State, String.valueOf(state.toString())));
        if (!StringHelper.isStringNullOrEmpty(comment)) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Comment, comment));
        }
        return element;
    }

    private static Element createTransactionEntry(Document doc, String id, String accountId, BigDecimal amount, BigDecimal rate) {
        Element element = doc.createElement(XmlTags.TransactionEntry);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.AccountId, accountId));
        element.setAttributeNode(createAttribute(doc, XmlTags.Amount, BigDecimalHelper.convertToString(amount)));
        element.setAttributeNode(createAttribute(doc, XmlTags.Rate, BigDecimalHelper.convertToString(rate)));
        return element;
    }

    private static Element createTemplateEntry(Document doc, String id, String accountId, BigDecimal amount) {
        Element element = doc.createElement(XmlTags.TemplateEntry);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, id));
        element.setAttributeNode(createAttribute(doc, XmlTags.AccountId, accountId));
        element.setAttributeNode(createAttribute(doc, XmlTags.Amount, BigDecimalHelper.convertToString(amount)));
        return element;
    }

    private static Attr createAttribute(Document doc, String name, String value) {
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        return attr;
    }

    //endregion
}

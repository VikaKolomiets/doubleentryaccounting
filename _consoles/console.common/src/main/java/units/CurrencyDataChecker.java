package units;

import doubleentryaccounting.common.utils.currency.*;

import java.util.*;

public class CurrencyDataChecker {

    public static void run() {

        ICurrencyDataCollection collection = new CurrencyDataCollection();
        ArrayList<CurrencyData> data = collection.getAvailableCurrencyData();
        for (CurrencyData cur : data) {
            System.out.println(cur.getCode() + " : " + cur.getSymbol() + " : " + cur.getName());
        }
    }

    private static void currenciesRun() {
        Set<Currency> availableCurrencies = Currency.getAvailableCurrencies();
        for (Currency currency :  availableCurrencies) {
            String currencyCode = currency.getCurrencyCode();
            int fractionDigits = currency.getDefaultFractionDigits();
            String displayName = currency.getDisplayName();
            int numericCode = currency.getNumericCode();
            String codeAsString = currency.getNumericCodeAsString();
            String symbol = currency.getSymbol();
            System.out.println(currencyCode + ", " + fractionDigits + ", " + displayName + ", " + numericCode + ", " + codeAsString + ", " + symbol);
        }
    }
}

import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.persistence.*;
import org.w3c.dom.*;
import units.*;

public class ConsoleCommonEntryPoint {
    public static void main(String[] args) {

        //CurrencyDataChecker.run();
        f1();
    }

    private static void f1() {
        final String filePathA = "C:\\\\Accounting\\ledgerA.xml";
        final String filePathB = "C:\\\\Accounting\\ledgerB.xml";

        try {
            IXmlSerializer serializer = new XmlSerializer();
            IXmlStorage storage1 = new XmlStorage(filePathA);
            IXmlStorage storage2 = new XmlStorage(filePathB);

            Document doc1 = LedgerXmlCreator.run();
            storage1.save(doc1);

            Document doc2 = storage1.load();
            Ledger ledger = serializer.deserialize(doc2);

            Document doc3 = serializer.serialize(ledger);
            storage2.save(doc3);

            int n = 0;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.out.println("Finish");
    }
}

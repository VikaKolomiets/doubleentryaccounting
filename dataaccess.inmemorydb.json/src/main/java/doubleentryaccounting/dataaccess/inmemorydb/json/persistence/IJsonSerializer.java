package doubleentryaccounting.dataaccess.inmemorydb.json.persistence;

import doubleentryaccounting.dataaccess.inmemorydb.*;
import org.w3c.dom.Document;

public interface IJsonSerializer {
    Document serialize(Ledger ledger);
    Ledger deserialize(Document document);

}

package doubleentryaccounting.dataaccess.inmemorydb.json.persistence;

import org.w3c.dom.*;

public interface IJsonStorage {
    Document load();
    void save(Document doc);

}

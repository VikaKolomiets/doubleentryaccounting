package doubleentryaccounting.dataaccess.inmemorydb.json.persistence;

import org.w3c.dom.*;

public interface IJsonValidator {
    void Validate(Document doc);
}

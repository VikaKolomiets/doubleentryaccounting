package doubleentryaccounting.common.services;

import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.services.base.*;

import java.util.*;

public interface ICurrencyService extends IOrderedEntityService, IFavoriteEntityService {
    UUID add(CurrencyInfo info);
    void update(CurrencyInfo info);
    void delete(String isoCode);
}

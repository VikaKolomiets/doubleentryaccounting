package doubleentryaccounting.common.services;

import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.base.*;

import java.util.*;

public interface ICurrencyRateService extends IEntityService<CurrencyRate, CurrencyRateInfo> {

    void deleteRateList(ArrayList<UUID> transactionIds);
}

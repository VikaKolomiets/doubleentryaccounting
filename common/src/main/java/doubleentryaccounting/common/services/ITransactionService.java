package doubleentryaccounting.common.services;

import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.base.*;

import java.util.*;

public interface ITransactionService extends IEntityService<Transaction, TransactionInfo> {
    void deleteTransactionList(ArrayList<UUID> transactionIds);
}

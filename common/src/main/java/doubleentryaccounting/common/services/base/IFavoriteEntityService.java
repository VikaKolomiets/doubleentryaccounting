package doubleentryaccounting.common.services.base;

import java.util.*;

public interface IFavoriteEntityService {
    void setFavoriteStatus(UUID entityId, boolean isFavorite);
}

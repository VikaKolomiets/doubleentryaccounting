package doubleentryaccounting.common.services.base;

import java.util.*;

public interface ICombinedEntityService {
    void combineTwoEntities(UUID primaryId, UUID secondaryId);
}

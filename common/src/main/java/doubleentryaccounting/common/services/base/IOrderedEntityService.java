package doubleentryaccounting.common.services.base;

import java.util.*;

public interface IOrderedEntityService {
    void setOrder(UUID entityId, int order);
}

package doubleentryaccounting.common.services.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IReferenceChildEntityService<T extends IEntity, TInfo> extends IReferenceEntityService<T, TInfo>, IChildEntityService {
}

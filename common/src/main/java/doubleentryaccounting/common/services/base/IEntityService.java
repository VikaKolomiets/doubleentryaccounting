package doubleentryaccounting.common.services.base;

import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.utils.interfaces.*;

import java.util.*;

public interface IEntityService<T extends IEntity, TInfo> extends IClassProvider<T> {
    UUID add(TInfo info);
    void update(UUID entityId, TInfo info);
    void delete(UUID entityId);
}

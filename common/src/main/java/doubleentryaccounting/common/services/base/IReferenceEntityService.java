package doubleentryaccounting.common.services.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IReferenceEntityService<T extends IEntity, TInfo > extends IEntityService<T, TInfo>, IOrderedEntityService, IFavoriteEntityService {
}

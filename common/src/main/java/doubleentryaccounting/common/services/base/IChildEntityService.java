package doubleentryaccounting.common.services.base;

import java.util.*;

public interface IChildEntityService {
    void moveToAnotherParent(UUID entityId, UUID parentId);
}

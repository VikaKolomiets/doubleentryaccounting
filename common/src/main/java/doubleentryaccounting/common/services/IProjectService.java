package doubleentryaccounting.common.services;

import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.base.*;

public interface IProjectService extends IReferenceChildEntityService<Project, ElementEntityInfo>, ICombinedEntityService {
}

package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.models.interfaces.*;

import java.util.*;

public interface IGlobalDataAccess {
    void save();
    IEntity getEntity(UUID id);
}

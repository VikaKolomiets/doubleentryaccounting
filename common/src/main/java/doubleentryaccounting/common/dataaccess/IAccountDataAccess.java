package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

import java.util.*;

public interface IAccountDataAccess
        extends IReferenceChildEntityDataAccess<Account> {
    void loadCurrency(Account account);
    ArrayList<Account> getAccountsByCorrespondent(Correspondent correspondent);
    ArrayList<Account> getAccountsByCategory(Category category);
    ArrayList<Account> getAccountsByProject(Project project);
}


package doubleentryaccounting.common.dataaccess.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IReferenceParentEntityDataAccess <T extends IEntity & INamedEntity>
        extends IEntityDataAccess<T>, IParentEntityDataAccess<T>{
}

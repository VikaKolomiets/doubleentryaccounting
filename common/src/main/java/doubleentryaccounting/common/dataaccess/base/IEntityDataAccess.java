package doubleentryaccounting.common.dataaccess.base;

import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.utils.interfaces.*;

import java.util.*;

public interface IEntityDataAccess<T extends IEntity> extends IClassProvider<T> {
    T get(UUID id);

    void add(T entity);
    void addList(ArrayList<T> list);

    void update(T entity);
    void updateList(ArrayList<T> list);

    void delete(T entity);
    void deleteList(ArrayList<T> list);
}

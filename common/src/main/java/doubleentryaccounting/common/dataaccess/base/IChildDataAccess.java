package doubleentryaccounting.common.dataaccess.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IChildDataAccess<T extends IEntity>  {
    void loadParent(T entity);
}

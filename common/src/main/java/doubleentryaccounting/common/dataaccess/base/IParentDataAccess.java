package doubleentryaccounting.common.dataaccess.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IParentDataAccess<T extends IEntity>  {
    void loadChildren(T entity);
}

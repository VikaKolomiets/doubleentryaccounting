package doubleentryaccounting.common.dataaccess.base;

import doubleentryaccounting.common.models.interfaces.*;

public interface IReferenceChildEntityDataAccess<T extends IEntity & INamedEntity>
        extends IEntityDataAccess<T>, IChildEntityDataAccess<T>  {
}

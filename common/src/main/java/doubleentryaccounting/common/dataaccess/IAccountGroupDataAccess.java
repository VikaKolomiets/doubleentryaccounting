package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

public interface IAccountGroupDataAccess
        extends IReferenceParentEntityDataAccess<AccountGroup> {
}

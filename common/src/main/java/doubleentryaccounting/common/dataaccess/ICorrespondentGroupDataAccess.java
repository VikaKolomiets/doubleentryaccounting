package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

public interface ICorrespondentGroupDataAccess
        extends IReferenceParentEntityDataAccess<CorrespondentGroup> {
}

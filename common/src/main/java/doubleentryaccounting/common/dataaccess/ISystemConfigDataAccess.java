package doubleentryaccounting.common.dataaccess;

import java.time.*;

public interface ISystemConfigDataAccess {
    String getMainCurrencyIsoCode();
    LocalDateTime getMinDateTime();
    LocalDateTime getMaxDateTime();
}

package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

import java.util.*;

public interface ITemplateDataAccess
        extends IReferenceChildEntityDataAccess<Template> {
    ArrayList<TemplateEntry> getEntriesByAccount(Account account);
    int getTemplateEntriesCount(UUID accountId);
}

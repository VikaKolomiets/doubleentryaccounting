package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

public interface IAccountSubGroupDataAccess
        extends IReferenceChildEntityDataAccess<AccountSubGroup>, IParentDataAccess<AccountSubGroup>
{
}

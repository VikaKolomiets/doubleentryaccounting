package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.Currency;

import java.util.*;

public interface ICurrencyDataAccess
        extends IEntityDataAccess<Currency> {

    ArrayList<Currency> getByIsoCode(String isoCode);
    int getMaxOrder();
    int getCount();
    ArrayList<Currency> getList();
}

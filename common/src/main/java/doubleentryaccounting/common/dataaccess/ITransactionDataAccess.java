package doubleentryaccounting.common.dataaccess;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.*;

import java.util.*;

public interface ITransactionDataAccess
        extends IEntityDataAccess<Transaction> {
    ArrayList<TransactionEntry> getEntriesByAccount(Account account);
    int getTransactionEntriesCount(UUID accountId);
}

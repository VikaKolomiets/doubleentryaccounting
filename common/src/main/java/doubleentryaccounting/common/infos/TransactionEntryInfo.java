package doubleentryaccounting.common.infos;

import java.math.*;
import java.util.*;

public class TransactionEntryInfo {

    private UUID accountId;
    private BigDecimal amount;
    private BigDecimal rate;

    public TransactionEntryInfo() {
    }

    public TransactionEntryInfo(UUID accountId, BigDecimal amount, BigDecimal rate) {

        this.accountId = accountId;
        this.amount = amount;
        this.rate = rate;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}

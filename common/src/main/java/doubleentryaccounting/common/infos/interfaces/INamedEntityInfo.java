package doubleentryaccounting.common.infos.interfaces;

public interface INamedEntityInfo {
    String getName();
    String getDescription();
    void setName(String name);
    void setDescription(String description);
}

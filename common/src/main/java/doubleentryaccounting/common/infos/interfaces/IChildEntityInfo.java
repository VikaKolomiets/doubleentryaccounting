package doubleentryaccounting.common.infos.interfaces;

import java.util.*;

public interface IChildEntityInfo {
    UUID getParentId();
    void setParentId(UUID parentId);
}

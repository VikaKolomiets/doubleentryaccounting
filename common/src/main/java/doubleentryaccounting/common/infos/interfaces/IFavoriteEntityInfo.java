package doubleentryaccounting.common.infos.interfaces;

public interface IFavoriteEntityInfo {
    boolean getIsFavorite();
    void setIsFavorite(boolean isFavorite);
}

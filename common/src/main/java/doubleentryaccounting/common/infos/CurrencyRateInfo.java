package doubleentryaccounting.common.infos;

import java.math.*;
import java.time.*;
import java.util.*;

public class CurrencyRateInfo  {

    private UUID  currencyId;
    private LocalDate date;
    private BigDecimal rate;
    private String comment;

    public CurrencyRateInfo() {
    }

    public CurrencyRateInfo(UUID currencyId, LocalDate date, BigDecimal rate) {
        this(currencyId, date, rate, null);
    }

    public CurrencyRateInfo(UUID currencyId, LocalDate date, BigDecimal rate, String comment) {
        this.currencyId = currencyId;
        this.date = date;
        this.rate = rate;
        this.comment = comment;
    }

    public UUID getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(UUID currencyId) {
        this.currencyId = currencyId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

package doubleentryaccounting.common.infos;

import doubleentryaccounting.common.infos.interfaces.*;

import java.util.*;

public class AccountInfo implements INamedEntityInfo, IFavoriteEntityInfo, IChildEntityInfo {

    private UUID parentId;
    private String name;
    private UUID currencyId;
    private UUID categoryId;
    private UUID correspondentId;
    private UUID projectId;
    private String description;
    private boolean isFavorite;

    public AccountInfo() {
    }

    public AccountInfo(UUID parentId, String name, UUID currencyId) {
        this(parentId, name, currencyId, null, null, null, null, false);
    }

    public AccountInfo(UUID parentId, String name, UUID currencyId,
                       UUID categoryId, UUID correspondentId, UUID projectId, String description, boolean isFavorite) {
        this.parentId = parentId;
        this.name = name;
        this.currencyId = currencyId;
        this.categoryId = categoryId;
        this.correspondentId = correspondentId;
        this.projectId = projectId;
        this.description = description;
        this.isFavorite = isFavorite;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(UUID currencyId) {
        this.currencyId = currencyId;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public UUID getCorrespondentId() {
        return correspondentId;
    }

    public void setCorrespondentId(UUID correspondentId) {
        this.correspondentId = correspondentId;
    }

    public UUID getProjectId() {
        return projectId;
    }

    public void setProjectId(UUID projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
}

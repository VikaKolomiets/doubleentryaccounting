package doubleentryaccounting.common.infos;

import doubleentryaccounting.common.infos.interfaces.*;

import java.util.*;

public class TemplateInfo  implements INamedEntityInfo, IFavoriteEntityInfo, IChildEntityInfo {

    private UUID parentId;
    private String name;
    private String description;
    private boolean isFavorite;
    private ArrayList<TemplateEntryInfo> entries;

    public TemplateInfo() {
    }

    public TemplateInfo(UUID parentId, String name, ArrayList<TemplateEntryInfo> templateEntryInfos) {
        this(parentId, name, null, false, templateEntryInfos);
    }

    public TemplateInfo(UUID parentId, String name, String description, boolean isFavorite, ArrayList<TemplateEntryInfo> templateEntryInfos) {
        this.parentId = parentId;
        this.name = name;
        this.description = description;
        this.isFavorite = isFavorite;
        this.entries = templateEntryInfos;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public ArrayList<TemplateEntryInfo> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<TemplateEntryInfo> entries) {
        this.entries = entries;
    }
}

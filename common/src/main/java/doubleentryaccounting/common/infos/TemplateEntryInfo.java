package doubleentryaccounting.common.infos;

import java.math.*;
import java.util.*;

public class TemplateEntryInfo {

    private UUID accountId;
    private BigDecimal amount;

    public TemplateEntryInfo() {
    }

    public TemplateEntryInfo(UUID accountId, BigDecimal amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

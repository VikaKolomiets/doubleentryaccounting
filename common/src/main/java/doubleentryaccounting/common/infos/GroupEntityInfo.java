package doubleentryaccounting.common.infos;

import doubleentryaccounting.common.infos.interfaces.*;

public class GroupEntityInfo implements INamedEntityInfo, IFavoriteEntityInfo {

    private String name;
    private String description;
    private boolean isFavorite;

    public GroupEntityInfo() {
    }

    public GroupEntityInfo(String name) {
        this(name, null, false);
    }

    public GroupEntityInfo(String name, String description, boolean isFavorite) {
        this.name = name;
        this.description = description;
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
}

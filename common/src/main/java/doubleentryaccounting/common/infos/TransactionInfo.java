package doubleentryaccounting.common.infos;

import doubleentryaccounting.common.models.enums.*;

import java.time.*;
import java.util.*;

public class TransactionInfo{

    private LocalDateTime dateTime;
    private TransactionState state;
    private String comment;
    private List<TransactionEntryInfo> entries;

    public TransactionInfo() {
    }

    public TransactionInfo(LocalDateTime dateTime, TransactionState state, List<TransactionEntryInfo> entries) {
        this(dateTime, state, null, entries);
    }

    public TransactionInfo(LocalDateTime dateTime, TransactionState state, String comment, List<TransactionEntryInfo> entries) {
        this.dateTime = dateTime;
        this.state = state;
        this.entries = entries;
        this.comment = comment;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public TransactionState getState() {
        return state;
    }

    public void setState(TransactionState state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<TransactionEntryInfo> getEntries() {
        return entries;
    }

    public void setEntries(List<TransactionEntryInfo> entries) {
        this.entries = entries;
    }
}

package doubleentryaccounting.common.infos;

import doubleentryaccounting.common.infos.interfaces.*;

import java.util.*;

public class ElementEntityInfo implements INamedEntityInfo, IFavoriteEntityInfo, IChildEntityInfo {

    private UUID parentId;
    private String name;
    private String description;
    private boolean isFavorite;

    public ElementEntityInfo() {
    }

    public ElementEntityInfo(UUID parentId, String name) {
        this(parentId, name, null, false);
    }

    public ElementEntityInfo(UUID parentId, String name, String description, boolean isFavorite) {
        this.parentId = parentId;
        this.name = name;
        this.description = description;
        this.isFavorite = isFavorite;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
}

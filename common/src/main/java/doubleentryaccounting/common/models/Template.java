package doubleentryaccounting.common.models;

import doubleentryaccounting.common.models.base.*;

import java.util.*;

public class Template extends ReferenceChildEntity<TemplateGroup> {

    private final ArrayList<TemplateEntry> entries = new ArrayList<>();

    public ArrayList<TemplateEntry> getEntries() {
        return entries;
    }
}

package doubleentryaccounting.common.models;

import doubleentryaccounting.common.models.base.*;
import doubleentryaccounting.common.models.interfaces.*;

import java.util.*;

public class Currency extends Entity implements ITrackedEntity, IFavoriteEntity, IOrderedEntity {

    private String timeStamp;
    private String isoCode;
    private String symbol;
    private String name;
    private int order;
    private boolean isFavorite;
    private final ArrayList<CurrencyRate> rates = new ArrayList<>();

    public String getTimeStamp() {
        return this.timeStamp;
    }
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIsoCode() {
        return this.isoCode;
    }
    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getSymbol() {
        return this.symbol;
    }
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return this.order;
    }
    public void setOrder(int order) {
        this.order = order;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }
    public void setIsFavorite(boolean favorite) {
        this.isFavorite = favorite;
    }

    public ArrayList<CurrencyRate> getRates() {
        return this.rates;
    }
}

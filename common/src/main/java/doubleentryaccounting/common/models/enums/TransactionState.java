package doubleentryaccounting.common.models.enums;

public enum TransactionState {
    NoValid,
    Draft,
    Planned,
    Confirmed
}

package doubleentryaccounting.common.models.base;

import doubleentryaccounting.common.models.interfaces.*;

public abstract class ReferenceChildEntity<T> extends ReferenceEntity implements IReferenceChildEntity<T> {

    private T parent;

    public T getParent() {
        return this.parent;
    }
    public void setParent(T value) {
        this.parent = value;
    }
}

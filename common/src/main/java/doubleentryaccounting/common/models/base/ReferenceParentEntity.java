package doubleentryaccounting.common.models.base;

import doubleentryaccounting.common.models.interfaces.*;

import java.util.*;

public abstract class ReferenceParentEntity <T> extends ReferenceEntity implements IReferenceParentEntity<T> {

    private final ArrayList<T> children = new ArrayList<>();

    public ArrayList<T> getChildren() {
        return this.children;
    }
}

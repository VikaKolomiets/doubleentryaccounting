package doubleentryaccounting.common.models.config;

import java.time.*;

public class SystemConfig {

    private String mainCurrencyIsoCode;
    private LocalDateTime minDateTime;
    private LocalDateTime maxDateTime;

    public String getMainCurrencyIsoCode() {
        return mainCurrencyIsoCode;
    }
    public void setMainCurrencyIsoCode(String mainCurrencyIsoCode) {
        this.mainCurrencyIsoCode = mainCurrencyIsoCode;
    }

    public LocalDateTime getMinDateTime() {
        return minDateTime;
    }
    public void setMinDateTime(LocalDateTime minDate) {
        this.minDateTime = minDate;
    }

    public LocalDateTime getMaxDateTime() {
        return maxDateTime;
    }
    public void setMaxDateTime(LocalDateTime maxDate) {
        this.maxDateTime = maxDate;
    }
}

package doubleentryaccounting.common.utils.validation;

public enum ValidationErrorType {
    None,
    MissedId,
    DuplicatedId,
    EmptyName,
    DuplicatedName,
    WrongOrders,
}

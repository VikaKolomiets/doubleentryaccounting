package doubleentryaccounting.common.utils.validation;

import doubleentryaccounting.common.models.interfaces.*;

import java.util.*;

public class ValidationError {
    private final ValidationErrorType type;
    private final UUID id;
    private final String entityTypeName;

    public ValidationError(ValidationErrorType type, Class<? extends IEntity> entityType, UUID id) {
        this.type = type;
        this.entityTypeName = entityType.getSimpleName();
        this.id = id;
    }

    public ValidationErrorType getType() {
        return type;
    }

    public String getEntityTypeName() {
        return entityTypeName;
    }

    public UUID getId() {
        return id;
    }
}

package doubleentryaccounting.common.utils.interfaces;

public interface IClassProvider<T> {
    Class<T> getType();
}

package doubleentryaccounting.common.utils.tuples;

import java.io.*;

public class Tuple2<T1, T2> implements Serializable {
    @Serial
    private static final long serialVersionUID = -1036387688559959103L;
    private final T1 value1;
    private final T2 value2;

    public Tuple2(final T1 value1, final T2 value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public static <T1, T2> Tuple2<T1, T2> create(final T1 value1, final T2 value2) {
        return new Tuple2<T1, T2>(value1, value2);
    }

    public T1 getValue1() {
        return value1;
    }

    public T2 getValue2() {
        return value2;
    }

    
    public String toString() {
        return "Pair{" + "1: " + value1 + ", 2: " + value2 + '}';
    }
}

package doubleentryaccounting.common.utils.tuples;

import java.io.*;

public class Tuple3<T1, T2, T3> implements Serializable {
    @Serial
    private static final long serialVersionUID = -6680803833894375373L;
    private final T1 value1;
    private final T2 value2;
    private final T3 value3;

    public Tuple3(final T1 value1, final T2 value2, final T3 value3) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }

    public static <T1, T2, T3> Tuple3<T1, T2, T3> create(final T1 value1, final T2 value2, final T3 value3) {
        return new Tuple3<T1, T2, T3>(value1, value2, value3);
    }

    public T1 getValue1() {
        return value1;
    }

    public T2 getValue2() {
        return value2;
    }

    public T3 getValue3() {
        return value3;
    }

    
    public String toString() {
        return "Tuple3 {" + "1: " + value1 + ", 2: " + value2 + ", 3: " + value3 + '}';
    }
}

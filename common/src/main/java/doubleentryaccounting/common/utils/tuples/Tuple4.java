package doubleentryaccounting.common.utils.tuples;

import java.io.*;

public class Tuple4<T1, T2, T3, T4> implements Serializable {

    private final T1 value1;
    private final T2 value2;
    private final T3 value3;
    private final T4 value4;

    public Tuple4(final T1 value1, final T2 value2, final T3 value3, final T4 value4) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public static <T1, T2, T3, T4> Tuple4<T1, T2, T3, T4> create(final T1 value1, final T2 value2, final T3 value3, final T4 value4) {
        return new Tuple4<T1, T2, T3, T4>(value1, value2, value3, value4);
    }

    public T1 getValue1() {
        return value1;
    }

    public T2 getValue2() {
        return value2;
    }

    public T3 getValue3() {
        return value3;
    }

    public T4 getValue4() {
        return value4;
    }

    public String toString() {
        return "Tuple3 {" + "1: " + value1 + ", 2: " + value2 + ", 3: " + value3 + ", 4: " + value4 + '}';
    }
}


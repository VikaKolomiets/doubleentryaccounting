package doubleentryaccounting.common.utils.helpers;

import java.time.*;
import java.time.format.*;

public class DateHelper {

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
    public static final ZoneId CURRENT_ZONE_ID = ZoneId.systemDefault();

    public static  String convertToString(LocalDate date) {
        return date.format(DATE_FORMATTER);
    }

    public static String convertToString() {
        LocalDate time = LocalDate.now(UTC_ZONE_ID);
        return time.format(DATE_FORMATTER);
    }

    public static LocalDate convertFromString(String stamp) {
        return LocalDate.parse(stamp, DATE_FORMATTER);
    }

    public static LocalDate createDate(int year, int month, int day) {
        return LocalDate.of(year, month, day);
    }
}

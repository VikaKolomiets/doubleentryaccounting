package doubleentryaccounting.common.utils.helpers;

public class StringHelper {
    public static boolean isStringNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }
}

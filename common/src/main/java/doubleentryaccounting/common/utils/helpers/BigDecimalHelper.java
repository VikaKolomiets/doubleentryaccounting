package doubleentryaccounting.common.utils.helpers;

import java.math.*;
import java.text.*;
import java.util.*;

public class BigDecimalHelper {

    private static DecimalFormat format  = new DecimalFormat("###0.0000",  new DecimalFormatSymbols(Locale.US));

    public static String convertToString(BigDecimal d) {
        return format.format(d);
    }

    public static BigDecimal convertFromString(String s)  {
        format.setParseBigDecimal(true);
        try {
            return (BigDecimal)format.parseObject(s);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

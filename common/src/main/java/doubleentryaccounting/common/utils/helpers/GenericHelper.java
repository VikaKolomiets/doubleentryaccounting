package doubleentryaccounting.common.utils.helpers;

import java.lang.reflect.*;

public class GenericHelper {

//    public static <T> Class<T> getGenericType(T outerInstance) {
//        return getGenericType(outerInstance, 0);
//    }
//
//    public static <T> Class<T> getGenericType(T outerInstance, int typeIndex) {
//        Type outerGenericType = outerInstance.getClass().getGenericSuperclass();
//        ParameterizedType outerParameterizedType = (ParameterizedType) outerGenericType;
//        Class<T> innerType = (Class) outerParameterizedType.getActualTypeArguments()[typeIndex];
//        return innerType;
//    }

    public static <T> T createGenericType(Class<T> type) {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException(type.getSimpleName() + " cannot instantiate");
        }
    }
}

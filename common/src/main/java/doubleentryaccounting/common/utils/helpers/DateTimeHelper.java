package doubleentryaccounting.common.utils.helpers;

import java.time.*;
import java.time.format.*;

public class DateTimeHelper {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
    public static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
    public static final ZoneId CURRENT_ZONE_ID = ZoneId.systemDefault();

    public static String convertToString(LocalDateTime dateTime) {
        return dateTime.format(DATE_TIME_FORMATTER);
    }

    public static String convertToString() {
        LocalDateTime dateTime = LocalDateTime.now(UTC_ZONE_ID);
        return convertToString(dateTime);
    }

    public static LocalDateTime convertFromString(String stamp) {
        return LocalDateTime.parse(stamp, DATE_TIME_FORMATTER);
    }

    public static LocalDateTime createDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        return LocalDateTime.of(year, month, day, hour, minute, second, millisecond * 1000000);
    }

    public static LocalDateTime createDateTime(int year, int month, int day, int hour, int minute, int second) {
        return LocalDateTime.of(year, month, day, hour, minute, second);
    }
}

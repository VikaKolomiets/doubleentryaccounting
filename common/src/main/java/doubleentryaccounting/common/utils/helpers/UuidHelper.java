package doubleentryaccounting.common.utils.helpers;

import java.util.*;

public class UuidHelper {

    public static String convertToString(UUID uuid) {
        return uuid.toString();
    }

    public static UUID convertFromString(String s) {
        return UUID.fromString(s);
    }
}

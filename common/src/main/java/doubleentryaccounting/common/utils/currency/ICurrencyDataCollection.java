package doubleentryaccounting.common.utils.currency;

import java.util.*;

public interface ICurrencyDataCollection {
    ArrayList<CurrencyData> getAvailableCurrencyData();
}

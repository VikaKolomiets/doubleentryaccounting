package doubleentryaccounting.common.utils.currency;

import java.util.*;
import java.util.stream.*;

public class CurrencyDataCollection implements ICurrencyDataCollection {
    public ArrayList<CurrencyData> getAvailableCurrencyData() {

        Locale[] availableLocales = Locale.getAvailableLocales();

        ArrayList<CurrencyData> totalList = getTotalListOfCurrencies(availableLocales);
        ArrayList<List<CurrencyData>> groupList = new ArrayList<>(totalList.stream().collect(Collectors.groupingBy(CurrencyData::getCode)).values());
        ArrayList<CurrencyData> list = getUniqueListOfCurrencies(groupList);

        list.sort(Comparator.comparing(CurrencyData::getCode));
        return list;
    }

    private static ArrayList<CurrencyData> getTotalListOfCurrencies(Locale[] availableLocales) {

        ArrayList<CurrencyData> totalList = new ArrayList<>();

        for (Locale locale : availableLocales) {

            Currency currency;
            try {
                currency = Currency.getInstance(locale);
            } catch (IllegalArgumentException e) {
                continue;
            }

            String code = currency.getCurrencyCode();
            String symbol = getSymbol(currency, locale, code);
            String name = currency.getDisplayName();

            CurrencyData curInfo = new CurrencyData(code, symbol, name);
            totalList.add(curInfo);
        }
        return totalList;
    }

    private static String getSymbol(Currency currency, Locale locale, String code) {
        String symbol = currency.getSymbol(locale);
        if(symbol.charAt(symbol.length() - 1) == '.'){
            symbol = symbol.substring(0, symbol.length() - 1);
        }
        if(symbol.length() > 3){
            symbol = code;
        }
        return symbol;
    }

    private static ArrayList<CurrencyData> getUniqueListOfCurrencies(ArrayList<List<CurrencyData>> groupList) {
        ArrayList<CurrencyData> list = new ArrayList<>();

        for (List<CurrencyData> group : groupList) {
            CurrencyData currencyData = group.get(0);
            if(group.size() == 1){
                list.add(currencyData);
                continue;
            }
            for (int i = 1; i < group.size(); i++){
                CurrencyData other = group.get(i);
                if(currencyData.getSymbol().length() > other.getSymbol().length()){
                    currencyData = other;
                    continue;
                }
                if(currencyData.getSymbol().length() < other.getSymbol().length()){
                    continue;
                }
                for (int j = 0; j < currencyData.getSymbol().length(); j++) {
                    char a = currencyData.getSymbol().charAt(j);
                    char b = other.getSymbol().charAt(j);
                    if(Character.isLowerCase(a) && Character.isUpperCase(b))
                    {
                        break;
                    }
                    if(Character.isUpperCase(a) && Character.isLowerCase(b))
                    {
                        currencyData = other;
                        break;
                    }
                }
            }
            list.add(currencyData);
        }
        return list;
    }
}

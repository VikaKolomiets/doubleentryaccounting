package doubleentryaccounting.common.utils.orderedentity;

import doubleentryaccounting.common.models.interfaces.*;

import java.util.*;

public class OrderedEntityComparator implements Comparator<IOrderedEntity> {
    
    public int compare(IOrderedEntity o1, IOrderedEntity o2) {
        return o1.getOrder() - o2.getOrder();
    }
}

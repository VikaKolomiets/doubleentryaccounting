package doubleentryaccounting.business.webapi;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.currency.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.comparison.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.*;
import doubleentryaccounting.dataaccess.inmemorydb.validation.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.*;
import doubleentryaccounting.dataaccess.inmemorydb.xml.persistence.*;
import org.springframework.context.annotation.*;

import java.time.*;

@Configuration
public class BusinessWebApiDiConfig {

    //region Implementation
    private LocalDateTime time;
    public BusinessWebApiDiConfig() {
        this.time = LocalDateTime.now();
    }
    //endregion

    //region Mode;s Utils
    @Bean
    public ICurrencyDataCollection currencyDataCollection() {
        return new CurrencyDataCollection();
    }
    //endregion

    //region Data Access XML
    @Bean
    public IXmlSerializer xmlSerializer() {
        return new XmlSerializer();
    }
    @Bean
    public IXmlStorage xmlStorage() {
        return new XmlStorage("C:\\\\\\\\Accounting\\\\ledger.xml");
    }
    @Bean
    public IXmlValidator xmlValidator() {
        return new XmlValidator();
    }
    @Bean
    public ILedgerFactory ledgerFactory() {
        return new XmlLedgerFactory(xmlStorage(), xmlValidator(), xmlSerializer());
    }
    //endregion

    //region Data Access Utils
    @Bean
    public ILedgerValidator ledgerValidator() {
        return new LedgerValidator();
    }
    @Bean
    public ILedgerComparer ldgerComparer() {
        return new LedgerComparer();
    }
    //endregion

    //region Data Access MemoryDb
    @Bean
    public IGlobalDataAccess globalDataAccess() {
        return new MemoryDbGlobalDataAccess(ledgerFactory());
    }
    @Bean
    public ICategoryGroupDataAccess categoryGroupDataAccess() {
        return new MemoryDbCategoryGroupDataAccess(ledgerFactory());
    }
    @Bean
    public ICategoryDataAccess categoryDataAccess() {
        return new MemoryDbCategoryDataAccess(ledgerFactory());
    }
    @Bean
    public ICorrespondentGroupDataAccess correspondentGroupDataAccess() {
        return new MemoryDbCorrespondentGroupDataAccess(ledgerFactory());
    }
    @Bean
    public ICorrespondentDataAccess correspondentDataAccess() {
        return new MemoryDbCorrespondentDataAccess(ledgerFactory());
    }
    @Bean
    public IProjectGroupDataAccess projectGroupDataAccess() {
        return new MemoryDbProjectGroupDataAccess(ledgerFactory());
    }
    @Bean
    public IProjectDataAccess projectDataAccess() {
        return new MemoryDbProjectDataAccess(ledgerFactory());
    }
    @Bean
    public ICurrencyDataAccess currencyDataAccess() {
        return new MemoryDbCurrencyDataAccess(ledgerFactory());
    }
    @Bean
    public ICurrencyRateDataAccess currencyRateDataAccess() {
        return new MemoryDbCurrencyRateDataAccess(ledgerFactory());
    }
    @Bean
    public IAccountGroupDataAccess accountGroupDataAccess() {
        return new MemoryDbAccountGroupDataAccess(ledgerFactory());
    }
    @Bean
    public IAccountSubGroupDataAccess accountSubGroupDataAccess() {
        return new MemoryDbAccountSubGroupDataAccess(ledgerFactory());
    }
    @Bean
    public IAccountDataAccess accountDataAccess() {
        return new MemoryDbAccountDataAccess(ledgerFactory());
    }
    @Bean
    public ITemplateGroupDataAccess templateGroupDataAccess() {
        return new MemoryDbTemplateGroupDataAccess(ledgerFactory());
    }
    @Bean
    public ITemplateDataAccess templateDataAccess() {
        return new MemoryDbTemplateDataAccess(ledgerFactory());
    }
    @Bean
    public ITransactionDataAccess transactionDataAccess() {
        return new MemoryDbTransactionDataAccess(ledgerFactory());
    }
    @Bean
    public ISystemConfigDataAccess systemConfigDataAccess() {
        return new MemoryDbSystemConfigDataAccess(ledgerFactory());
    }
    //endregion

    //region Business
    @Bean
    public ICategoryGroupService categoryGroupService() {
        return new CategoryGroupService(globalDataAccess(), categoryGroupDataAccess(), categoryDataAccess());
    }
    @Bean
    public ICategoryService categoryService() {
        return new CategoryService(globalDataAccess(), categoryDataAccess(), categoryGroupDataAccess(), accountDataAccess());
    }
    @Bean
    public ICorrespondentGroupService correspondentGroupService() {
        return new CorrespondentGroupService(globalDataAccess(), correspondentGroupDataAccess(), correspondentDataAccess());
    }
    @Bean
    public ICorrespondentService correspondentService() {
        return new CorrespondentService(globalDataAccess(), correspondentDataAccess(), correspondentGroupDataAccess(), accountDataAccess());
    }
    @Bean
    public IProjectGroupService projectGroupService() {
        return new ProjectGroupService(globalDataAccess(), projectGroupDataAccess(), projectDataAccess());
    }
    @Bean
    public IProjectService projectService() {
        return new ProjectService(globalDataAccess(), projectDataAccess(), projectGroupDataAccess(), accountDataAccess());
    }
    @Bean
    public IAccountGroupService accountGroupService() {
        return new AccountGroupService(globalDataAccess(), accountGroupDataAccess(), accountSubGroupDataAccess());
    }
    @Bean
    public IAccountSubGroupService accountSubGroupService() {
        return new AccountSubGroupService(globalDataAccess(), accountDataAccess(), accountSubGroupDataAccess(), accountGroupDataAccess());
    }
    @Bean
    public IAccountService accountService() {
        return new AccountService(
                globalDataAccess(), accountDataAccess(), accountSubGroupDataAccess(),
                categoryDataAccess(), projectDataAccess(), correspondentDataAccess(),
                templateDataAccess(), transactionDataAccess(), currencyDataAccess());
    }
    @Bean
    public ICurrencyService currencyService() {
        return new CurrencyService(globalDataAccess(), currencyDataAccess(), currencyDataCollection());
    }
    @Bean
    public ICurrencyRateService currencyRateService() {
        return new CurrencyRateService(globalDataAccess(), currencyRateDataAccess(), currencyDataAccess());
    }
    @Bean
    public ITemplateGroupService templateGroupService() {
        return new TemplateGroupService(globalDataAccess(), templateGroupDataAccess(), templateDataAccess());
    }
    @Bean
    public ITemplateService templateService() {
        return new TemplateService(globalDataAccess(), templateDataAccess(), templateGroupDataAccess(), accountDataAccess());
    }
    @Bean
    public ITransactionService transactionService() {
        return new TransactionService(globalDataAccess(), transactionDataAccess(), accountDataAccess(), systemConfigDataAccess());
    }
    //endregion
}

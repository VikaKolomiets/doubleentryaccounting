package doubleentryaccounting.business.webapi;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class BusinessWebApiEntryPoint {
    public static void main(String[] args) {
        //AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DiConfiguration.class);
        SpringApplication.run(BusinessWebApiEntryPoint.class, args);
    }
}

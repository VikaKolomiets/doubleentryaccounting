package doubleentryaccounting.business.webapi.controllers;

import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v1/category")
public class CategoryServiceController implements ICategoryService {

    private final ICategoryService service;
    public CategoryServiceController(ICategoryService service) {
        this.service = service;
    }

    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UUID add(@RequestBody ElementEntityInfo info) {
        return this.service.add(info);
    }

    @PostMapping(path = "/update/{entityId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(@PathVariable UUID entityId, @RequestBody ElementEntityInfo info) {
        this.service.update(entityId, info);
    }

    @PostMapping(path = "/delete/{entityId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable UUID entityId) {
        this.service.delete(entityId);
    }

    @PostMapping(path = "/move/{entityId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void moveToAnotherParent(@PathVariable UUID entityId, @RequestBody UUID parentId) {
        this.service.moveToAnotherParent(entityId, parentId);
    }

    @PostMapping(path = "/combine/{primaryId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void combineTwoEntities(@PathVariable UUID primaryId, @RequestBody UUID secondaryId) {
        this.service.combineTwoEntities(primaryId, secondaryId);
    }

    @PostMapping(path = "/favorite/{entityId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void setFavoriteStatus(@PathVariable UUID entityId, @RequestBody boolean isFavorite) {
        this.service.setFavoriteStatus(entityId, isFavorite);
    }

    @PostMapping(path = "/order/{entityId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void setOrder(@PathVariable UUID entityId, int order) {
        this.service.setOrder(entityId, order);
    }

    public Class<Category> getType() {
        return Category.class;
    }
}

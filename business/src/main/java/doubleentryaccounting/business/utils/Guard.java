package doubleentryaccounting.business.utils;

import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.utils.helpers.*;

import java.math.*;
import java.time.*;
import java.util.*;

public class Guard {

    public static void checkUuidForNull(UUID uuid) {
        if (uuid == null) {
            throw new NullPointerException("UUID cannot be a null");
        }
    }

    public static void checkStringForNull(String s) {
        if (StringHelper.isStringNullOrEmpty(s)) {
            throw new NullPointerException("String cannot be a null");
        }
    }

    public static void checkDateForNull(LocalDate date) {
        if (date == null) {
            throw new NullPointerException("Date cannot be a null");
        }
    }

    public static void checkDateTimeForNull(LocalDateTime dateTime) {
        if (dateTime == null) {
            throw new NullPointerException("DateTime cannot be a null");
        }
    }

    public static void checkDecimalForNull(BigDecimal decimal) {
        if (decimal == null) {
            throw new NullPointerException("Decimal cannot be a null");
        }
    }

    public static void checkListForNull(List<?> list) {
        if (list == null) {
            throw new NullPointerException("List cannot be a null");
        }
    }

    public static void checkParameterForNull(Object o) {
        if (o == null) {
            throw new NullPointerException("Parameter cannot be a null");
        }
    }

    public static <T extends IEntity> T getEntityById(IEntityDataAccess<T> dataAccess, UUID entityId) {
        T entity = dataAccess.get(entityId);
        if (entity == null) {
            String typeName = dataAccess.getType().getSimpleName();
            throw new NoSuchElementException("Entity " + typeName + " does not exist");
        }
        return entity;
    }

    public static <T extends IEntity & INamedEntity> void checkEntityWithSameName(IParentEntityDataAccess<T> query, UUID id, String name) {
        ArrayList<T> entities = query.getByName(name);
        checkEntityWithSameName(entities, id, name);
    }

    public static <T extends IEntity & INamedEntity> void checkEntityWithSameName(IChildEntityDataAccess<T> query, UUID parentId, UUID id, String name) {
        ArrayList<T> entities = query.getByName(parentId, name);
        checkEntityWithSameName(entities, id, name);
    }

    public static <T extends IEntity & INamedEntity> void checkEntityWithSameName(ArrayList<T> entities, UUID id, String name) {
        if (entities == null) {
            return;
        }
        entities.forEach((e) -> {
            if (e.getName().equalsIgnoreCase(name) && e.getId() != id) {
                throw new IllegalArgumentException("Entity with the same name has already existed in the collection");
            }
        });
    }

    public static <T extends IEntity & INamedEntity> void checkExistedChildrenInTheGroup(IChildEntityDataAccess<T> query, UUID parentId) {
        if (query.getCount(parentId) > 0) {
            throw new IllegalArgumentException("Parent cannot be deleted. It contains items");
        }
    }
}

package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.currency.*;
import doubleentryaccounting.common.utils.helpers.*;

import java.util.*;

public class CurrencyService  implements ICurrencyService {
    private final IGlobalDataAccess globalDataAccess;
    private final ICurrencyDataAccess currencyDataAccess;
    private final ICurrencyDataCollection currencyDataCollection;

    public CurrencyService(IGlobalDataAccess globalDataAccess, ICurrencyDataAccess currencyDataAccess, ICurrencyDataCollection currencyDataCollection) {
        this.globalDataAccess = globalDataAccess;
        this.currencyDataAccess = currencyDataAccess;
        this.currencyDataCollection = currencyDataCollection;
    }

    public UUID add(CurrencyInfo info) {
        //todo add currency
        CurrencyData currencyData = checkCurrencyInfo(info);
        String symbol = StringHelper.isStringNullOrEmpty(info.getSymbol()) ? currencyData.getSymbol() : info.getSymbol();
        String name = StringHelper.isStringNullOrEmpty(info.getName()) ? currencyData.getSymbol() : info.getName();

        Currency currency = new Currency();
        currency.setIsoCode(currency.getIsoCode());
        currency.setSymbol(symbol);
        currency.setName(name);
        return null;
    }

    //todo update currency
    public void update(CurrencyInfo info) {
        CurrencyData currencyData = checkCurrencyInfo(info);

    }

    //todo delete currency
    public void delete(String isoCode) {

        Guard.checkStringForNull(isoCode);
    }

    //todo currency set status
    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
    }

    //todo currency set order
    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
    }

    private CurrencyData checkCurrencyInfo(CurrencyInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getCode());
        ArrayList<CurrencyData> currenciesData = this.currencyDataCollection.getAvailableCurrencyData();
        CurrencyData currencyData = currenciesData.stream().filter(cd -> cd.getCode().equals(info.getCode())).findFirst().orElse(null);
        if(currencyData == null){
            throw new IllegalArgumentException("Invalid Iso Code for currency");
        }
        return currencyData;
    }
}

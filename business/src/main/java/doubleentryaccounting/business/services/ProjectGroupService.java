package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

public class ProjectGroupService extends ReferenceParentEntityService<ProjectGroup, Project, GroupEntityInfo> implements IProjectGroupService {
    public ProjectGroupService(
            IGlobalDataAccess globalDataAccess,
            IProjectGroupDataAccess entityDataAccess,
            IProjectDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }

    public Class<ProjectGroup> getType() {
        return ProjectGroup.class;
    }
}

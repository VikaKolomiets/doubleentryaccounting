package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

import java.util.*;

public class CorrespondentService extends ReferenceChildEntityService<Correspondent, CorrespondentGroup, ElementEntityInfo> implements ICorrespondentService {
    public CorrespondentService(
            IGlobalDataAccess globalDataAccess,
            ICorrespondentDataAccess entityDataAccess,
            ICorrespondentGroupDataAccess parentEntityDataAccess,
            IAccountDataAccess accountDataAccess) {
        super(globalDataAccess, entityDataAccess, parentEntityDataAccess, accountDataAccess);
    }

    
    protected ArrayList<Account> GetAccountsByEntity(Correspondent entity) {
        return this.getAccountDataAccess().getAccountsByCorrespondent(entity);
    }
    
    protected void AccountEntitySetter(Correspondent entity, Account account) {
        account.setCorrespondent(entity);
    }

    public Class<Correspondent> getType() {
        return Correspondent.class;
    }
}

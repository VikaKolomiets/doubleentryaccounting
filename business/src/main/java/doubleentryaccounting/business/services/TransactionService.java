package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.models.enums.*;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.tuples.*;

import java.math.*;
import java.util.*;

public class TransactionService implements ITransactionService {
    private final ITransactionDataAccess transactionDataAccess;
    private final IAccountDataAccess accountDataAccess;
    private final ISystemConfigDataAccess systemConfigDataAccess;
    private final IGlobalDataAccess globalDataAccess;
    private final BigDecimal zero = new BigDecimal(0);
    private final BigDecimal one = new BigDecimal(1);

    public TransactionService(
            IGlobalDataAccess globalDataAccess,
            ITransactionDataAccess transactionDataAccess,
            IAccountDataAccess accountDataAccess,
            ISystemConfigDataAccess systemConfigDataAccess) {
        this.transactionDataAccess = transactionDataAccess;
        this.accountDataAccess = accountDataAccess;
        this.systemConfigDataAccess = systemConfigDataAccess;
        this.globalDataAccess = globalDataAccess;
    }

    public UUID add(TransactionInfo info)   {
        checkInputTransactionInfo(info);

        Transaction addedEntity = new Transaction();
        Tuple2<List<TransactionEntry>, BigDecimal> tuple = createEntries(info, addedEntity);
        List<TransactionEntry> addedEntries = tuple.getValue1();
        BigDecimal sumAmount = tuple.getValue2();

        addedEntity.setComment(info.getComment());
        addedEntity.setDateTime(info.getDateTime());
        addedEntity.setState(sumAmount.compareTo(zero) == 0 ? info.getState() : TransactionState.NoValid);
        addedEntity.getEntries().addAll(addedEntries);

        this.transactionDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, TransactionInfo info) {
        checkInputTransactionInfo(info);
        Transaction updatedTransaction = Guard.getEntityById(this.transactionDataAccess, entityId);

        Tuple2<List<TransactionEntry>, BigDecimal> tuple = createEntries(info, updatedTransaction);
        List<TransactionEntry> addedEntries = tuple.getValue1();
        BigDecimal sumAmount = tuple.getValue2();

        updatedTransaction.setComment(info.getComment());
        updatedTransaction.setDateTime(info.getDateTime());
        updatedTransaction.setState(sumAmount.compareTo(zero) == 0 ? info.getState() : TransactionState.NoValid);
        updatedTransaction.getEntries().clear();
        updatedTransaction.getEntries().addAll(addedEntries);
        this.transactionDataAccess.update(updatedTransaction);

        this.globalDataAccess.save();
    }

    public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        Transaction deletedTransaction = Guard.getEntityById(this.transactionDataAccess, entityId);
        this.transactionDataAccess.delete(deletedTransaction);

        this.globalDataAccess.save();
    }
    public void deleteTransactionList(ArrayList<UUID> transactionIds) {
        Guard.checkListForNull(transactionIds);
        ArrayList<Transaction> deletedTransactions = new ArrayList<Transaction>();
        for (UUID transactionId : transactionIds) {
            Transaction deletedTransaction = Guard.getEntityById(this.transactionDataAccess, transactionId);
            deletedTransactions.add(deletedTransaction);
        }
        this.transactionDataAccess.deleteList(deletedTransactions);

        this.globalDataAccess.save();
    }

    private void checkInputTransactionInfo(TransactionInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkDateTimeForNull(info.getDateTime());

        if (info.getDateTime().isBefore(this.systemConfigDataAccess.getMinDateTime())
                || info.getDateTime().isAfter(this.systemConfigDataAccess.getMaxDateTime())) {
            throw new IllegalArgumentException("Data and Time is not in valid range");
        }
        if (info.getEntries() == null || info.getEntries().size() < 2) {
            throw new IllegalArgumentException("Invalid amount of Transaction Entries: amount must be more than 1");
        }
    }

    private Tuple2<List<TransactionEntry>, BigDecimal> createEntries(TransactionInfo info, Transaction transaction) {
        String mainCurrencyIsoCode = this.systemConfigDataAccess.getMainCurrencyIsoCode();
        BigDecimal sumAmount = new BigDecimal(0);

        ArrayList<TransactionEntry> entries = new ArrayList<TransactionEntry>();
        for (TransactionEntryInfo entryInfo : info.getEntries()) {
            if (entryInfo.getRate().compareTo(zero) <= 0) {
                throw new IllegalArgumentException("Currency rate must be more than 0");
            }
            Guard.checkUuidForNull(entryInfo.getAccountId());
            Account account = Guard.getEntityById(this.accountDataAccess, entryInfo.getAccountId());
            this.accountDataAccess.loadCurrency(account);

            if (account.getCurrency().getIsoCode() == mainCurrencyIsoCode && entryInfo.getRate().compareTo(one) != 0) {
                throw new IllegalArgumentException("Rate for main Currency should be 1");
            }

            TransactionEntry transactionEntry = new TransactionEntry();
            transactionEntry.setAccount(account);
            transactionEntry.setRate(entryInfo.getRate());
            transactionEntry.setAmount(entryInfo.getAmount());
            transactionEntry.setTransaction(transaction);
            entries.add(transactionEntry);

            sumAmount = sumAmount.add(transactionEntry.getAmount().multiply(transactionEntry.getRate()));
        }
        return new Tuple2<>(entries, sumAmount);
    }

    public Class<Transaction> getType() {
        return Transaction.class;
    }
}

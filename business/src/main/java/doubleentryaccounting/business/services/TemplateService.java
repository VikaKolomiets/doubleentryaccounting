package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.orderedentity.*;

import java.util.*;

public class TemplateService implements ITemplateService {
    private final IGlobalDataAccess globalDataAccess;
    private final ITemplateDataAccess templateDataAccess;
    private final ITemplateGroupDataAccess templateGroupDataAccess;
    private final IAccountDataAccess accountDataAccess;

    public TemplateService(IGlobalDataAccess globalDataAccess,
                           ITemplateDataAccess templateDataAccess,
                           ITemplateGroupDataAccess templateGroupDataAccess,
                           IAccountDataAccess accountDataAccess) {
        this.globalDataAccess = globalDataAccess;
        this.templateDataAccess = templateDataAccess;
        this.templateGroupDataAccess = templateGroupDataAccess;
        this.accountDataAccess = accountDataAccess;
    }

    public UUID add(TemplateInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getName());
        Guard.checkUuidForNull(info.getParentId());
        if (info.getEntries() == null || info.getEntries().size() < 2) {
            throw new IllegalArgumentException("Invalid amount of Transaction Entries: amount must be more than 1");
        }

        List<TemplateEntry> entries = createEntries(info);

        TemplateGroup templateGroup = Guard.getEntityById(this.templateGroupDataAccess, info.getParentId());
        Guard.checkEntityWithSameName(this.templateDataAccess, templateGroup.getId(), null, info.getName());

        Template addedEntity = new Template();
        addedEntity.setName(info.getName());
        addedEntity.setDescription(info.getDescription());
        addedEntity.setIsFavorite(info.getIsFavorite());
        addedEntity.setOrder(this.templateDataAccess.getMaxOrder(templateGroup.getId()) + 1);

        addedEntity.getEntries().addAll(entries);

        templateGroup.getChildren().add(addedEntity);
        addedEntity.setParent(templateGroup);
        this.templateDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, TemplateInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(entityId);
        Guard.checkStringForNull(info.getName());
        if (info.getEntries() == null || info.getEntries().size() < 2) {
            throw new IllegalArgumentException("Template entity is not correct: number of Entries must be more or equal 2");
        }
        List<TemplateEntry> entries = createEntries(info);

        Template updatedTemplate = Guard.getEntityById(this.templateDataAccess, entityId);
        Guard.checkEntityWithSameName(this.templateDataAccess, updatedTemplate.getParent().getId(), entityId, info.getName());

        updatedTemplate.setName(info.getName());
        updatedTemplate.setDescription(info.getDescription());
        updatedTemplate.setIsFavorite(info.getIsFavorite());
        updatedTemplate.getEntries().clear();
        updatedTemplate.getEntries().addAll(entries);

        this.templateDataAccess.update(updatedTemplate);

        this.globalDataAccess.save();
    }

    public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        Template deletedTemplate = Guard.getEntityById(this.templateDataAccess, entityId);
        this.templateDataAccess.loadParent(deletedTemplate);
        TemplateGroup templateGroup = deletedTemplate.getParent();
        this.templateGroupDataAccess.loadChildren(templateGroup);

        templateGroup.getChildren().remove(deletedTemplate);
        this.templateDataAccess.delete(deletedTemplate);

        OrderedEntityHelper.reorder(templateGroup.getChildren());
        this.templateDataAccess.updateList(templateGroup.getChildren());

        this.globalDataAccess.save();
    }

    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
        Template template = Guard.getEntityById(this.templateDataAccess, entityId);
        if (template.getIsFavorite() != isFavorite) {
            template.setIsFavorite(isFavorite);
            this.templateDataAccess.update(template);
        }

        this.globalDataAccess.save();
    }

    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
        Template template = Guard.getEntityById(this.templateDataAccess, entityId);
        if (template.getOrder() != order) {
            this.templateDataAccess.loadParent(template);
            TemplateGroup group = template.getParent();
            this.templateGroupDataAccess.loadChildren(group);
            OrderedEntityHelper.setOrder(group.getChildren(), template, order);
            this.templateDataAccess.updateList(group.getChildren());
        }

        this.globalDataAccess.save();
    }

    public void moveToAnotherParent(UUID entityId, UUID parentId) {
        Guard.checkUuidForNull(entityId);
        Guard.checkUuidForNull(parentId);
        Template entity = Guard.getEntityById(this.templateDataAccess, entityId);
        this.templateDataAccess.loadParent(entity);
        TemplateGroup fromTemplateGroup = entity.getParent();
        TemplateGroup toTemplateGroup = Guard.getEntityById(this.templateGroupDataAccess, parentId);

        if (fromTemplateGroup.getId() != toTemplateGroup.getId()) {
            Guard.checkEntityWithSameName(this.templateDataAccess, toTemplateGroup.getId(), entity.getId(), entity.getName());
            this.templateGroupDataAccess.loadChildren(fromTemplateGroup);

            toTemplateGroup.getChildren().add(entity);
            entity.setParent(toTemplateGroup);
            entity.setOrder(this.templateDataAccess.getMaxOrder(fromTemplateGroup.getId()) + 1);

            fromTemplateGroup.getChildren().remove(entity);
            this.templateDataAccess.update(entity);
            OrderedEntityHelper.reorder(fromTemplateGroup.getChildren());
            this.templateDataAccess.updateList(fromTemplateGroup.getChildren());
        }

        this.globalDataAccess.save();
    }

     private ArrayList<TemplateEntry> createEntries(TemplateInfo info) {
        ArrayList<TemplateEntry> entries = new ArrayList<TemplateEntry>();
        for (TemplateEntryInfo entry : info.getEntries()) {
            Guard.checkUuidForNull(entry.getAccountId());
            TemplateEntry templateEntry = new TemplateEntry();

            templateEntry.setAccount(Guard.getEntityById(this.accountDataAccess, entry.getAccountId()));
            templateEntry.setAmount(entry.getAmount());

            entries.add(templateEntry);
        }

        return entries;
    }

    public Class<Template> getType() {
        return Template.class;
    }
}

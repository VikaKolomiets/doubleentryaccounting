package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

import java.util.*;

public class ProjectService extends ReferenceChildEntityService<Project, ProjectGroup, ElementEntityInfo> implements IProjectService {
    public ProjectService(
            IGlobalDataAccess globalDataAccess,
            IProjectDataAccess entityDataAccess,
            IProjectGroupDataAccess parentEntityDataAccess,
            IAccountDataAccess accountDataAccess) {
        super(globalDataAccess, entityDataAccess, parentEntityDataAccess, accountDataAccess);
    }

    
    protected ArrayList<Account> GetAccountsByEntity(Project entity) {
        return this.getAccountDataAccess().getAccountsByProject(entity);
    }
    
    protected void AccountEntitySetter(Project entity, Account account) {
        account.setProject(entity);
    }

    public Class<Project> getType() {
        return Project.class;
    }
}

package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.orderedentity.*;

import java.util.*;

public class AccountSubGroupService implements IAccountSubGroupService {
    private final IGlobalDataAccess globalDataAccess;
    private final IAccountDataAccess accountDataAccess;
    private final IAccountSubGroupDataAccess accountSubGroupDataAccess;
    private final IAccountGroupDataAccess accountGroupDataAccess;

    public AccountSubGroupService(
            IGlobalDataAccess globalDataAccess,
            IAccountDataAccess accountDataAccess,
            IAccountSubGroupDataAccess accountSubGroupDataAccess,
            IAccountGroupDataAccess accountGroupDataAccess) {
        this.globalDataAccess = globalDataAccess;
        this.accountDataAccess = accountDataAccess;
        this.accountSubGroupDataAccess = accountSubGroupDataAccess;
        this.accountGroupDataAccess = accountGroupDataAccess;
    }

    public UUID add(AccountSubGroupInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getName());
        Guard.checkUuidForNull(info.getParentId());

        AccountGroup group = Guard.getEntityById(this.accountGroupDataAccess, info.getParentId());
        Guard.checkEntityWithSameName(this.accountSubGroupDataAccess, group.getId(), null, info.getName());

        AccountSubGroup addedEntity = new AccountSubGroup();
        addedEntity.setName(info.getName());
        addedEntity.setDescription(info.getDescription());
        addedEntity.setIsFavorite(info.getIsFavorite());
        addedEntity.setOrder(this.accountSubGroupDataAccess.getMaxOrder(addedEntity.getId()) + 1);

        group.getChildren().add(addedEntity);
        addedEntity.setParent(group);

        this.accountSubGroupDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, AccountSubGroupInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(entityId);
        Guard.checkStringForNull(info.getName());

        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, entityId);
        this.accountSubGroupDataAccess.loadParent(subGroup);
        AccountGroup parent = subGroup.getParent();
        Guard.checkEntityWithSameName(this.accountSubGroupDataAccess, parent.getId(), entityId, info.getName());

        subGroup.setName(info.getName());
        subGroup.setDescription(info.getDescription());
        subGroup.setIsFavorite(info.getIsFavorite());

        this.accountSubGroupDataAccess.update(subGroup);

        this.globalDataAccess.save();
    }

    public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, entityId);
        Guard.checkExistedChildrenInTheGroup(this.accountDataAccess, subGroup.getId());

        this.accountSubGroupDataAccess.loadParent(subGroup);
        AccountGroup group = subGroup.getParent();
        this.accountGroupDataAccess.loadChildren(group);

        group.getChildren().remove(subGroup);
        this.accountSubGroupDataAccess.delete(subGroup);

        OrderedEntityHelper.reorder(group.getChildren());
        this.accountSubGroupDataAccess.updateList(group.getChildren());

        this.globalDataAccess.save();
    }

    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, entityId);
        if (subGroup.getIsFavorite() != isFavorite) {
            subGroup.setIsFavorite(isFavorite);
            this.accountSubGroupDataAccess.update(subGroup);
        }
        this.globalDataAccess.save();
    }

    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, entityId);
        if (subGroup.getOrder() != order) {
            this.accountSubGroupDataAccess.loadParent(subGroup);
            AccountGroup group = subGroup.getParent();
            this.accountGroupDataAccess.loadChildren(group);
            OrderedEntityHelper.setOrder(group.getChildren(), subGroup, order);
            this.accountSubGroupDataAccess.updateList(group.getChildren());
        }

        this.globalDataAccess.save();
    }

    public void moveToAnotherParent(UUID entityId, UUID groupId) {
        Guard.checkUuidForNull(entityId);
        Guard.checkUuidForNull(groupId);
        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, entityId);
        this.accountSubGroupDataAccess.loadParent(subGroup);
        AccountGroup fromAccountGroup = subGroup.getParent();

        AccountGroup toAccountGroup = Guard.getEntityById(this.accountGroupDataAccess, groupId);

        if (fromAccountGroup.getId() != toAccountGroup.getId()) {
            Guard.checkEntityWithSameName(this.accountSubGroupDataAccess, toAccountGroup.getId(), subGroup.getId(), subGroup.getName());

            this.accountGroupDataAccess.loadChildren(fromAccountGroup);

            subGroup.setOrder(this.accountSubGroupDataAccess.getMaxOrder(toAccountGroup.getId()) + 1);

            fromAccountGroup.getChildren().remove(subGroup);
            toAccountGroup.getChildren().add(subGroup);
            subGroup.setParent(toAccountGroup);
            this.accountSubGroupDataAccess.update(subGroup);

            OrderedEntityHelper.reorder(fromAccountGroup.getChildren());
            this.accountSubGroupDataAccess.updateList(fromAccountGroup.getChildren());
        }

        this.globalDataAccess.save();
    }

    public Class<AccountSubGroup> getType() {
        return AccountSubGroup.class;
    }
}

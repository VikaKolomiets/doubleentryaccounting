package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

import java.util.*;

public class CurrencyRateService implements ICurrencyRateService {

    private final ICurrencyRateDataAccess currencyRateDataAccess;
    private final IGlobalDataAccess globalDataAccess;
    private final ICurrencyDataAccess currencyDataAccess;

    public CurrencyRateService(
            IGlobalDataAccess globalDataAccess, ICurrencyRateDataAccess currencyRateDataAccess,
            ICurrencyDataAccess currencyDataAccess) {
        this.currencyRateDataAccess = currencyRateDataAccess;
        this.globalDataAccess = globalDataAccess;
        this.currencyDataAccess = currencyDataAccess;
    }

    public UUID add(CurrencyRateInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(info.getCurrencyId());
        Guard.checkDateForNull(info.getDate());
        Guard.checkDecimalForNull(info.getRate());

        Currency currency = Guard.getEntityById(this.currencyDataAccess, info.getCurrencyId());

        CurrencyRate addedEntity = new CurrencyRate();
        addedEntity.setDate(info.getDate());
        addedEntity.setRate(info.getRate());
        addedEntity.setComment(info.getComment());

        currency.getRates().add(addedEntity);
        addedEntity.setCurrency(currency);
        this.currencyRateDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, CurrencyRateInfo info) {
        //todo update currency rate
    }

    public void delete(UUID entityId) {
        //todo delete currency rate
    }

    public void deleteRateList(ArrayList<UUID> transactionIds) {
        //todo delete list of currency rate
    }

    public Class<CurrencyRate> getType() {
        return CurrencyRate.class;
    }
}

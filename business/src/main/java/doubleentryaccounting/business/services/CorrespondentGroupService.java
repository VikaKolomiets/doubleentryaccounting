package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

public class CorrespondentGroupService extends ReferenceParentEntityService<CorrespondentGroup, Correspondent, GroupEntityInfo> implements ICorrespondentGroupService {
    public CorrespondentGroupService(
            IGlobalDataAccess globalDataAccess,
            ICorrespondentGroupDataAccess entityDataAccess,
            ICorrespondentDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }

    public Class<CorrespondentGroup> getType() {
        return CorrespondentGroup.class;
    }
}

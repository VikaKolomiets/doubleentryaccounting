package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

public class AccountGroupService extends ReferenceParentEntityService<AccountGroup, AccountSubGroup, AccountGroupInfo> implements IAccountGroupService {
    public AccountGroupService(
            IGlobalDataAccess globalDataAccess,
            IAccountGroupDataAccess entityDataAccess,
            IAccountSubGroupDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }

    public Class<AccountGroup> getType() {
        return AccountGroup.class;
    }
}

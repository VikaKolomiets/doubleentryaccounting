package doubleentryaccounting.business.services;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;
import doubleentryaccounting.common.utils.orderedentity.*;

import java.util.*;

public class AccountService implements IAccountService {
    private final IGlobalDataAccess globalDataAccess;
    private final IAccountDataAccess accountDataAccess;
    private final IAccountSubGroupDataAccess accountSubGroupDataAccess;
    private final ICategoryDataAccess categoryDataAccess;
    private final IProjectDataAccess projectDataAccess;
    private final ICorrespondentDataAccess correspondentDataAccess;
    private final ITemplateDataAccess templateDataAccess;
    private final ITransactionDataAccess transactionDataAccess;
    private final ICurrencyDataAccess currencyDataAccess;

    public AccountService(IGlobalDataAccess globalDataAccess
            , IAccountDataAccess accountDataAccess
            , IAccountSubGroupDataAccess subGroupDataAccess
            , ICategoryDataAccess categoryDataAccess
            , IProjectDataAccess projectDataAccess
            , ICorrespondentDataAccess correspondentDataAccess
            , ITemplateDataAccess templateDataAccess
            , ITransactionDataAccess transactionDataAccess
            , ICurrencyDataAccess currencyDataAccess) {
        this.globalDataAccess = globalDataAccess;
        this.accountDataAccess = accountDataAccess;
        this.accountSubGroupDataAccess = subGroupDataAccess;
        this.categoryDataAccess = categoryDataAccess;
        this.projectDataAccess = projectDataAccess;
        this.correspondentDataAccess = correspondentDataAccess;
        this.templateDataAccess = templateDataAccess;
        this.transactionDataAccess = transactionDataAccess;
        this.currencyDataAccess = currencyDataAccess;
    }

    public UUID add(AccountInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getName());
        Guard.checkUuidForNull(info.getParentId());
        Guard.checkUuidForNull(info.getCurrencyId());

        AccountSubGroup subGroup = Guard.getEntityById(this.accountSubGroupDataAccess, info.getParentId());
        Guard.checkEntityWithSameName(this.accountDataAccess, subGroup.getId(), null, info.getName());

        Account addedEntity = new Account();
        addedEntity.setName(info.getName());
        addedEntity.setDescription(info.getDescription());
        addedEntity.setIsFavorite(info.getIsFavorite());
        addedEntity.setOrder(this.accountDataAccess.getMaxOrder(subGroup.getId()) + 1);

        addedEntity.setCurrency(Guard.getEntityById(this.currencyDataAccess, info.getCurrencyId()));

        if (info.getCategoryId() != null) {
            addedEntity.setCategory(Guard.getEntityById(this.categoryDataAccess, info.getCategoryId()));
        }
        if (info.getProjectId() != null) {
            addedEntity.setProject(Guard.getEntityById(this.projectDataAccess, info.getProjectId()));
        }
        if (info.getCorrespondentId() != null) {
            addedEntity.setCorrespondent(Guard.getEntityById(this.correspondentDataAccess, info.getCorrespondentId()));
        }

        subGroup.getChildren().add(addedEntity);
        addedEntity.setParent(subGroup);
        this.accountDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, AccountInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(entityId);
        Guard.checkStringForNull(info.getName());

        Account updatedEntity = Guard.getEntityById(this.accountDataAccess, entityId);
        this.accountDataAccess.loadParent(updatedEntity);
        AccountSubGroup subGroup = updatedEntity.getParent();
        Guard.checkEntityWithSameName(this.accountDataAccess, subGroup.getId(), entityId, info.getName());

        Category category = info.getCategoryId() == null ? null : (Guard.getEntityById(this.categoryDataAccess, info.getCategoryId()));
        Project project = info.getProjectId() == null ? null : (Guard.getEntityById(this.projectDataAccess, info.getProjectId()));
        Correspondent correspondent = info.getCorrespondentId() == null
                ? null : (Guard.getEntityById(this.correspondentDataAccess, info.getCorrespondentId()));

        updatedEntity.setName(info.getName());
        updatedEntity.setDescription(info.getDescription());
        updatedEntity.setIsFavorite(info.getIsFavorite());

        updatedEntity.setCategory(category);
        updatedEntity.setProject(project);
        updatedEntity.setCorrespondent(correspondent);

        this.accountDataAccess.update(updatedEntity);

        this.globalDataAccess.save();
    }

     public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        Account account = Guard.getEntityById(this.accountDataAccess, entityId);
        if (this.transactionDataAccess.getTransactionEntriesCount(account.getId()) > 0) {
            throw new IllegalArgumentException("Account cannot be delete, it contains transaction.");
        }
        if (this.templateDataAccess.getTemplateEntriesCount(account.getId()) > 0) {
            throw new IllegalArgumentException("Account cannot be delete, it contains template.");
        }

        this.accountDataAccess.loadParent(account);
        AccountSubGroup subGroup = account.getParent();
        this.accountSubGroupDataAccess.loadChildren(subGroup);

        subGroup.getChildren().remove(account);
        this.accountDataAccess.delete(account);

        OrderedEntityHelper.reorder(subGroup.getChildren());
        this.accountDataAccess.updateList(subGroup.getChildren());

        this.globalDataAccess.save();
    }

    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
        Account account = Guard.getEntityById(this.accountDataAccess, entityId);
        if (account.getIsFavorite() != isFavorite) {
            account.setIsFavorite(isFavorite);
            this.accountDataAccess.update(account);
        }

        this.globalDataAccess.save();
    }

    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
        Account account = Guard.getEntityById(this.accountDataAccess, entityId);
        if (account.getOrder() != order) {
            this.accountDataAccess.loadParent(account);
            AccountSubGroup subGroup = account.getParent();
            this.accountSubGroupDataAccess.loadChildren(subGroup);

            OrderedEntityHelper.setOrder(subGroup.getChildren(), account, order);
            this.accountDataAccess.updateList(subGroup.getChildren());
        }

        this.globalDataAccess.save();
    }

    public void moveToAnotherParent(UUID entityId, UUID parentId) {
        Guard.checkUuidForNull(entityId);
        Guard.checkUuidForNull(parentId);
        Account account = Guard.getEntityById(this.accountDataAccess, entityId);
        this.accountDataAccess.loadParent(account);
        AccountSubGroup fromAccountSubGroup = account.getParent();

        AccountSubGroup toAccountSubGroup = Guard.getEntityById(this.accountSubGroupDataAccess, parentId);

        if (fromAccountSubGroup.getId() != toAccountSubGroup.getId()) {

            Guard.checkEntityWithSameName(this.accountDataAccess, toAccountSubGroup.getId(), account.getId(), account.getName());

            this.accountSubGroupDataAccess.loadChildren(fromAccountSubGroup);

            fromAccountSubGroup.getChildren().remove(account);
            toAccountSubGroup.getChildren().add(account);
            account.setParent(toAccountSubGroup);
            account.setOrder(this.accountDataAccess.getMaxOrder(toAccountSubGroup.getId()) + 1);

            this.accountDataAccess.update(account);

            OrderedEntityHelper.reorder(fromAccountSubGroup.getChildren());
            this.accountDataAccess.updateList(fromAccountSubGroup.getChildren());
        }

        this.globalDataAccess.save();
    }

    public void combineTwoEntities(UUID primaryId, UUID secondaryId) {
        Guard.checkUuidForNull(primaryId);
        Guard.checkUuidForNull(secondaryId);
        Account primaryAccount = Guard.getEntityById(this.accountDataAccess, primaryId);
        Account secondaryAccount = Guard.getEntityById(this.accountDataAccess, secondaryId);

        if (primaryAccount.getId() != secondaryAccount.getId()) {
            this.accountDataAccess.loadCurrency(primaryAccount);
            this.accountDataAccess.loadCurrency(secondaryAccount);
            if (primaryAccount.getCurrency().getId() != secondaryAccount.getCurrency().getId()) {
                throw new IllegalArgumentException("Can't combine accounts with different currencies");
            }

            ArrayList<TemplateEntry> templates = this.templateDataAccess.getEntriesByAccount(secondaryAccount);
            for (TemplateEntry templateEntry : templates) {
                templateEntry.setAccount(primaryAccount);
                this.templateDataAccess.update(templateEntry.getTemplate());
            }

            List<TransactionEntry> transactions = this.transactionDataAccess.getEntriesByAccount(secondaryAccount);
            for (TransactionEntry transactionEntry : transactions) {
                transactionEntry.setAccount(primaryAccount);
                this.transactionDataAccess.update(transactionEntry.getTransaction());
            }

            this.accountDataAccess.loadParent(secondaryAccount);
            AccountSubGroup secondarySubGroup = secondaryAccount.getParent();
            this.accountSubGroupDataAccess.loadChildren(secondarySubGroup);

            secondarySubGroup.getChildren().remove(secondaryAccount);
            this.accountDataAccess.delete(secondaryAccount);

            OrderedEntityHelper.reorder(secondarySubGroup.getChildren());
            this.accountDataAccess.updateList(secondarySubGroup.getChildren());
        }

        this.globalDataAccess.save();
    }

    public Class<Account> getType() {
        return Account.class;
    }
}

package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

public class CategoryGroupService extends ReferenceParentEntityService<CategoryGroup, Category, GroupEntityInfo> implements ICategoryGroupService {
    public CategoryGroupService(
            IGlobalDataAccess globalDataAccess,
            ICategoryGroupDataAccess entityDataAccess,
            ICategoryDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }

    public Class<CategoryGroup> getType() {
        return CategoryGroup.class;
    }
}

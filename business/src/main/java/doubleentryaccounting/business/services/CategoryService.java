package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

import java.util.*;

public class CategoryService extends ReferenceChildEntityService<Category, CategoryGroup, ElementEntityInfo> implements ICategoryService {
    public CategoryService(
            IGlobalDataAccess globalDataAccess,
            ICategoryDataAccess entityDataAccess,
            ICategoryGroupDataAccess parentEntityDataAccess,
            IAccountDataAccess accountDataAccess) {
        super(globalDataAccess, entityDataAccess, parentEntityDataAccess, accountDataAccess);
    }

    
    protected ArrayList<Account> GetAccountsByEntity(Category entity) {
        return this.getAccountDataAccess().getAccountsByCategory(entity);
    }
    
    protected void AccountEntitySetter(Category entity, Account account) {
        account.setCategory(entity);
    }

    public Class<Category> getType() {
        return Category.class;
    }
}

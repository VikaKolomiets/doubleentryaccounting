package doubleentryaccounting.business.services;

import doubleentryaccounting.business.services.base.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.services.*;

public class TemplateGroupService extends ReferenceParentEntityService<TemplateGroup, Template, GroupEntityInfo> implements ITemplateGroupService {
    public TemplateGroupService(
            IGlobalDataAccess globalDataAccess,
            ITemplateGroupDataAccess entityDataAccess,
            ITemplateDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }

    public Class<TemplateGroup> getType() {
        return TemplateGroup.class;
    }
}

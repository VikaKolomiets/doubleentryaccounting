package doubleentryaccounting.business.services.base;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.infos.interfaces.*;
import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.services.base.*;
import doubleentryaccounting.common.utils.helpers.*;
import doubleentryaccounting.common.utils.orderedentity.*;

import java.util.*;

public abstract class ReferenceParentEntityService<T extends IReferenceParentEntity<TChild>, TChild extends IReferenceChildEntity<T>,
        TI extends INamedEntityInfo & IFavoriteEntityInfo>
        implements IReferenceEntityService<T, TI> {
    private final IGlobalDataAccess globalDataAccess;
    private final IReferenceParentEntityDataAccess<T> entityDataAccess;
    private final IReferenceChildEntityDataAccess<TChild> childEntityDataAccess;

    public ReferenceParentEntityService(
            IGlobalDataAccess globalDataAccess,
            IReferenceParentEntityDataAccess<T> entityDataAccess,
            IReferenceChildEntityDataAccess<TChild> childEntityDataAccess) {
        this.globalDataAccess = globalDataAccess;
        this.entityDataAccess = entityDataAccess;
        this.childEntityDataAccess = childEntityDataAccess;
    }

    public UUID add(TI info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getName());
        Guard.checkEntityWithSameName(this.entityDataAccess, null, info.getName());

        T addedEntity = GenericHelper.createGenericType(this.getType());
        addedEntity.setName(info.getName());
        addedEntity.setDescription(info.getDescription());
        addedEntity.setIsFavorite(info.getIsFavorite());
        addedEntity.setOrder(this.entityDataAccess.getMaxOrder() + 1);

        this.entityDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, TI info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(entityId);
        Guard.checkStringForNull(info.getName());
        Guard.checkEntityWithSameName(this.entityDataAccess, entityId, info.getName());

        T updatedEntity = Guard.getEntityById(this.entityDataAccess, entityId);
        updatedEntity.setName(info.getName());
        updatedEntity.setDescription(info.getDescription());
        updatedEntity.setIsFavorite(info.getIsFavorite());

        this.entityDataAccess.update(updatedEntity);

        this.globalDataAccess.save();
    }

    public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        T deletedEntity = Guard.getEntityById(this.entityDataAccess, entityId);
        Guard.checkExistedChildrenInTheGroup(this.childEntityDataAccess, deletedEntity.getId());

        this.entityDataAccess.delete(deletedEntity);

        ArrayList<T> list = this.entityDataAccess.getList();
        OrderedEntityHelper.reorder(list);
        this.entityDataAccess.updateList(list);

        this.globalDataAccess.save();

    }

    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
        T entity = Guard.getEntityById(this.entityDataAccess, entityId);
        if (entity.getIsFavorite() != isFavorite) {
            entity.setIsFavorite(isFavorite);
            this.entityDataAccess.update(entity);
        }

        this.globalDataAccess.save();
    }


    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
        T entity = Guard.getEntityById(this.entityDataAccess, entityId);
        if (entity.getOrder() != order) {
            ArrayList<T> list = this.entityDataAccess.getList();
            OrderedEntityHelper.setOrder(list, entity, order);
            this.entityDataAccess.updateList(list);
        }

        this.globalDataAccess.save();
    }
}

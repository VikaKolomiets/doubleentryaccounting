package doubleentryaccounting.business.services.base;

import doubleentryaccounting.business.utils.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.dataaccess.base.*;
import doubleentryaccounting.common.infos.interfaces.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.services.base.*;
import doubleentryaccounting.common.utils.helpers.*;
import doubleentryaccounting.common.utils.orderedentity.*;

import java.util.*;

public abstract class ReferenceChildEntityService<T extends IReferenceChildEntity<TParent>, TParent extends IReferenceParentEntity<T>,
        TInfo extends INamedEntityInfo & IFavoriteEntityInfo & IChildEntityInfo>
        implements IReferenceChildEntityService<T, TInfo> {
    private final IGlobalDataAccess globalDataAccess;
    private final IReferenceChildEntityDataAccess<T> entityDataAccess;
    private final IReferenceParentEntityDataAccess<TParent> parentEntityDataAccess;
    private final IAccountDataAccess accountDataAccess;

    public ReferenceChildEntityService(
            IGlobalDataAccess globalDataAccess,
            IReferenceChildEntityDataAccess<T> entityDataAccess,
            IReferenceParentEntityDataAccess<TParent> parentEntityDataAccess,
            IAccountDataAccess accountDataAccess)
    {
        this.globalDataAccess = globalDataAccess;
        this.entityDataAccess = entityDataAccess;
        this.parentEntityDataAccess = parentEntityDataAccess;
        this.accountDataAccess = accountDataAccess;
    }

    protected IAccountDataAccess getAccountDataAccess() { return this.accountDataAccess; }
    protected abstract ArrayList<Account> GetAccountsByEntity(T entity);
    protected abstract void AccountEntitySetter(T entity, Account account);

    public UUID add(TInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkStringForNull(info.getName());
        Guard.checkUuidForNull(info.getParentId());

        TParent parent = Guard.getEntityById(this.parentEntityDataAccess, info.getParentId());
        Guard.checkEntityWithSameName(this.entityDataAccess, parent.getId(), null, info.getName());

        T addedEntity = GenericHelper.createGenericType(this.getType());
        addedEntity.setName(info.getName());
        addedEntity.setDescription(info.getDescription());
        addedEntity.setIsFavorite(info.getIsFavorite());
        addedEntity.setOrder(this.entityDataAccess.getMaxOrder(parent.getId()) + 1);

        addedEntity.setParent(parent);
        parent.getChildren().add(addedEntity);

        this.entityDataAccess.add(addedEntity);

        this.globalDataAccess.save();

        return addedEntity.getId();
    }

    public void update(UUID entityId, TInfo info) {
        Guard.checkParameterForNull(info);
        Guard.checkUuidForNull(entityId);
        Guard.checkStringForNull(info.getName());

        T updatedEntity = Guard.getEntityById(this.entityDataAccess, entityId);
        this.entityDataAccess.loadParent(updatedEntity);
        TParent parent = updatedEntity.getParent();
        Guard.checkEntityWithSameName(this.entityDataAccess, parent.getId(), entityId, info.getName());

        updatedEntity.setName(info.getName());
        updatedEntity.setDescription(info.getDescription());
        updatedEntity.setIsFavorite(info.getIsFavorite());

        this.entityDataAccess.update(updatedEntity);

        this.globalDataAccess.save();
    }

    
    public void delete(UUID entityId) {
        Guard.checkUuidForNull(entityId);
        T deletedEntity = Guard.getEntityById(this.entityDataAccess, entityId);
        this.entityDataAccess.loadParent(deletedEntity);
        TParent parent = deletedEntity.getParent();
        this.parentEntityDataAccess.loadChildren(parent);

        List<Account> accounts = this.GetAccountsByEntity(deletedEntity);
        for(Account account : accounts)
        {
            this.AccountEntitySetter(null, account);
            this.accountDataAccess.update(account);
        }

        parent.getChildren().remove(deletedEntity);
        this.entityDataAccess.delete(deletedEntity);

        OrderedEntityHelper.reorder(parent.getChildren());
        this.entityDataAccess.updateList(parent.getChildren());

        this.globalDataAccess.save();
    }

    
    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkUuidForNull(entityId);
        T etrity = Guard.getEntityById(this.entityDataAccess, entityId);
        if (etrity.getIsFavorite() != isFavorite)
        {
            etrity.setIsFavorite(isFavorite);
            this.entityDataAccess.update(etrity);
        }

        this.globalDataAccess.save();
    }

    
    public void setOrder(UUID entityId, int order) {
        Guard.checkUuidForNull(entityId);
        T entity = Guard.getEntityById(this.entityDataAccess, entityId);
        if (entity.getOrder() != order)
        {
            this.entityDataAccess.loadParent(entity);
            TParent parent = entity.getParent();
            this.parentEntityDataAccess.loadChildren(parent);
            OrderedEntityHelper.setOrder(parent.getChildren(), entity, order);
            this.entityDataAccess.updateList(parent.getChildren());
        }

        this.globalDataAccess.save();
    }
    
    public void moveToAnotherParent(UUID entityId, UUID parentId) {
        Guard.checkUuidForNull(entityId);
        T entity = Guard.getEntityById(this.entityDataAccess, entityId);
        this.entityDataAccess.loadParent(entity);
        TParent fromParent = entity.getParent();
        TParent toParent = Guard.getEntityById(this.parentEntityDataAccess, parentId);

        if (fromParent.getId() != toParent.getId())
        {
            Guard.checkEntityWithSameName(this.entityDataAccess, toParent.getId(), entity.getId(), entity.getName());
            this.parentEntityDataAccess.loadChildren(fromParent);

            entity.setOrder(this.entityDataAccess.getMaxOrder(toParent.getId()) + 1);
            entity.setParent(toParent);
            toParent.getChildren().add(entity);
            this.entityDataAccess.update(entity);

            fromParent.getChildren().remove(entity);
            OrderedEntityHelper.reorder(fromParent.getChildren());
            this.entityDataAccess.updateList(fromParent.getChildren());
        }

        this.globalDataAccess.save();
    }

    public void combineTwoEntities(UUID primaryId, UUID secondaryId) {
        Guard.checkUuidForNull(primaryId);
        Guard.checkUuidForNull(secondaryId);
        T primaryItem = Guard.getEntityById(this.entityDataAccess, primaryId);
        T secondaryItem = Guard.getEntityById(this.entityDataAccess, secondaryId);

        if (primaryItem.getId() != secondaryItem.getId())
        {
            List<Account> accounts = this.GetAccountsByEntity(secondaryItem);
            for (Account account : accounts)
            {
                this.AccountEntitySetter(primaryItem, account);
                this.accountDataAccess.update(account);
            }

            this.entityDataAccess.loadParent(secondaryItem);
            TParent parent = secondaryItem.getParent();
            this.parentEntityDataAccess.loadChildren(parent);

            parent.getChildren().remove(secondaryItem);
            this.entityDataAccess.delete(secondaryItem);

            OrderedEntityHelper.reorder(parent.getChildren());
            this.entityDataAccess.updateList(parent.getChildren());
        }

        this.globalDataAccess.save();
    }
}

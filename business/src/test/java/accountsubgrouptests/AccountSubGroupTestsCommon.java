package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AccountSubGroupTestsCommon {

    public static AccountSubGroup createAccountSubGroup(){
        AccountSubGroup subGroup = new AccountSubGroup();
        subGroup.setId(UUID.randomUUID());
        subGroup.setName("SubGroup");
        subGroup.setName("For Unit Test");
        return subGroup;
    }
    public static Account createAccount(){
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");
        Account account = new Account();
        account.setCurrency(currency);
        account.setId(UUID.randomUUID());
        account.setName("TestAccount");
        return account;
    }
    public static AccountGroup createAccountGroup(){
        AccountGroup group = new AccountGroup();
        group.setName("TestGroup");
        group.setId(UUID.randomUUID());
        return group;
    }

    public static IGlobalDataAccess createMockGlobalDataAccess(){
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }

    public static IAccountDataAccess createMockAccountDataAccess(){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }
    public static IAccountSubGroupDataAccess createMockAccountSubGroupDataAccess(){
        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        return accountSubGroupDataAccess;
    }
    public static IAccountSubGroupDataAccess createMockAccountSubGroupDataAccessReturnEntity(AccountSubGroup group){
        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        when(accountSubGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(group);
        when(accountSubGroupDataAccess.getType()).thenReturn(AccountSubGroup.class);
        return accountSubGroupDataAccess;
    }
    public static IAccountGroupDataAccess createMockAccountGroupDataAccess(){
        IAccountGroupDataAccess accountGroupDataAccess = Mockito.mock(IAccountGroupDataAccess.class);
        return accountGroupDataAccess;
    }
    public static IAccountGroupDataAccess createMockAccountGroupDataAccessReturnEntity(AccountGroup group){
        IAccountGroupDataAccess accountGroupDataAccess = Mockito.mock(IAccountGroupDataAccess.class);
        when(accountGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(group);
        when(accountGroupDataAccess.getType()).thenReturn(AccountGroup.class);
        return accountGroupDataAccess;
    }
    public static AccountSubGroupService createAccountSubGroupService(){
        AccountSubGroupService service = new AccountSubGroupService(
                createMockGlobalDataAccess(),
                createMockAccountDataAccess(),
                createMockAccountSubGroupDataAccess(),
                createMockAccountGroupDataAccess());
        return service;
    }

}

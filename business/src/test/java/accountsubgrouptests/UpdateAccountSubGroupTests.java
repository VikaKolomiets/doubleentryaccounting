package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateAccountSubGroupTests {
    private AccountGroup parent;
    private AccountSubGroup entity;
    private AccountSubGroupInfo info;

    @BeforeEach
    public void setUpTest(){
        parent = AccountSubGroupTestsCommon.createAccountGroup();
        entity = AccountSubGroupTestsCommon.createAccountSubGroup();
        entity.setParent(parent);
        parent.getChildren().add(entity);
    }
    @AfterEach
    public void tearDownTest(){
        parent = null;
        info = null;
    }

    @Test
    public void testUpdateAccountSubGroupCheckParameterForNullException(){
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), null),
        "NullPointerException is not thrown.");

    }
    @Test
    public void testUpdateAccountSubGroupCheckUuidForNullException(){
        info = new AccountSubGroupInfo(parent.getId(), "For UpdateTest");
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(null, info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateAccountSubGroupcheckStringForNullException(){
        info = new AccountSubGroupInfo(parent.getId(), null);
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), info),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testUpdateAccountSubGroupCheckAndGetEntityByIdException(){
        info = new AccountSubGroupInfo(parent.getId(),"For UpdateTest");
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(null),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(entity.getId(), info),
        "NoSuchElementException is not thrown.");
    }

    @Test
    public void testUpdateAccountSubGroupCheckEntityWithSameNameException(){
        info = new AccountSubGroupInfo(parent.getId(),"For UpdateTest");
        AccountSubGroup subGroupSameName = new AccountSubGroup();
        subGroupSameName.setName(info.getName());
        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity);
        when(accountSubGroupDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(subGroupSameName)));
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
        "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateAccountSubGroupPositive(){
        info = new AccountSubGroupInfo(parent.getId(),"For UpdateTest", "It's only for test", true);

        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity);
        ArgumentCaptor<AccountSubGroup> entityCaptor = ArgumentCaptor.forClass(AccountSubGroup.class);
        doNothing().when(accountSubGroupDataAccess).update(entityCaptor.capture());
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());

        service.update(entity.getId(), info);

        AccountSubGroup capturedEntity = entityCaptor.getValue();
        Assertions.assertEquals(info.getName(), capturedEntity.getName(), "Name is not passed to updated AccountSubGroup.");
        Assertions.assertEquals(info.getDescription(), capturedEntity.getDescription(), "Description is not passed to updated AccountSubGroup.");
        Assertions.assertTrue(capturedEntity.getIsFavorite());
    }
}

package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class MoveAccountSubGroupTests {

    private AccountGroup fromParent;
    private AccountGroup toParent;
    private AccountSubGroup entity;
    private final int MAX_ORDER = 7;

    @BeforeEach
    public void setUpTest() {
        fromParent = AccountSubGroupTestsCommon.createAccountGroup();
        entity = AccountSubGroupTestsCommon.createAccountSubGroup();
        entity.setParent(fromParent);
        fromParent.getChildren().add(entity);
        toParent = AccountSubGroupTestsCommon.createAccountGroup();
    }

    @AfterEach
    public void tearDownTest() {
        fromParent = null;
        toParent = null;
        entity = null;
    }

    @Test
    public void testMoveAccountSubGroupCheckUuidForNullEntityException() {
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(null, toParent.getId()),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testMoveAccountSubGroupCheckUuidForNullParentException() {
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(entity.getId(), null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testMoveAccountSubGroupCheckAndGetEntityByIdException() {
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(null),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testMoveAccountSubGroupCheckAndGetParentByIdException() {
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(null));
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testMoveAccountSubGroupCheckEntityWithSameNameException() {
        AccountSubGroup subGroupSameName = AccountSubGroupTestsCommon.createAccountSubGroup();
        subGroupSameName.setName(entity.getName());
        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity);
        when(accountSubGroupDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(subGroupSameName)));

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(toParent));
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testMoveAccountSubGroupPositive() {
        AccountSubGroup first = AccountSubGroupTestsCommon.createAccountSubGroup();
        first.setOrder(1);
        first.setParent(fromParent);
        AccountSubGroup third = AccountSubGroupTestsCommon.createAccountSubGroup();
        third.setOrder(1);
        third.setId(UUID.randomUUID());
        third.setParent(fromParent);
        fromParent.getChildren().add(first);
        fromParent.getChildren().add(third);
        int childrenSizeFromParentBefore = fromParent.getChildren().size();
        int childrenSizeToParentBefore = toParent.getChildren().size();

        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity);
        when(accountSubGroupDataAccess
                .getMaxOrder(Mockito.any(UUID.class)))
                .thenReturn(MAX_ORDER);
        ArgumentCaptor<AccountSubGroup> argumentCaptor = ArgumentCaptor.forClass(AccountSubGroup.class);
        doNothing().when(accountSubGroupDataAccess).update(argumentCaptor.capture());

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(toParent));
        service.moveToAnotherParent(entity.getId(), toParent.getId());
        AccountSubGroup capturedEntity = argumentCaptor.getValue();
        Integer reOrdereThird = null;
        for (AccountSubGroup group: fromParent.getChildren()) {
            if (group.getId().equals(third.getId())){
                reOrdereThird = group.getOrder();
            }
        }

        Assertions.assertEquals(MAX_ORDER + 1, capturedEntity.getOrder(), "Order is not change for entity");
        Assertions.assertEquals(toParent.getId(), capturedEntity.getParent().getId(), "Parent is not changed into entity");
        Assertions.assertEquals(childrenSizeFromParentBefore - 1, fromParent.getChildren().size(), "FromParent doesn't delete the entity");
        Assertions.assertEquals(childrenSizeToParentBefore + 1, toParent.getChildren().size(), "ToParent doesn't added the entity");
        Assertions.assertEquals(2, reOrdereThird, "The reOdered method is not done.");
    }


}

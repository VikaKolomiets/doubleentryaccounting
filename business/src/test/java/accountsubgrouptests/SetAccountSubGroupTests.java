package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class SetAccountSubGroupTests {

    private AccountSubGroup entity;
    private final int ORDER = 2;
    @BeforeEach
    public void setUpTest(){
        entity = AccountSubGroupTestsCommon.createAccountSubGroup();
    }
    @AfterEach
    public void tearDownTest(){
        entity = null;
    }

    @Test
    public void testSetFavoriteAccountSubGroupCheckUuidForNullException(){
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setFavoriteStatus(null, true),
                "NullPointerException is not thrown.");

    }
    @Test
    public void testSetFavoriteAccountSubGroupCheckAndGetEntityByIdException(){
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(null),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setFavoriteStatus(entity.getId(), true),
                "NoSuchElementException is not thrown.");

    }

    @Test
    public void testSetFavoriteAccountSubGroupPositive(){
        boolean iaFavoriteBefore = entity.getIsFavorite();
        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity);
        ArgumentCaptor<AccountSubGroup> entityCaptor = ArgumentCaptor.forClass(AccountSubGroup.class);
        doNothing().when(accountSubGroupDataAccess).update(entityCaptor.capture());
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        service.setFavoriteStatus(entity.getId(), true);
        AccountSubGroup capturedEntity  = entityCaptor.getValue();

        Assertions.assertTrue(capturedEntity.getIsFavorite(), "Parameter in update method is not passed with true.");
        Assertions.assertEquals(true, capturedEntity.getIsFavorite(), "Parameter in update method is not passed with true.");
        Assertions.assertNotEquals(iaFavoriteBefore, capturedEntity.getIsFavorite(), "Parameter in update method is not passed with changed boolean.");
    }

    @Test
    public void testSetOrderAccountSubGroupCheckUuidForNullException(){
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setOrder(null, ORDER),
                "NullPointerException is not thrown.");

    }
    @Test
    public void testSetOrderAccountSubGroupCheckAndGetEntityByIdException(){
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(null),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setOrder(entity.getId(), ORDER),
                "NoSuchElementException is not thrown.");

    }

    @Test
    public void testSetOrderAccountSubGroupPositive(){
        AccountGroup parent = AccountSubGroupTestsCommon.createAccountGroup();
        AccountSubGroup first = AccountSubGroupTestsCommon.createAccountSubGroup();
        first.setOrder(1);
        first.setParent(parent);
        AccountSubGroup third = AccountSubGroupTestsCommon.createAccountSubGroup();
        third.setOrder(2);
        third.setParent(parent);
        entity.setParent(parent);
        parent.getChildren().add(first);
        parent.getChildren().add(third);
        parent.getChildren().add(entity);

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());

        service.setOrder(entity.getId(), ORDER);
        Integer actualOrder = null;
        for (AccountSubGroup child: parent.getChildren()){
            if(child.getId().equals(entity.getId())){
                actualOrder = child.getOrder();
                break;
            }
        }

        Assertions.assertEquals(ORDER, actualOrder);
        Assertions.assertEquals(ORDER + 1, third.getOrder());
        Assertions.assertEquals(ORDER - 1, first.getOrder());
    }

}

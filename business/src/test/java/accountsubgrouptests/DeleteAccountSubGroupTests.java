package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteAccountSubGroupTests {
    private AccountGroup parent;
    private AccountSubGroup entity;
    @BeforeEach
    public void setUpTest(){
        parent = AccountSubGroupTestsCommon.createAccountGroup();
        entity = AccountSubGroupTestsCommon.createAccountSubGroup();
        entity.setParent(parent);
        parent.getChildren().add(entity);
    }

    @AfterEach
    public void tearDownTest(){
        parent = null;
        entity = null;
    }

    @Test
    public void testDeleteAccountSubGroupcheckUuidForNullException(){
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
        "NullPointerException is not thrown.");
    }

    @Test
    public void testDeleteAccountSubGroupCheckAndGetEntityByIdException(){
       AccountSubGroupService service = new AccountSubGroupService(
               AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
               AccountSubGroupTestsCommon.createMockAccountDataAccess(),
               AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(null),
               AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(entity.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testDeleteAccountSubGroupCheckExistedChildrenInTheGroupException(){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getCount(Mockito.any(UUID.class))).thenReturn(2);
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccessReturnEntity(entity),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.delete(entity.getId()),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testDeleteAccountSubGroupPositive() {
        entity.setOrder(2);
        AccountSubGroup first = AccountSubGroupTestsCommon.createAccountSubGroup();
        first.setParent(parent);
        first.setOrder(1);
        AccountSubGroup third = AccountSubGroupTestsCommon.createAccountSubGroup();
        third.setParent(parent);
        third.setOrder(3);
        parent.getChildren().add(first);
        parent.getChildren().add(third);
        int childrenSizeBefore = parent.getChildren().size();
        IAccountSubGroupDataAccess accountSubGroupDataAccess = AccountSubGroupTestsCommon
                .createMockAccountSubGroupDataAccessReturnEntity(entity);
        ArgumentCaptor<AccountSubGroup> entityCaptor = ArgumentCaptor.forClass(AccountSubGroup.class);
        doNothing().when(accountSubGroupDataAccess).delete(entityCaptor.capture());

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccess());
        service.delete(entity.getId());
        AccountSubGroup capturedEntity = entityCaptor.getValue();
        Assertions.assertEquals(entity.getId(), capturedEntity.getId(), "Entity is not passed to delete method as a paramenter.");
        Assertions.assertEquals(entity.getOrder(), third.getOrder(), "Reodered method is not done.");
        Assertions.assertEquals(childrenSizeBefore - 1, parent.getChildren().size(), "Number of children is not cut for deleted entity.");
    }
}

package accountsubgrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AddAccountSubGroupTests {
    private AccountGroup parent;
    private AccountSubGroupInfo info;
    private final int MAX_ORDER = 4;

    @BeforeEach
    public void setUpTest() {
        parent = AccountSubGroupTestsCommon.createAccountGroup();
    }

    @AfterEach
    public void tearDownTest() {
        info = null;
        parent = null;
    }

    @Test
    public void testAddAccountSubGroupCheckParameterForNullException() {
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(null),
                "NullPointerException is not thrown");
    }

    @Test
    public void testAddAccountSubGroupCheckStringForNullException() {
        info = new AccountSubGroupInfo(parent.getId(), null);
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(info),
                "NullPointerException is not thrown");
    }

    @Test
    public void testAddAccountSubGroupCheckUuidForNullException() {
        info = new AccountSubGroupInfo(null, "InfoTest");
        AccountSubGroupService service = AccountSubGroupTestsCommon.createAccountSubGroupService();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(info),
                "NullPointerException is not thrown");
    }

    @Test
    public void testAddAccountSubGroupCheckAndGetEntityByIdException() {
        info = new AccountSubGroupInfo(parent.getId(), "InfoTest");

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(null));

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(info),
        "NoSuchElementException is not thrown.");
    }
    @Test
    public void testAddAccountSubGroupCheckEntityWithSameNameException(){
        info = new AccountSubGroupInfo(parent.getId(), "InfoTest");
        AccountSubGroup subGroupSameName = AccountSubGroupTestsCommon.createAccountSubGroup();
        subGroupSameName.setName(info.getName());

        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        when(accountSubGroupDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(subGroupSameName)));
        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(parent));

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(info),
        "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testAddAccountSubGroupPositive(){
        info = new AccountSubGroupInfo(parent.getId(), "InfoTest", "Description for test", true);
        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        when(accountSubGroupDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);
        ArgumentCaptor<AccountSubGroup> entityCaptor = ArgumentCaptor.forClass(AccountSubGroup.class);
        doNothing().when(accountSubGroupDataAccess).add(entityCaptor.capture());

        AccountSubGroupService service = new AccountSubGroupService(
                AccountSubGroupTestsCommon.createMockGlobalDataAccess(),
                AccountSubGroupTestsCommon.createMockAccountDataAccess(),
                accountSubGroupDataAccess,
                AccountSubGroupTestsCommon.createMockAccountGroupDataAccessReturnEntity(parent));
        service.add(info);
        AccountSubGroup capturedEntity = entityCaptor.getValue();
        Assertions.assertEquals(MAX_ORDER + 1, capturedEntity.getOrder(), "Order is not given to added entity");
        Assertions.assertEquals(info.getName(), capturedEntity.getName(), "AccountSubGroup is not added, name is not given");
        Assertions.assertEquals(parent.getId(), capturedEntity.getParent().getId());
        Assertions.assertEquals(info.getDescription(), capturedEntity.getDescription());
        Assertions.assertTrue(capturedEntity.getIsFavorite());
    }

}

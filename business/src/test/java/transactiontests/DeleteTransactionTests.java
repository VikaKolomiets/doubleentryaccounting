package transactiontests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteTransactionTests {
    private Transaction entity;

    @BeforeEach
    public void setUpTest(){
        entity = TransactionTestsCommon.createTransactionForTests();
    }
    @AfterEach
    public void tearDown(){
        entity = null;
    }
    @Test
    public void testDeleteTransactionCheckUuidForNullException(){
        TransactionService service = TransactionTestsCommon.createMockTransactionServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testDeleteTransactionCheckAndGetEntityByIdException(){
        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(null),
                TransactionTestsCommon.createMockAccountDataAccess(),
                TransactionTestsCommon.createMockSystemConfigDataAccess()
        );
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(entity.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testDeleteTransactionPositive(){
        ITransactionDataAccess transactionDataAccess = TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity);
        ArgumentCaptor<Transaction> argumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        doNothing().when(transactionDataAccess).delete(argumentCaptor.capture());
        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), transactionDataAccess,
                TransactionTestsCommon.createMockAccountDataAccess(),
                TransactionTestsCommon.createMockSystemConfigDataAccess()
        );
        service.delete(entity.getId());
        Transaction capturedTransaction  = argumentCaptor.getValue();
        Assertions.assertEquals(entity.getId(), capturedTransaction.getId(), "Method delete is not gotten entity as a parameter for delete");
    }

}

package transactiontests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.models.enums.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.math.*;
import java.time.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateTransactionTests {
    private Transaction entity;
    private TransactionInfo info;
    @BeforeEach
    public void setUpTest(){
        entity = TransactionTestsCommon.createTransactionForTests();

    }
    @AfterEach
    public void tearDownTest(){
        entity = null;
        info = null;
    }
    @Test
    public void testUpdateTransactionCheckParameterForNullException(){
        TransactionService service = TransactionTestsCommon.createMockTransactionServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), null),
                "NullPointerException is not thrown");
    }
    @Test
    public void testUpdateTransactionCheckDateTimeForNullException() {
        info = new TransactionInfo(null, TransactionState.Draft, new ArrayList<>());
        TransactionService service = TransactionTestsCommon.createMockTransactionServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckDateTimeLessMinException() {
        info = new TransactionInfo(LocalDateTime.of(2020, 01, 01, 00, 00), TransactionState.Draft, new ArrayList<>());
        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2020, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2025, 01, 01, 00, 11));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccess(),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckDateTimeMoreMaxException() {
        info = new TransactionInfo(LocalDateTime.of(2020, 01, 01, 00, 12), TransactionState.Draft, new ArrayList<>());

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2020, 01, 01, 00, 11));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccess(),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckEntriesForNullException() {
        info = new TransactionInfo(LocalDateTime.of(2022, 01, 01, 00, 12), TransactionState.Draft, new ArrayList<>());

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccess(),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckEntriesLessTwoException() {
        TransactionEntryInfo first = new TransactionEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(12.29), null);
        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 00, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccess(),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckAndGetEntityByIdException() {
        TransactionEntryInfo first = new TransactionEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.000));
        TransactionEntryInfo second = new TransactionEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(-12.29), BigDecimal.valueOf(1.000));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(null),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(entity.getId(), info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckRateToLessZeroException() {
        TransactionEntryInfo first = new TransactionEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(-1.000));
        TransactionEntryInfo second = new TransactionEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(-12.29), BigDecimal.valueOf(-1.000));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckAccountUuidForNullException() {
        TransactionEntryInfo first = new TransactionEntryInfo(null, BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.000));
        TransactionEntryInfo second = new TransactionEntryInfo(null, BigDecimal.valueOf(-12.29), BigDecimal.valueOf(1.000));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity),
                TransactionTestsCommon.createMockAccountDataAccess(),
                systemConfigDataAccess
        );

        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckAndGetEntityByIdAccountException() {
        Account accountFirst = new Account();
        accountFirst.setId(UUID.randomUUID());
        Account accountThird = new Account();
        accountThird.setId(UUID.randomUUID());
        TransactionEntryInfo first = new TransactionEntryInfo(accountFirst.getId(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.000));
        TransactionEntryInfo second = new TransactionEntryInfo(accountThird.getId(), BigDecimal.valueOf(-12.29), BigDecimal.valueOf(1.000));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        IAccountDataAccess accountDataAccess = TransactionTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity),
                accountDataAccess,
                systemConfigDataAccess
        );

        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateTransactionCheckCurrencyIsoCodeWithMainException() {
        Currency currency = new Currency();
        currency.setIsoCode("UAH");
        Account accountFirst = new Account();
        accountFirst.setId(UUID.randomUUID());
        accountFirst.setCurrency(currency);
        Account accountThird = new Account();
        accountThird.setId(UUID.randomUUID());
        accountThird.setCurrency(currency);
        TransactionEntryInfo first = new TransactionEntryInfo(accountFirst.getId(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.001));
        TransactionEntryInfo second = new TransactionEntryInfo(accountThird.getId(), BigDecimal.valueOf(-12.29), BigDecimal.valueOf(1.001));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        IAccountDataAccess accountDataAccess = TransactionTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(accountFirst, accountThird);

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity),
                accountDataAccess,
                systemConfigDataAccess
        );

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTransactionSumZeroPositive() {
        Currency currency = new Currency();
        currency.setIsoCode("UAH");
        Account accountFirst = new Account();
        accountFirst.setId(UUID.randomUUID());
        accountFirst.setCurrency(currency);
        Account accountThird = new Account();
        accountThird.setId(UUID.randomUUID());
        accountThird.setCurrency(currency);
        TransactionEntryInfo first = new TransactionEntryInfo(accountFirst.getId(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.00));
        TransactionEntryInfo second = new TransactionEntryInfo(accountThird.getId(), BigDecimal.valueOf(-12.29), BigDecimal.valueOf(1.00));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        IAccountDataAccess accountDataAccess = TransactionTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(accountFirst, accountThird);

        ITransactionDataAccess transactionDataAccess = TransactionTestsCommon
                .createMockTransactionDataAccessReturnTransaction(entity);
        ArgumentCaptor<Transaction> argumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        doNothing().when(transactionDataAccess).update(argumentCaptor.capture());

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), transactionDataAccess,
                accountDataAccess,
                systemConfigDataAccess
        );
        service.update(entity.getId(), info);
        Transaction capturedTransaction = argumentCaptor.getValue();

        Assertions.assertEquals(info.getDateTime().getYear(),
                capturedTransaction.getDateTime().getYear(),
                "Parameter in add method doesn't contain datatime from TransationInfo.");
        Assertions.assertEquals(info.getState(),
                capturedTransaction.getState(),
                "Parameter in add method doesn't contain status from TransationInfo.");
        Assertions.assertEquals(info.getEntries().size(),
                capturedTransaction.getEntries().size(),
                "Parameter in add method doesn't contain list of entrie.");
    }
    @Test
    public void testUpdateTransactionSumNotZeroPositive() {
        Currency currency = new Currency();
        currency.setIsoCode("UAH");
        Account accountFirst = new Account();
        accountFirst.setId(UUID.randomUUID());
        accountFirst.setCurrency(currency);
        Account accountThird = new Account();
        accountThird.setId(UUID.randomUUID());
        accountThird.setCurrency(currency);
        TransactionEntryInfo first = new TransactionEntryInfo(accountFirst.getId(), BigDecimal.valueOf(12.29), BigDecimal.valueOf(1.00));
        TransactionEntryInfo second = new TransactionEntryInfo(accountThird.getId(), BigDecimal.valueOf(-122.29), BigDecimal.valueOf(1.00));

        info = new TransactionInfo(LocalDateTime.of(
                2022, 01, 01, 10, 12)
                , TransactionState.Draft
                , new ArrayList<>(List.of(first, second)));

        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        when(systemConfigDataAccess.getMinDateTime()).thenReturn(LocalDateTime.of(2000, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMaxDateTime()).thenReturn(LocalDateTime.of(2023, 01, 01, 00, 11));
        when(systemConfigDataAccess.getMainCurrencyIsoCode()).thenReturn("UAH");

        IAccountDataAccess accountDataAccess = TransactionTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(accountFirst, accountThird);

        ITransactionDataAccess transactionDataAccess = TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(entity);
        ArgumentCaptor<Transaction> argumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        doNothing().when(transactionDataAccess).update(argumentCaptor.capture());

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), transactionDataAccess,
                accountDataAccess,
                systemConfigDataAccess
        );
        service.update(entity.getId(), info);
        Transaction capturedTransaction = argumentCaptor.getValue();

        Assertions.assertNotEquals(info.getState(),
                capturedTransaction.getState(),
                "Transaction.State is not changed when sum is not zero..");
        Assertions.assertEquals(TransactionState.NoValid,
                capturedTransaction.getState(),
                "Transaction.State is not changed for NoValid when sum is not zero.");
    }


}

package transactiontests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.models.enums.*;
import org.mockito.*;

import java.time.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class TransactionTestsCommon {

    public static TransactionEntry createTransactionEntryFforTest(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        firstAccount.setName("first");
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());
        thirdAccount.setName("first");
        TransactionEntry entry = new TransactionEntry();
        entry.setId(UUID.randomUUID());
        entry.setAccount(firstAccount);
        entry.setAccount(thirdAccount);
        return entry;
    }
    public static Transaction createTransactionForTests(){
        TransactionEntry first = createTransactionEntryFforTest();
        TransactionEntry third = createTransactionEntryFforTest();

        Transaction transaction = new Transaction();
        transaction.setId(UUID.randomUUID());
        transaction.setState(TransactionState.Draft);
        transaction.setComment("Transaction comment");
        transaction.setDateTime(LocalDateTime.of(2022,05,15, 10,33));
        transaction.getEntries().add(first);
        transaction.getEntries().add(third);

        return transaction;
    }

    public static ITransactionDataAccess createMockTransactionDataAccess() {
        ITransactionDataAccess transactionDataAccess = Mockito.mock(ITransactionDataAccess.class);
        return transactionDataAccess;
    }
    public static ITransactionDataAccess createMockTransactionDataAccessReturnTransaction(Transaction entity) {
        ITransactionDataAccess transactionDataAccess = Mockito.mock(ITransactionDataAccess.class);
        when(transactionDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(transactionDataAccess.getType()).thenReturn(Transaction.class);
        return transactionDataAccess;
    }


    public static IAccountDataAccess createMockAccountDataAccess() {
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }

    public static ISystemConfigDataAccess createMockSystemConfigDataAccess() {
        ISystemConfigDataAccess systemConfigDataAccess = Mockito.mock(ISystemConfigDataAccess.class);
        return systemConfigDataAccess;
    }

    public static IGlobalDataAccess createMockGlobalDataAccess() {
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }

    public static TransactionService createMockTransactionServiceForTests() {
        TransactionService transactionService = new TransactionService(
                createMockGlobalDataAccess(), createMockTransactionDataAccess(),
                createMockAccountDataAccess(),
                createMockSystemConfigDataAccess()
        );
        return transactionService;
    }


}

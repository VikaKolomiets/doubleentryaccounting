package transactiontests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteTransactionListTests {

    private ArrayList<Transaction> entities;

    @Test
    public void testDeleteTransactionListCheckListForNullException(){
        TransactionService service = TransactionTestsCommon.createMockTransactionServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.deleteTransactionList(null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testDeleteTransactionListCheckAndGetEntityByIdException(){
        Transaction first = TransactionTestsCommon.createTransactionForTests();
        Transaction second = TransactionTestsCommon.createTransactionForTests();
        Transaction third = TransactionTestsCommon.createTransactionForTests();
        ArrayList<Transaction> listTransactions = new ArrayList<>(List.of(first, second, third));
        ArrayList<UUID> listTransactionIds = new ArrayList<>(List.of(first.getId(), second.getId(), third.getId()));

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), TransactionTestsCommon.createMockTransactionDataAccessReturnTransaction(null),
                TransactionTestsCommon.createMockAccountDataAccess(),
                TransactionTestsCommon.createMockSystemConfigDataAccess()
        );
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.deleteTransactionList(listTransactionIds),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testDeleteTransactionListPositive(){
        Transaction first = TransactionTestsCommon.createTransactionForTests();
        Transaction second = TransactionTestsCommon.createTransactionForTests();
        Transaction third = TransactionTestsCommon.createTransactionForTests();
        ArrayList<Transaction> listTransactions = new ArrayList<>(List.of(first, second, third));
        ArrayList<UUID> listTransactionIds = new ArrayList<>(List.of(first.getId(), second.getId(), third.getId()));

        ITransactionDataAccess transactionDataAccess = TransactionTestsCommon.createMockTransactionDataAccess();
        when(transactionDataAccess.get(Mockito.any(UUID.class))).thenReturn(first, second, third);
        ArgumentCaptor<ArrayList<Transaction>> argumentCaptor = ArgumentCaptor.forClass( ArrayList.class);
        doNothing().when(transactionDataAccess).deleteList(argumentCaptor.capture());

        TransactionService service = new TransactionService(
                TransactionTestsCommon.createMockGlobalDataAccess(), transactionDataAccess,
                TransactionTestsCommon.createMockAccountDataAccess(),
                TransactionTestsCommon.createMockSystemConfigDataAccess()
        );
        service.deleteTransactionList(listTransactionIds);
        ArrayList<Transaction> capturedList = argumentCaptor.getValue();

        Assertions.assertEquals(listTransactions.size(), capturedList.size());
        Assertions.assertEquals(first.getId(), capturedList.get(0).getId());
    }
}

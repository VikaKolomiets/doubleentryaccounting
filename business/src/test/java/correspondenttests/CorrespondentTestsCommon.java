package correspondenttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CorrespondentTestsCommon {

    public static Correspondent createCorrespondentForTest(){
        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());
        correspondent.setName("Child");
        correspondent.setOrder(2);
        correspondent.setIsFavorite(true);
        return correspondent;
    }

    public static CorrespondentGroup createCorrespondentGroupWithChildren(){
        CorrespondentGroup group = new CorrespondentGroup();
        group.setId(UUID.randomUUID());
        group.setName("Family");

        Correspondent firstEntity = new Correspondent();
        firstEntity.setId(UUID.randomUUID());
        firstEntity.setName("Father");
        firstEntity.setOrder(1);
        firstEntity.setParent(group);
        Correspondent thirdEntity = new Correspondent();
        thirdEntity.setId(UUID.randomUUID());
        thirdEntity.setName("Mother");
        thirdEntity.setOrder(3);
        thirdEntity.setParent(group);

        group.getChildren().add(firstEntity);
        group.getChildren().add(thirdEntity);
        return  group;
    }

    public static ICorrespondentGroupDataAccess createCorrespondentGroupDataAccess(){
        ICorrespondentGroupDataAccess correspondentGroupDataAccess = Mockito.mock(ICorrespondentGroupDataAccess.class);
        return correspondentGroupDataAccess;
    }

    public static IGlobalDataAccess createGlobalDataAccess(){
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }
    public static ICorrespondentDataAccess createCorrespondentDataAccess(Correspondent entity){
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(correspondentDataAccess.getType()).thenReturn(Correspondent.class);
        return correspondentDataAccess;
    }

    public static IAccountDataAccess createAccountDataAccess(){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }
    public static CorrespondentService createCorrespondentServiceForTest(Correspondent entity){
        CorrespondentService service = new CorrespondentService(
                CorrespondentTestsCommon.createGlobalDataAccess(),
                CorrespondentTestsCommon.createCorrespondentDataAccess(entity),
                CorrespondentTestsCommon.createCorrespondentGroupDataAccess(),
                CorrespondentTestsCommon.createAccountDataAccess());
        return service;
    }
    public static ArrayList<Account> createAccountForTest(Correspondent entity){
        ArrayList<Account> accounts = new ArrayList<>();
        Account accountFirst = new Account();
        accountFirst.setName("Sister");
        accountFirst.setCorrespondent(entity);
        accounts.add(accountFirst);
        Account accountSecond = new Account();
        accountSecond.setName("Brother");
        accountSecond.setCorrespondent(entity);
        accounts.add(accountSecond);
        return accounts;
    }


}

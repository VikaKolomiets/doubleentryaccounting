package correspondenttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteCorrespondentTests {
    private Correspondent deletedEntity;
    private CorrespondentGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = CorrespondentTestsCommon.createCorrespondentGroupWithChildren();
        deletedEntity = CorrespondentTestsCommon.createCorrespondentForTest();
        deletedEntity.setName("Name Delete");
        deletedEntity.setParent(parent);
        parent.getChildren().add(deletedEntity);
    }
    @AfterEach
    public void tearDown(){
        parent = null;
        deletedEntity = null;
    }

    @Test
    public void testDeleteCorrespondentCheckUuidForNullException(){
        CorrespondentService service = CorrespondentTestsCommon.createCorrespondentServiceForTest(deletedEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testDeleteCorrespondentCheckAndGetEntityByIdException(){
        CorrespondentService service = CorrespondentTestsCommon.createCorrespondentServiceForTest(null);

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(deletedEntity.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testDeleteCorrespondentPositive(){
        int sizeParentChildrenBefore = parent.getChildren().size();

        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(deletedEntity);
        ArgumentCaptor<Correspondent> deleteEntityCaptor = ArgumentCaptor.forClass(Correspondent.class);
        doNothing().when(correspondentDataAccess).delete(deleteEntityCaptor.capture());

        ArrayList<Account> accounts = CorrespondentTestsCommon.createAccountForTest(deletedEntity);
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByCorrespondent(Mockito.any(Correspondent.class))).thenReturn(accounts);
        ArgumentCaptor<Account> updateAccountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(updateAccountCaptor.capture());

        CorrespondentService service = new CorrespondentService(
                CorrespondentTestsCommon.createGlobalDataAccess(),
                correspondentDataAccess,
                CorrespondentTestsCommon.createCorrespondentGroupDataAccess(),
                accountDataAccess);
        service.delete(deletedEntity.getId());

        Correspondent deletedEntityCaptored = deleteEntityCaptor.getValue();
        Account updatedAccountCaptored = updateAccountCaptor.getValue();

        Assertions.assertEquals(
                sizeParentChildrenBefore - 1,
                parent.getChildren().size(),
                "Parent does not delete deleted entity");
        Assertions.assertEquals(
                deletedEntity.getId(),
                deletedEntityCaptored.getId(),
                "Parameter in delete method is not equal to deleteEntity");
        Assertions.assertNull(
                updatedAccountCaptored.getCorrespondent(),
                "Account is not switch for null correspondent field, which was deleted");
        Assertions.assertEquals(2, parent.getChildren().get(1).getOrder());
    }
}

package correspondenttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CombineCorrespondentTests {
    private Correspondent primaryEntity;
    private Correspondent secondaryEntity;
    private CorrespondentGroup parent;
    private final int ORDER = 4;


    @BeforeEach
    public void setUpTest(){
        parent = CorrespondentTestsCommon.createCorrespondentGroupWithChildren();
        primaryEntity = CorrespondentTestsCommon.createCorrespondentForTest();
        primaryEntity.setName("Primary");
        primaryEntity.setOrder(ORDER);
        primaryEntity.setParent(parent);
        parent.getChildren().add(primaryEntity);
        secondaryEntity = CorrespondentTestsCommon.createCorrespondentForTest();
        secondaryEntity.setName("Secondary");
        secondaryEntity.setParent(parent);
        parent.getChildren().add(secondaryEntity);
    }
    @AfterEach
    public void tearDouw(){
        primaryEntity = null;
        secondaryEntity = null;
        parent = null;
    }
    @Test
    public void testCombineCorrespondentNullPrimaryEntityException(){
        CorrespondentService service = CorrespondentTestsCommon.createCorrespondentServiceForTest(primaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(null, secondaryEntity.getId()),
              "NullPointerException is not thrown.");
    }

    @Test
    public void testCombineCorrespondentNullSecondaryEntityException(){
        CorrespondentService service = CorrespondentTestsCommon.createCorrespondentServiceForTest(primaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testCombineCorrespondentCheckAndGetFirstEntityByIdException(){
        CorrespondentService service = CorrespondentTestsCommon.createCorrespondentServiceForTest(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown");
    }

    @Test
    public void testCombineCorrespondentCheckAndGetSecondEntityByIdException(){
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, (Correspondent) null);
        when(correspondentDataAccess.getType()).thenReturn(Correspondent.class);

        CorrespondentService service = new CorrespondentService(
                CorrespondentTestsCommon.createGlobalDataAccess(),
                correspondentDataAccess,
                CorrespondentTestsCommon.createCorrespondentGroupDataAccess(),
                CorrespondentTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown");
    }

    @Test
    public void testCombineCorrespondentPositive(){
        int sizeParentChildrenBefore = parent.getChildren().size();
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, secondaryEntity);
        ArgumentCaptor<Correspondent> deleteEntityCaptor = ArgumentCaptor.forClass(Correspondent.class);
        doNothing().when(correspondentDataAccess).delete(deleteEntityCaptor.capture());

        ArrayList<Account> accounts = CorrespondentTestsCommon.createAccountForTest(secondaryEntity);

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByCorrespondent(Mockito.any(Correspondent.class))).thenReturn(accounts);
        ArgumentCaptor<Account> updateAccountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(updateAccountCaptor.capture());

        CorrespondentService service = new CorrespondentService(
                CorrespondentTestsCommon.createGlobalDataAccess(),
                correspondentDataAccess,
                CorrespondentTestsCommon.createCorrespondentGroupDataAccess(),
                accountDataAccess);
        service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId());

        Correspondent deletedEntityCaptured = deleteEntityCaptor.getValue();
        Account updatedAccountCaptured = updateAccountCaptor.getValue();

        Assertions.assertEquals(
                primaryEntity.getId(),
                updatedAccountCaptured.getCorrespondent().getId(),
                "Added accounts from secondaryEntity is not change Correspondent");
        Assertions.assertEquals(
                secondaryEntity.getId(),
                deletedEntityCaptured.getId(),
                "The parameter for Delete method is not Secondary");
        Assertions.assertEquals(
                sizeParentChildrenBefore - 1,
                parent.getChildren().size(),
                "SecondaryEntity is not deleted from parent");
        Assertions.assertNotEquals(ORDER, primaryEntity.getOrder(), "ORDER is not changed after reordering");
    }
}

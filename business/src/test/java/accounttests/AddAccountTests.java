package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AddAccountTests {

    private AccountSubGroup parent;
    private final int MAX_ORDER = 7;
    private AccountInfo entityInfo;
    @BeforeEach
    public void setUpTest(){
        parent = AccountTestsCommon.createParentWithoutChildren();

    }
    @AfterEach
    void tearDown() {
        parent = null;
        entityInfo = null;
    }

    @Test
    public void testAddAccountCheckParameterForNullException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(null),
        "NullPointerException is not thrown");
    }
    @Test
    public void testAddAccountCheckStringForNullException(){
        entityInfo = new AccountInfo(parent.getId(), null, UUID.randomUUID());

        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(entityInfo),
                "NullPointerException is not thrown");
    }
    @Test
    public void testAddAccountCheckParentUuidForNullException(){
        entityInfo = new AccountInfo(null, "Add Account", UUID.randomUUID());

        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(entityInfo),
                "NullPointerException is not thrown");
    }
    @Test
    public void testAddAccountCheckCurrencyUuidForNullException(){
        entityInfo = new AccountInfo(parent.getId(), "Add Account", null);

        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(entityInfo),
                "NullPointerException is not thrown");
    }

    @Test
    public void testAddAccountCheckAndGetEntityByIdParentException() {
        entityInfo = new AccountInfo(parent.getId(),"Add Account",UUID.randomUUID());
        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccess(),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(null),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(entityInfo),
                "NoSuchElementException is not thrown");
    }

    @Test
    public void testAddAccountCheckEntityWithSameNameException(){
        entityInfo = new AccountInfo(parent.getId(),"Add Account",UUID.randomUUID());
        Account sameNameAccount = new Account();
        sameNameAccount.setName(entityInfo.getName());

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class))).thenReturn(new ArrayList<>(List.of(sameNameAccount)));

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(entityInfo),
                "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testAddAccountCheckAndGetEntityByIdCurrencyException(){
        entityInfo = new AccountInfo(parent.getId(),"Add Account",UUID.randomUUID());

        ICurrencyDataAccess currencyDataAccess = Mockito.mock(ICurrencyDataAccess.class);
        when(currencyDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(currencyDataAccess.getType()).thenReturn(Currency.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccess(MAX_ORDER),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                currencyDataAccess);

        Assertions.assertThrows(
                NoSuchElementException.class,
                () ->service.add(entityInfo),
                "NoSuchElementException is not thrown");
    }
    @Test
    public void testAddAccountCheckAndGetEntityByIdCategoryException(){
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");
        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("Premium");

        entityInfo = new AccountInfo(
                parent.getId(),
                "Add Account",
                currency.getId(),
                category.getId(),
                null,
                null,
                "Description exists",
                true);

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(categoryDataAccess.getType()).thenReturn(Category.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccess(MAX_ORDER),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                categoryDataAccess,
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccessReturnCurrency(currency));

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(entityInfo),
        "NoSuchElementException is not thrown.");
    }
    @Test
    public void testAddAccountCheckAndGetEntityByIdProjectException(){
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");
        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName("Vacation");

        entityInfo = new AccountInfo(
                parent.getId(),
                "Add Account",
                currency.getId(),
                null,
                null,
                project.getId(),
                "Description exists",
                true);

        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(projectDataAccess.getType()).thenReturn(Project.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccess(MAX_ORDER),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                projectDataAccess,
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccessReturnCurrency(currency));

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(entityInfo),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testAddAccountCheckAndGetEntityByIdCorrespondentException(){
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");
        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());
        correspondent.setName("Dad");

        entityInfo = new AccountInfo(
                parent.getId(),
                "Add Account",
                currency.getId(),
                null,
                correspondent.getId(),
                null,
                "Description exists",
                true);

        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(correspondentDataAccess.getType()).thenReturn(Correspondent.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccess(MAX_ORDER),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                correspondentDataAccess,
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccessReturnCurrency(currency));

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(entityInfo),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testAddAccountPositive(){
        int childrenSizeBefore = parent.getChildren().size();
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");

        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("Premium");

        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName("Vacation");

        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());
        correspondent.setName("Dad");

        entityInfo = new AccountInfo(
                parent.getId(),
                "Add Account",
                currency.getId(),
                category.getId(),
                correspondent.getId(),
                project.getId(),
                "Description exists",
                true);

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).add(accountCaptor.capture());

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(category);

        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(project);

        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(correspondent);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(parent),
                categoryDataAccess,
                projectDataAccess,
                correspondentDataAccess,
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccessReturnCurrency(currency));

        service.add(entityInfo);

        Account accountCaptured = accountCaptor.getValue();

        Assertions.assertEquals(
                childrenSizeBefore + 1,
                parent.getChildren().size(),
                "Parent doesn't increase size of Children");
       Assertions.assertEquals(
               entityInfo.getName(),
               accountCaptured.getName(),
               "Name is not passed fron addedEntityInfor");
        Assertions.assertEquals(
                MAX_ORDER + 1,
                accountCaptured.getOrder(),
                "Order is not changed");
        Assertions.assertEquals(
                entityInfo.getCurrencyId(),
                accountCaptured.getCurrency().getId(),
                "Currency is not passed fron addedEntityInfo");
        Assertions.assertEquals(
                entityInfo.getCategoryId(),
                accountCaptured.getCategory().getId(),
                "Categoryis not passed fron addedEntityInfo");
        Assertions.assertEquals(
                entityInfo.getProjectId(),
                accountCaptured.getProject().getId(),
                "Project is not passed fron addedEntityInfo");
        Assertions.assertEquals(
                entityInfo.getCorrespondentId(),
                accountCaptured.getCorrespondent().getId(),
                "Correspondent is not passed fron addedEntityInfo");
        Assertions.assertTrue(accountCaptured.getIsFavorite());
    }
}

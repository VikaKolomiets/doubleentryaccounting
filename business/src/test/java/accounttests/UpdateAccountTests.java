package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateAccountTests {
    private AccountSubGroup parent;
    private Account updatedEntity;
    private AccountInfo info;

    @BeforeEach
    public void setUpTest(){
        parent = AccountTestsCommon.createParentWithoutChildren();
        updatedEntity = AccountTestsCommon.createAccount();
        updatedEntity.setParent(parent);
        parent.getChildren().add(updatedEntity);
    }
    @AfterEach
    public void tearDownTest(){
        parent = null;
        info = null;
    }

    @Test
    public void testUpdateAccountCheckParameterForNullException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updatedEntity.getId(), null),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateAccountCheckUuidForNullException(){
        info = new AccountInfo(parent.getId(), "UpdatedInfo", updatedEntity.getCurrency().getId());
        updatedEntity.setId(null);
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testUpdateAccountCheckStringForNullException(){
        info = new AccountInfo(parent.getId(), null, updatedEntity.getCurrency().getId());
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testUpdateAccountCheckAndGetEntityByIdException(){
        info = new AccountInfo(parent.getId(), "UpdatedInfo", updatedEntity.getCurrency().getId());
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateAccountCheckEntityWithSameNameException(){
        info = new AccountInfo(parent.getId(), "UpdatedInfo", updatedEntity.getCurrency().getId());
        Account sameNameAccount = new Account();
        sameNameAccount.setName(info.getName());
        sameNameAccount.setId(UUID.randomUUID());

        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(updatedEntity);
        when(accountDataAccess.
                getByName(Mockito.any(UUID.class), Mockito.any(String.class))).
                thenReturn(new ArrayList<>(List.of(sameNameAccount)));

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(updatedEntity.getId(), info),
        "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateAccountCheckAndGetEntityByIdCategoryException(){
        Category category = new Category();
        category.setId(UUID.randomUUID());

        info = new AccountInfo(
                parent.getId(),"UpdatedInfo",updatedEntity.getCurrency().getId(),
                category.getId(),null,null,
                "For Test", true);
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(categoryDataAccess.getType()).thenReturn(Category.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(updatedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                categoryDataAccess,
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateAccountCheckAndGetEntityByIdProjectException(){
        Project project = new Project();
        project.setId(UUID.randomUUID());

        info = new AccountInfo(
                parent.getId(),"UpdatedInfo",updatedEntity.getCurrency().getId(),
                null,null, project.getId(),
                "For Test", true);
        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(projectDataAccess.getType()).thenReturn(Project.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(updatedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                projectDataAccess,
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateAccountCheckAndGetEntityByIdCorrespondentException(){
        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());

        info = new AccountInfo(
                parent.getId(),"UpdatedInfo",updatedEntity.getCurrency().getId(),
                null,correspondent.getId(), null,
                "For Test", true);
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(correspondentDataAccess.getType()).thenReturn(Correspondent.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(updatedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                correspondentDataAccess,
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updatedEntity.getId(), info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateAccountPositive(){
        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("updated Category");
        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());
        correspondent.setName("updated Correspondent");
        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName("updated Project");

        info = new AccountInfo(
                parent.getId(),"UpdatedInfo",updatedEntity.getCurrency().getId(),
                category.getId(),correspondent.getId(), project.getId(),
                "For Test", true);
        IAccountDataAccess accountDataAccess =  AccountTestsCommon.createMockAccountDataAccessReturnAccount(updatedEntity);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountCaptor.capture());

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(category);
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        when(correspondentDataAccess.get(Mockito.any(UUID.class))).thenReturn(correspondent);
        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(project);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                categoryDataAccess,
                projectDataAccess,
                correspondentDataAccess,
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        service.update(updatedEntity.getId(), info);
        Account capturedAccount = accountCaptor.getValue();

        Assertions.assertEquals(info.getName(), capturedAccount.getName(), "Name is not passed into updated Account");
        Assertions.assertEquals(info.getCorrespondentId(), capturedAccount.getCorrespondent().getId(), "Correspondent is not passed into updated Account");
        Assertions.assertEquals(info.getCategoryId(), capturedAccount.getCategory().getId(), "Category is not passed into updated Account");
        Assertions.assertEquals(info.getProjectId(), capturedAccount.getProject().getId(), "ProjectI is not passed into updated Account");
        Assertions.assertEquals(info.getDescription(), capturedAccount.getDescription(), "Description is not passed into updated Account");
        Assertions.assertTrue((capturedAccount.getIsFavorite()));
    }
}

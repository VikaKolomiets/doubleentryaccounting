package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class MoveAccountTests {
    private Account movedEntity;
    private AccountSubGroup fromParent;
    private AccountSubGroup toParent;
    private final int MAX_ORDER = 7;

    @BeforeEach
    public void setUpTest(){
        fromParent = AccountTestsCommon.createParentWithChildren();
        movedEntity = AccountTestsCommon.createAccount();
        movedEntity.setParent(fromParent);
        fromParent.getChildren().add(movedEntity);
        toParent = AccountTestsCommon.createParentWithoutChildren();
    }
    @AfterEach
    public void tearDownTest(){
        fromParent = null;
        toParent = null;
        movedEntity = null;
    }
    @Test
    public void testMoveAccountCheckUuidForNullEntityException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(null, toParent.getId()),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testMoveAccountCheckUuidForNullParentException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), null),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testMoveAccountCheckAndGetEntityByIdException(){
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), toParent.getId()),
        "NoSuchElementException is not thrown.");
    }
    @Test
    public void testMoveAccountCheckAndGetEntityParentByIdException() {
        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(movedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(null),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), toParent.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testMoveAccountCheckEntityWithSameNameException() {
        Account sameName = new Account();
        sameName.setName(movedEntity.getName());
        sameName.setId(UUID.randomUUID());

        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(movedEntity);
        when(accountDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<Account>((List.of(sameName))));

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(toParent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), toParent.getId()),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testMoveAccountPositive() {
        int sizeToParentBefore = toParent.getChildren().size();
        int sizeFromParentBefore = fromParent.getChildren().size();
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(movedEntity);
        when(accountDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);
        ArgumentCaptor<Account> argumentCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(argumentCaptor.capture());

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccessReturnParent(toParent),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        service.moveToAnotherParent(movedEntity.getId(), toParent.getId());
        Account capturedEntity = argumentCaptor.getValue();
        Assertions.assertEquals(
                sizeToParentBefore + 1,
                toParent.getChildren().size(),
        "Size of Children is not increased in toParent group");
        Assertions.assertEquals(
                sizeFromParentBefore - 1,
                fromParent.getChildren().size(),
                "Size of Children is not decreased in fromParent group");
        Assertions.assertEquals(
                toParent.getId(),
                capturedEntity.getParent().getId(),
        "Parent is not changed in Moved Entity");

    }
}

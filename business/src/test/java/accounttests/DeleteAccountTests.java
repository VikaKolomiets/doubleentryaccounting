package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteAccountTests {
    private AccountSubGroup parent;
    private Account deletedEntity;

    @BeforeEach
    public void setUpTest(){
        parent = AccountTestsCommon.createParentWithChildren();
        deletedEntity = AccountTestsCommon.createAccount();
        deletedEntity.setParent(parent);
        parent.getChildren().add(deletedEntity);
    }
    @AfterEach
    public void tearDownTest(){
        deletedEntity = null;
        parent = null;
    }

    @Test
    public void testDeleteAccountCheckUuidForNullException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
        "NullPointerException is not thrown.");
    }

    @Test
    public void testDeleteAccountCheckAndGetEntityByIdException(){
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(deletedEntity.getId()),
        "NoSuchElementException is not thrown.");
    }

    @Test
    public void testDeleteAccountTransactionEntriesCountException(){
        ITransactionDataAccess transactionDataAccess = Mockito.mock(ITransactionDataAccess.class);
        when(transactionDataAccess.getTransactionEntriesCount(Mockito.any(UUID.class))).thenReturn(2);
        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(deletedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                transactionDataAccess,
                AccountTestsCommon.createMockCurrencyDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.delete(deletedEntity.getId()),
        "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testDeleteAccountTemplateEntriesCountException(){
        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        when(templateDataAccess.getTemplateEntriesCount(Mockito.any(UUID.class))).thenReturn(2);
        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                AccountTestsCommon.createMockAccountDataAccessReturnAccount(deletedEntity),
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                templateDataAccess,
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.delete(deletedEntity.getId()),
                "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testDeleteAccountPositive(){
        int sizeChildrenBefore = parent.getChildren().size();
        int orderMaxChildBefore = parent.getChildren().get(1).getOrder();
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(deletedEntity);
        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).delete(accountArgumentCaptor.capture());

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        service.delete(deletedEntity.getId());
        Account capturedAccount = accountArgumentCaptor.getValue();
        Assertions.assertEquals(sizeChildrenBefore - 1, parent.getChildren().size(), "Number of children ia not changed after delete child.");
        Assertions.assertEquals(deletedEntity.getId(), capturedAccount.getId(), "Deleted entity is not passed as a parament to delete method.");
        Assertions.assertNotEquals(orderMaxChildBefore, parent.getChildren().get(1).getOrder());
        Assertions.assertEquals(orderMaxChildBefore - 1, parent.getChildren().get(1).getOrder());
    }

}

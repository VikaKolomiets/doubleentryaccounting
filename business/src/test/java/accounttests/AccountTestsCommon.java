package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AccountTestsCommon {

    public static Account createAccount(){
        Account account = new Account();
        account.setId(UUID.randomUUID());
        account.setName("Test");
        account.setOrder(2);
        account.setIsFavorite(false);
        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName("Vacation");
        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("Premium");
        Correspondent correspondent = new Correspondent();
        correspondent.setId(UUID.randomUUID());
        correspondent.setName("Dad");
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID());
        currency.setIsoCode("UAH");

        account.setCategory(category);
        account.setProject(project);
        account.setCorrespondent(correspondent);
        account.setCurrency(currency);
        return account;
    }
    public static AccountSubGroup createParentWithoutChildren(){
        AccountSubGroup subgroup = new AccountSubGroup();
        subgroup.setId(UUID.randomUUID());
        subgroup.setName("SubGroup");
        return subgroup;
    }
    public static AccountSubGroup createParentWithChildren(){
        AccountSubGroup subgroup = new AccountSubGroup();
        subgroup.setId(UUID.randomUUID());
        subgroup.setName("SubGroup");

        Account first = new Account();
        first.setId(UUID.randomUUID());
        first.setName("First");
        first.setOrder(1);
        first.setParent(subgroup);

        Account third = new Account();
        third.setId(UUID.randomUUID());
        third.setName("Third");
        third.setOrder(3);
        third.setParent(subgroup);

        subgroup.getChildren().add(first);
        subgroup.getChildren().add(third);

        return subgroup;
    }

    public static IGlobalDataAccess createMockGlobalDataAccess() {
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }

    public static IAccountDataAccess createMockAccountDataAccess() {
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }
    public static IAccountDataAccess createMockAccountDataAccessReturnAccount(Account entity) {
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(accountDataAccess.getType()).thenReturn(Account.class);
        return accountDataAccess;
    }

    public static IAccountDataAccess createMockAccountDataAccess(int maxOrder) {
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(maxOrder);
        return accountDataAccess;
    }
    public static IAccountSubGroupDataAccess createMockAccountSubGroupDataAccess() {
        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        return accountSubGroupDataAccess;
    }
    public static IAccountSubGroupDataAccess createMockAccountSubGroupDataAccessReturnParent(AccountSubGroup entity){
        IAccountSubGroupDataAccess accountSubGroupDataAccess = Mockito.mock(IAccountSubGroupDataAccess.class);
        when(accountSubGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(accountSubGroupDataAccess.getType()).thenReturn(AccountSubGroup.class);
        return accountSubGroupDataAccess;
    }
    public static ICategoryDataAccess createMockCategoryDataAccess() {
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        return categoryDataAccess;
    }
    public static IProjectDataAccess createMockProjectDataAccess() {
        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        return projectDataAccess;
    }
    public static ICorrespondentDataAccess createMockCorrespondentDataAccess() {
        ICorrespondentDataAccess correspondentDataAccess = Mockito.mock(ICorrespondentDataAccess.class);
        return correspondentDataAccess;
    }

    public static ITemplateDataAccess createMockTemplateDataAccess() {
        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        return templateDataAccess;
    }

    public static ITransactionDataAccess createMockTransactionDataAccess() {
        ITransactionDataAccess transactionDataAccess = Mockito.mock(ITransactionDataAccess.class);
        return transactionDataAccess;
    }
    public static ICurrencyDataAccess createMockCurrencyDataAccess() {
        ICurrencyDataAccess currencyDataAccess = Mockito.mock(ICurrencyDataAccess.class);
        return currencyDataAccess;
    }
    public static ICurrencyDataAccess createMockCurrencyDataAccessReturnCurrency(Currency entity) {
        ICurrencyDataAccess currencyDataAccess = Mockito.mock(ICurrencyDataAccess.class);
        when(currencyDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        return currencyDataAccess;
    }
    public static AccountService createAccountServiceForTest() {
        AccountService service = new AccountService(
                createMockGlobalDataAccess(),
                createMockAccountDataAccess(),
                createMockAccountSubGroupDataAccess(),
                createMockCategoryDataAccess(),
                createMockProjectDataAccess(),
                createMockCorrespondentDataAccess(),
                createMockTemplateDataAccess(),
                createMockTransactionDataAccess(),
                createMockCurrencyDataAccess());
        return service;
    }
    public static AccountService createAccountServiceForReturnAccount(Account entity){
        AccountService service = new AccountService(
                createMockGlobalDataAccess(),
                createMockAccountDataAccessReturnAccount(entity),
                createMockAccountSubGroupDataAccess(),
                createMockCategoryDataAccess(),
                createMockProjectDataAccess(),
                createMockCorrespondentDataAccess(),
                createMockTemplateDataAccess(),
                createMockTransactionDataAccess(),
                createMockCurrencyDataAccess());
        return service;
    }

}

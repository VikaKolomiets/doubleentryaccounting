package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CombineAccountTests {
    private Account primaryEntity;
    private Account secondaryEntity;

    private AccountSubGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = AccountTestsCommon.createParentWithoutChildren();

        primaryEntity = AccountTestsCommon.createAccount();
        primaryEntity.setName("Setter");
        primaryEntity.setParent(parent);

        secondaryEntity = AccountTestsCommon.createAccount();
        secondaryEntity.setName("Getter");
        secondaryEntity.setParent(parent);

        parent.getChildren().add(primaryEntity);
        parent.getChildren().add(secondaryEntity);
    }
    @AfterEach
    public void tearDownTest(){
        parent = null;
        primaryEntity = null;
        secondaryEntity = null;
    }

    @Test
    public void testCombineAccountCheckUuidForNullPrimaryException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(null, secondaryEntity.getId()),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testCombineAccountCheckUuidForNullSecondaryException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testCombineAccountcheckAndGetEntityByIdPrimaryException(){
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testCombineAccountcheckAndGetEntityByIdSecondaryException(){
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, (Account) null);
        when(accountDataAccess.getType()).thenReturn(Account.class);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testCombineAccountCheckCurrencyException(){
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, secondaryEntity);

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testCombineAccountPositive(){
        secondaryEntity.setCurrency(primaryEntity.getCurrency());
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, secondaryEntity);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).delete(accountCaptor.capture());

        Template template = new Template();
        template.setName("Big");
        TemplateEntry templateEntry = new TemplateEntry();
        templateEntry.setAccount(secondaryEntity);
        templateEntry.setTemplate(template);
        template.getEntries().add(templateEntry);

        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        when(templateDataAccess
                .getEntriesByAccount(Mockito.any(Account.class)))
                .thenReturn(new ArrayList<>(List.of(templateEntry)));
        ArgumentCaptor<Template> templateCaptor = ArgumentCaptor.forClass(Template.class);
        doNothing().when(templateDataAccess).update(templateCaptor.capture());

        Transaction transaction = new Transaction();
        transaction.setId(UUID.randomUUID());
        TransactionEntry transactionEntry = new TransactionEntry();
        transactionEntry.setAccount(secondaryEntity);
        transactionEntry.setTransaction(transaction);
        transaction.getEntries().add(transactionEntry);

        ITransactionDataAccess transactionDataAccess = Mockito.mock(ITransactionDataAccess.class);
        when(transactionDataAccess
                .getEntriesByAccount(Mockito.any(Account.class)))
                .thenReturn(new ArrayList<>(List.of(transactionEntry)));
        ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);
        doNothing().when(transactionDataAccess).update(transactionCaptor.capture());

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                templateDataAccess,
                transactionDataAccess,
                AccountTestsCommon.createMockCurrencyDataAccess());

        service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId());
        Template capturedTemplate = templateCaptor.getValue();
        Transaction capturedTransaction = transactionCaptor.getValue();

        Account capturedAccount = accountCaptor.getValue();

        Assertions.assertEquals(secondaryEntity.getId(), capturedAccount.getId(), "SecondaryEntity is not deleted when combine two entities");
        Assertions.assertEquals(
                primaryEntity.getId(),
                capturedTemplate.getEntries().get(0).getAccount().getId(),
        "Template is not changed account to primary");
        Assertions.assertEquals(
                primaryEntity.getId(),
                capturedTransaction.getEntries().get(0).getAccount().getId(),
                "Transaction is not changed account to primary");

    }
}

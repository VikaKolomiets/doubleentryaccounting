package accounttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class SetAccountTests {
    private Account setEntity;
    private AccountSubGroup parent;
    private final int ORDER = 5;
    @BeforeEach
    public void setUpTest(){
        setEntity = AccountTestsCommon.createAccount();
    }
     @AfterEach
    public void tearDownTest(){
        setEntity = null;
        parent = null;
    }
    @Test
    public void testSetFavoriteStatusAccountCheckUuidForNulException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setFavoriteStatus(null, true),
        "NullPointerException is not thrown");
    }
    @Test
    public void testSetFavoriteStatusAccountCheckAndGetEntityByIdException(){
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setFavoriteStatus(setEntity.getId(), true),
                "NoSuchElementException is not thrown");
    }
    @Test
    public void testSetFavoriteStatusAccountPositive() {
        Boolean isFavoriteBefore = setEntity.getIsFavorite();
        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(setEntity);
        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountArgumentCaptor.capture());

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        service.setFavoriteStatus(setEntity.getId(), true);
        Account capturedAccount = accountArgumentCaptor.getValue();

        Assertions.assertNotEquals(isFavoriteBefore, capturedAccount.getIsFavorite(), "IsFavorite is not changed");
        Assertions.assertTrue(capturedAccount.getIsFavorite());
    }
    @Test
    public void testSetOrderStatusAccountCheckUuidForNulException(){
        AccountService service = AccountTestsCommon.createAccountServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setOrder(null, ORDER),
                "NullPointerException is not thrown");
    }
    @Test
    public void testSetOrderStatusAccountCheckAndGetEntityByIdException(){
        AccountService service = AccountTestsCommon.createAccountServiceForReturnAccount(null);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setOrder(setEntity.getId(), ORDER),
                "NoSuchElementException is not thrown");
    }
    @ParameterizedTest()
    @ValueSource(ints = {1,3})
    public void testSetOrderStatusAccountPositive(int order) {
        int orderBefore = setEntity.getOrder();
        parent = AccountTestsCommon.createParentWithChildren();
        parent.getChildren().add(setEntity);
        setEntity.setParent(parent);

        IAccountDataAccess accountDataAccess = AccountTestsCommon.createMockAccountDataAccessReturnAccount(setEntity);
        ArgumentCaptor<ArrayList<Account>> accountsArgumentCaptor = ArgumentCaptor.forClass(ArrayList.class);
        doNothing().when(accountDataAccess).updateList(accountsArgumentCaptor.capture());

        AccountService service = new AccountService(
                AccountTestsCommon.createMockGlobalDataAccess(),
                accountDataAccess,
                AccountTestsCommon.createMockAccountSubGroupDataAccess(),
                AccountTestsCommon.createMockCategoryDataAccess(),
                AccountTestsCommon.createMockProjectDataAccess(),
                AccountTestsCommon.createMockCorrespondentDataAccess(),
                AccountTestsCommon.createMockTemplateDataAccess(),
                AccountTestsCommon.createMockTransactionDataAccess(),
                AccountTestsCommon.createMockCurrencyDataAccess());

        service.setOrder(setEntity.getId(), order);
        ArrayList<Account> capturedAccounts = accountsArgumentCaptor.getValue();
        int orderAfter = 0;
        for (Account account: capturedAccounts) {
            if (account.getId() == setEntity.getId()){
                orderAfter = account.getOrder();
                break;
            }
        }
        Assertions.assertEquals(order, orderAfter, "SetOrder does not change the Order.");
        Assertions.assertNotEquals(orderBefore, orderAfter, "Order is not changed.");
    }
}

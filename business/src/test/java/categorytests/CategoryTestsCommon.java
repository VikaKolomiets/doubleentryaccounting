package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CategoryTestsCommon {

    public static Category createCategory() {
        Category setEntity = new Category();
        setEntity.setId(UUID.randomUUID());
        setEntity.setName("Name");
        setEntity.setDescription("");
        setEntity.setIsFavorite(true);
        setEntity.setOrder(2);
        return setEntity;
    }

    public static CategoryGroup createCategoryGroupWithChildren() {
        CategoryGroup parent = new CategoryGroup();
        parent.setId(UUID.randomUUID());
        parent.setName("Parent");
        Category firstChild = new Category();
        firstChild.setName("FirstChild");
        firstChild.setDescription("");
        firstChild.setIsFavorite(true);
        firstChild.setOrder(1);
        firstChild.setParent(parent);

        Category thirdChild = new Category();
        thirdChild.setName("ThirdChild");
        thirdChild.setDescription("");
        thirdChild.setIsFavorite(false);
        thirdChild.setOrder(3);
        thirdChild.setParent(parent);
        parent.getChildren().add(firstChild);
        parent.getChildren().add(thirdChild);
        return parent;
    }

    public static ICategoryDataAccess createCategoryDataAccess(Category setEntity) {
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(setEntity);
        when(categoryDataAccess.getType()).thenReturn(Category.class);
        return categoryDataAccess;
    }

    public static ICategoryDataAccess createCategoryDataAccess() {
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        return categoryDataAccess;
    }


    public static ICategoryGroupDataAccess createCategoryGroupDataAccess() {
        return Mockito.mock(ICategoryGroupDataAccess.class);
    }

    public static IGlobalDataAccess createGlobalDataAccess() {
        return Mockito.mock(IGlobalDataAccess.class);
    }

    public static IAccountDataAccess createAccountDataAccess() {
        return Mockito.mock(IAccountDataAccess.class);
    }

    public static ArrayList<Account> createAccountForTest(Category category){
        ArrayList<Account> accounts = new ArrayList<>();
        Account accountFirst = new Account();
        accountFirst.setName("Mother");
        accountFirst.setCategory(category);
        accounts.add(accountFirst);
        Account accountSecond = new Account();
        accountSecond.setName("Dad");
        accountSecond.setCategory(category);
        accounts.add(accountSecond);
        return accounts;
    }
    public static CategoryService createCategoryServiceForTests(Category entity) {
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(entity),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        return service;
    }

    public static CategoryService createCategoryServiceForTests() {
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        return service;
    }
}

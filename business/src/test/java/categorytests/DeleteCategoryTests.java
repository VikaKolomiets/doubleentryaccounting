package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteCategoryTests {

    private Category deletedEntity;
    private CategoryGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = CategoryTestsCommon.createCategoryGroupWithChildren();
        deletedEntity = CategoryTestsCommon.createCategory();
        deletedEntity.setParent(parent);
        parent.getChildren().add(deletedEntity);
    }
    @AfterEach
    public void tearDouw(){
        deletedEntity = null;
        parent = null;
    }

    @Test
    public void testDeleteCategoryPositive(){
        ArrayList<Account> accounts =  CategoryTestsCommon.createAccountForTest(deletedEntity);
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByCategory(Mockito.any(Category.class))).thenReturn(accounts);

        ArgumentCaptor<Account> accountFirst = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountFirst.capture());

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(deletedEntity),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                accountDataAccess);
        int expectedChildrenSize = parent.getChildren().size() - 1;
        service.delete(deletedEntity.getId());
        Account accountFirstCaptured = accountFirst.getValue();

        Assertions.assertEquals(expectedChildrenSize, parent.getChildren().size(), "Parend doesn't remove deletedEntity");
        Assertions.assertNull(accountFirstCaptured.getCategory(), "there is not changed Category to null in Account");
    }
    @Test
    public void testDeleteCategoryCheckUuidForNullException(){
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(deletedEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testDeleteCategoryCheckAndGetEntityByIdException(){
        ICategoryDataAccess categoryDataAccess = CategoryTestsCommon.createCategoryDataAccess(null);
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(deletedEntity.getId()),
                "NoSuchElementException is not thrown");
    }

}

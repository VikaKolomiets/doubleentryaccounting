package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import java.util.*;

public class SetCategoryTests {

    private Category setEntity;
    private CategoryGroup parent;

    @BeforeEach
    public void SetUpCategoryTest() {
        setEntity = CategoryTestsCommon.createCategory();
        parent = CategoryTestsCommon.createCategoryGroupWithChildren();
        setEntity.setParent(parent);
        parent.getChildren().add(setEntity);
    }

    @AfterEach
    public void tearDownCategoryTest() {
        setEntity = null;
        parent = null;
    }

    @Test
    public void testSetFavoriteStatusCategoryPositive() {
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(setEntity);

        service.setFavoriteStatus(setEntity.getId(), false);
        Assertions.assertFalse(setEntity.getIsFavorite());
    }

    @Test
    public void testSetFavoriteStatusCategoryUuidNullException() {
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(setEntity);

        service.setFavoriteStatus(setEntity.getId(), false);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setFavoriteStatus(null, false),
                "NullPointerException is not thrown");
    }

    @Test
    public void testSetFavoriteStatusCategoryCheckAndGetEntityByIdException() {

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(null),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setFavoriteStatus(setEntity.getId(), false),
                "NoSuchElementException isn't thrown");
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3})
    public void testSetOrderCategoryPositive(int order) {
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(setEntity);

        service.setOrder(setEntity.getId(), order);
        Assertions.assertEquals(order, setEntity.getOrder(), "Method setOrder doesn't change order");
    }

    @Test
    public void testSetOrderCategoryCheckUuidForNullException() {
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(setEntity);

        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setOrder(null, 5),
                "NullPointerException is not thrown");
    }

    @Test
    public void testSetOrderCategoryCheckEntityForNullException() {
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(null),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setOrder(setEntity.getId(), 5),
                "NullPointerException is not thrown");
    }


}


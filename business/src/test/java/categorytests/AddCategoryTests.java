package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AddCategoryTests {
    private CategoryGroup parent;
    private final Integer MAX_ORDER = 7;
    private ElementEntityInfo entityInfo;

    @BeforeEach
    public void SetUpCategoryTest() {
        parent = CategoryTestsCommon.createCategoryGroupWithChildren();
    }

    @AfterEach
    public void tearDownCategoryTest() {
        parent = null;
        entityInfo = null;
    }

    @Test
    public void testAddCategoryCheckParameterForNullException(){
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(null),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testAddCategoryCheckStringForNullException(){
        entityInfo = new ElementEntityInfo(UUID.randomUUID(),null);
        //parent.setId(entityInfo.getParentId());
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(entityInfo),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testAddCategoryCheckUuidForNullException(){
        entityInfo = new ElementEntityInfo(null,"addedName");
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(entityInfo),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testAddCategoryCheckAndGetParentByIdException() {
        entityInfo = new ElementEntityInfo(parent.getId(),"addedName");

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(categoryGroupDataAccess.getType()).thenReturn(CategoryGroup.class);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(),
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(entityInfo),
                "NoSuchElementException is not thrown");
    }
    @Test
    public void testAddCategoryCheckEntityWithSameNameException(){
        entityInfo = new ElementEntityInfo(parent.getId(),"addedName");

        Category sameName = new Category();
        sameName.setName(entityInfo.getName());
        sameName.setId(UUID.randomUUID());

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class))).thenReturn(new ArrayList<>(List.of(sameName)));

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(parent);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(entityInfo),
                "IllegalArgumentException is not thrown");
    }

    @Test
    public void testAddCategoryPositive(){
        entityInfo = new ElementEntityInfo(parent.getId(), "addedName", "Description", true);

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class))).thenReturn(new ArrayList<>());
        when(categoryDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);

        ArgumentCaptor<Category> addedEntityCapture = ArgumentCaptor.forClass(Category.class);
        doNothing().when(categoryDataAccess).add(addedEntityCapture.capture());

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(parent);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());

        int expectedChildrenSize = parent.getChildren().size() + 1;

        service.add(entityInfo);

        Category entityCapture = addedEntityCapture.getValue();

        Assertions.assertEquals(expectedChildrenSize, parent.getChildren().size(), "parent doesn't add an Entity");
        Assertions.assertEquals(MAX_ORDER + 1, entityCapture.getOrder(), "Entity is not change order");
        Assertions.assertEquals(entityInfo.getName(), entityCapture.getName(), "Category is not  passed as parameter into add() method");
        Assertions.assertEquals(entityInfo.getDescription(), entityCapture.getDescription(), "Category is not  passed as parameter into add() method");
        Assertions.assertEquals(entityInfo.getParentId(), entityCapture.getParent().getId(), "Category is not  passed as parameter into add() method");
        Assertions.assertEquals(entityInfo.getIsFavorite(), entityCapture.getIsFavorite(), "Category is not  passed as parameter into add() method");
    }
}

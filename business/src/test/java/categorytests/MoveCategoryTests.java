package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class MoveCategoryTests {
    private Category movedEntity;
    private CategoryGroup parentFrom;
    private CategoryGroup parentTo;
    private final int MAX_ORDER = 7;

    @BeforeEach
    public void setUpTest(){
        parentFrom = CategoryTestsCommon.createCategoryGroupWithChildren();
        movedEntity = CategoryTestsCommon.createCategory();
        movedEntity.setParent(parentFrom);
        parentFrom.getChildren().add(movedEntity);
        parentTo = CategoryTestsCommon.createCategoryGroupWithChildren();
        parentTo.setName("ParentTo");
        parentTo.getChildren().get(1).setOrder(2);

    }
    @AfterEach
    public void tearDouw(){
        movedEntity = null;
        parentFrom = null;
        parentTo = null;
    }

    @Test
    public void testMoveCategorycheckUuidForNullException(){
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(movedEntity),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(null, parentTo.getId()),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testMoveCategoryCheckAndGetEntityByIdException(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(categoryDataAccess.getType()).thenReturn(Category.class);
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), parentTo.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testMoveCategoryCheckAndGetEntityByIdParentException(){
        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(null);
        when(categoryGroupDataAccess.getType()).thenReturn(CategoryGroup.class);
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(movedEntity),
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), parentTo.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testMoveCategoryCheckEntityWithSameNameException(){
        Category sameNameCategory = new Category();
        sameNameCategory.setName(movedEntity.getName());
        ArrayList<Category> categories = new ArrayList<>(List.of(sameNameCategory));

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(movedEntity);
        when(categoryDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class))).thenReturn(categories);

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(parentTo);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.moveToAnotherParent(movedEntity.getId(), parentTo.getId()),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testMoveCategoryPositive(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(movedEntity);
        when(categoryDataAccess.getByName(Mockito.any(UUID.class), Mockito.any(String.class))).thenReturn(new ArrayList<Category>());
        when(categoryDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);

        ArgumentCaptor<Category> categoryCapture = ArgumentCaptor.forClass(Category.class);
        doNothing().when(categoryDataAccess).update(categoryCapture.capture());

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(parentTo);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                categoryGroupDataAccess,
                CategoryTestsCommon.createAccountDataAccess());

        int sizeParentToBefore = parentTo.getChildren().size();
        int sizeParentFromBefore = parentFrom.getChildren().size();

        service.moveToAnotherParent(movedEntity.getId(), parentTo.getId());

        Category movedEntityCaptured = categoryCapture.getValue();

        Assertions.assertEquals(sizeParentFromBefore - 1, parentFrom.getChildren().size(), "Children are not changed in ParentFrom");
        Assertions.assertEquals(sizeParentToBefore + 1, parentTo.getChildren().size(), "number of children are not changed ");
        Assertions.assertEquals(MAX_ORDER + 1, movedEntityCaptured.getOrder(), "Order is not changed for moved Entity");
        Assertions.assertEquals(parentTo, movedEntityCaptured.getParent(), "Parent is not changed for movedEntity");
    }









}

package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateCategoryTests {
    private Category updatedEntity;
    private CategoryGroup parent;
    private ElementEntityInfo entityInfo;

    @BeforeEach
    public void setUpTest(){
        updatedEntity = CategoryTestsCommon.createCategory();
        parent = CategoryTestsCommon.createCategoryGroupWithChildren();
        updatedEntity.setParent(parent);
        parent.getChildren().add(updatedEntity);
    }
    @AfterEach
    public void tearDown(){
        updatedEntity = null;
        parent = null;
        entityInfo = null;
    }

    @Test
    public void testUpdateCategoryCheckParameterForNullException(){
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updatedEntity.getId(), null),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateCategoryCheckUuidForNullException(){
        entityInfo = new ElementEntityInfo(parent.getId(),"Updated Name");
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(null, entityInfo),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateCategorycheckStringForNullException(){
        entityInfo = new ElementEntityInfo(parent.getId(),null);
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updatedEntity.getId(), entityInfo),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateCategoryCheckAndGetEntityByIdException() {
        entityInfo = new ElementEntityInfo(parent.getId(), "Updated Name");
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                CategoryTestsCommon.createCategoryDataAccess(null),
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updatedEntity.getId(), entityInfo),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testUpdateCategoryCheckEntityWithSameNameException(){
        entityInfo = new ElementEntityInfo(parent.getId(), "Updated Name");
        Category sameName = new Category();
        sameName.setName(entityInfo.getName());
        sameName.setId(UUID.randomUUID());

        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(updatedEntity);
        when(categoryDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(java.util.List.of(sameName)));
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(updatedEntity.getId(), entityInfo),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateCategoryPositive(){

        entityInfo = new ElementEntityInfo(parent.getId(), "Updated Name", "Description", true);
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(updatedEntity);
        ArgumentCaptor<Category> categoryCaptor = ArgumentCaptor.forClass(Category.class);
        doNothing().when(categoryDataAccess).update(categoryCaptor.capture());

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        service.update(updatedEntity.getId(), entityInfo);

        Category categoryCaptored = categoryCaptor.getValue();

        Assertions.assertEquals(entityInfo.getName(), categoryCaptored.getName(), "Update doesn't change the Name.");
        Assertions.assertEquals(entityInfo.getDescription(), categoryCaptored.getDescription(), "Update doesn't change Description.");
        Assertions.assertEquals(entityInfo.getIsFavorite(), categoryCaptored.getIsFavorite(), "Update doesn't change IsFavorite.");
    }
}

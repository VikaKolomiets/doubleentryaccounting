package categorytests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CombineCategoryTests {
    private Category primaryEntity;
    private Category secondaryEntity;
    private CategoryGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = CategoryTestsCommon.createCategoryGroupWithChildren();
        primaryEntity = CategoryTestsCommon.createCategory();
        primaryEntity.setName("Primary");
        primaryEntity.setParent(parent);
        parent.getChildren().add(primaryEntity);
        secondaryEntity = CategoryTestsCommon.createCategory();
        secondaryEntity.setName("Secondary");
        secondaryEntity.setParent(parent);
        parent.getChildren().add(secondaryEntity);
    }
    @AfterEach
    public void tearDouw(){
        primaryEntity = null;
        secondaryEntity = null;
        parent = null;
    }
    @Test
    public void testCombineCategoryPositive(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, secondaryEntity);
        ArgumentCaptor<Category> categoryCaptor = ArgumentCaptor.forClass(Category.class);
        doNothing().when(categoryDataAccess).delete(categoryCaptor.capture());

        ArrayList<Account> accounts =  CategoryTestsCommon.createAccountForTest(secondaryEntity);
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByCategory(Mockito.any(Category.class))).thenReturn(accounts);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountCaptor.capture());

        int parentSizeBefore = parent.getChildren().size();

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                accountDataAccess);

        service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId());

        Category deletedCategoryCaptured = categoryCaptor.getValue();
        Account accountCaptored = accountCaptor.getValue();

        Assertions.assertEquals(primaryEntity, accountCaptored.getCategory(), "There is not changed Category in Account.");
        Assertions.assertEquals(parentSizeBefore - 1, parent.getChildren().size(), "Parent does not remove the secondaryEntity");
        Assertions.assertEquals(secondaryEntity.getName(), deletedCategoryCaptured.getName(), "SecondaryEntity wasn't passed to delete from CategoryDataAccess");
    }

    @Test
    public void testCombineCategoryCheckUuidForNullPrimaryEntityException(){
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(primaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(null, secondaryEntity.getId()),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testCombineCategoryCheckUuidForNullSecondaryEntityException(){
        CategoryService service = CategoryTestsCommon.createCategoryServiceForTests(primaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testCombineCategoryCheckAndGetEntityByIdPrimaryException(){
        ICategoryDataAccess categoryDataAccess = CategoryTestsCommon.createCategoryDataAccess(null);
        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown");
    }
    @Test
    public void testCombineCategoryCheckAndGetEntityByIdSecondaryException(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, (Category) null);
        when(categoryDataAccess.getType()).thenReturn(Category.class);

        CategoryService service = new CategoryService(
                CategoryTestsCommon.createGlobalDataAccess(),
                categoryDataAccess,
                CategoryTestsCommon.createCategoryGroupDataAccess(),
                CategoryTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown");
    }
}

package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class TemplateTestsCommon {

    public static ArrayList<TemplateEntryInfo> createTemplateEntryInfoList() {
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(4.6));
        TemplateEntryInfo thirdTemplateEntryInfo = new TemplateEntryInfo(thirdAccount.getId(), BigDecimal.valueOf(-10.0));

        ArrayList<TemplateEntryInfo> infos = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo, thirdTemplateEntryInfo));
        return infos;
    }

    public static Template createTemplateForTest(){
        Template template = new Template();
        template.setId(UUID.randomUUID());
        template.setName("Vacation");
        return template;
    }
    public static TemplateGroup createTemplateGroupForTest(){
        TemplateGroup group = new TemplateGroup();
        group.setId(UUID.randomUUID());
        group.setName("Emotion");
        return group;
    }


    public static IGlobalDataAccess createMockGlobalDataAccess(){
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }
    public static ITemplateDataAccess createMockTemplateDataAccess(){
        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        return  templateDataAccess;
    }

    public static ITemplateDataAccess createMockTemplateDataAccessReturnTemplate(Template entity){
        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        when(templateDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(templateDataAccess.getType()).thenReturn(Template.class);
        return  templateDataAccess;
    }

    public static ITemplateGroupDataAccess createMockTemplateGroupDataAccess(){
        ITemplateGroupDataAccess templateGroupDataAccess = Mockito.mock(ITemplateGroupDataAccess.class);
        return templateGroupDataAccess;
    }
    public static ITemplateGroupDataAccess createMockTemplateGroupDataAccessReturnParent(TemplateGroup entity){
        ITemplateGroupDataAccess templateGroupDataAccess = Mockito.mock(ITemplateGroupDataAccess.class);
        when(templateGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(templateGroupDataAccess.getType()).thenReturn(TemplateGroup.class);
        return templateGroupDataAccess;
    }

    public static IAccountDataAccess createAccountDataAccess(){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }
    public static IAccountDataAccess createAccountDataAccessReturnAccount(Account entity){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.get(Mockito.mock(UUID.class))).thenReturn(entity);
        when(accountDataAccess.getType()).thenReturn(Account.class);
        return accountDataAccess;
    }

    public static TemplateService createTemplateServiceForTest(){
        TemplateService service = new TemplateService(
                createMockGlobalDataAccess(),
                createMockTemplateDataAccess(),
                createMockTemplateGroupDataAccess(),
                createAccountDataAccess());
        return service;
    }
}

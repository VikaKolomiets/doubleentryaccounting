package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class MoveTemplateTests {
    private Template entity;
    private TemplateGroup toParent;
    private TemplateGroup fromParent;
    private final int MAX_ORDER = 7;

    @BeforeEach
    public void setUpTest() {
        entity = TemplateTestsCommon.createTemplateForTest();
        fromParent = TemplateTestsCommon.createTemplateGroupForTest();
        toParent = TemplateTestsCommon.createTemplateGroupForTest();
        entity.setParent(fromParent);
        fromParent.getChildren().add(0, entity);
    }

    @AfterEach
    public void tearDown() {
        entity = null;
        fromParent = null;
        toParent = null;
    }

    @Test
    public void testMoveTemplateCheckUuidForNullEntityException() {
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(null, toParent.getId()),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testMoveTemplateCheckUuidForNullParentException() {
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.moveToAnotherParent(entity.getId(), null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testMoveTemplateCheckAndGetEntityByIdException() {
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(null),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testMoveTemplateCheckAndGetParentByIdException() {
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity),
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(null),
                TemplateTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testMoveTemplateCheckEntityWithSameNameException() {
        Template sameNameEntity = TemplateTestsCommon.createTemplateForTest();
        sameNameEntity.setName(entity.getName());
        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity);
        when(templateDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(sameNameEntity)));

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(toParent),
                TemplateTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.moveToAnotherParent(entity.getId(), toParent.getId()),
                "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testMoveTemplatePositive() {
        Template first = TemplateTestsCommon.createTemplateForTest();
        first.setOrder(1);
        first.setParent(fromParent);
        Template third = TemplateTestsCommon.createTemplateForTest();
        third.setParent(fromParent);
        third.setOrder(3);
        fromParent.getChildren().add(1, first);
        fromParent.getChildren().add(2, third);
        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity);
        when(templateDataAccess.getMaxOrder(Mockito.any(UUID.class))).thenReturn(MAX_ORDER);

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(toParent),
                TemplateTestsCommon.createAccountDataAccess());
        service.moveToAnotherParent(entity.getId(), toParent.getId());
        Assertions.assertEquals(toParent.getId(), entity.getParent().getId());
        Assertions.assertEquals(MAX_ORDER + 1, entity.getOrder());
        Assertions.assertEquals(2, fromParent.getChildren().size());
        Assertions.assertEquals(2, fromParent.getChildren().get(1).getOrder());
    }
}

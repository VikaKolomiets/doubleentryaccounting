package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateTemplateTests {
    private TemplateGroup parent;
    private Template entity;
    private TemplateInfo info;

    @BeforeEach
    public void setUpTest(){
        entity = TemplateTestsCommon.createTemplateForTest();
        parent = TemplateTestsCommon.createTemplateGroupForTest();
        entity.setParent(parent);
        parent.getChildren().add(entity);
    }
    @AfterEach
    public void tearDownTest(){
        entity = null;
        info = null;
        parent = null;
    }

    @Test
    public void testUpdateTemplateCheckParameterForNullException(){
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), null),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testUpdateTemplateCheckUuidForNullException(){
        info = new TemplateInfo(parent.getId(), "Name", new ArrayList<>());
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(null, info),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testUpdateTemplateCheckStringForNullException(){
        info = new TemplateInfo(parent.getId(), null, new ArrayList<>());
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(entity.getId(), info),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testUpdateTemplateCheckTemplateEntitiesForNullException(){
        info = new TemplateInfo(parent.getId(), "Name", new ArrayList<>());
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTemplateCheckTemplateEntitiesLessTwoException(){
        info = new TemplateInfo(
                parent.getId(),
                "Name",
                new ArrayList<>(List.of(new TemplateEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(5.44) ))));
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testUpdateTemplateCheckUuidForNullAccountException(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(4.6));
        TemplateEntryInfo thirdTemplateEntryInfo = new TemplateEntryInfo(thirdAccount.getId(), BigDecimal.valueOf(-10.0));

        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo, thirdTemplateEntryInfo));
        info = new TemplateInfo(parent.getId(),"Name", infors);

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccess(),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccessReturnAccount(null));
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(entity.getId(), info),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testUpdateTemplateCheckAndGetEntityByIdException(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(4.6));
        TemplateEntryInfo thirdTemplateEntryInfo = new TemplateEntryInfo(thirdAccount.getId(), BigDecimal.valueOf(-10.0));

        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo, thirdTemplateEntryInfo));
        info = new TemplateInfo(parent.getId(),"Name", infors);

        IAccountDataAccess accountDataAccess = TemplateTestsCommon.createAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount, thirdAccount);

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(null),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                accountDataAccess);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(entity.getId(), info),
                "NoSuchElementException is not thrown.");
    }

    @Test
     public void testUpdateTemplateCheckEntityWithSameNameException(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(4.6));
        TemplateEntryInfo thirdTemplateEntryInfo = new TemplateEntryInfo(thirdAccount.getId(), BigDecimal.valueOf(-10.0));

        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo, thirdTemplateEntryInfo));
        info = new TemplateInfo(parent.getId(),"Template", infors);

        Template sameName = new Template();
        sameName.setName(info.getName());

        IAccountDataAccess accountDataAccess = TemplateTestsCommon.createAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount);

        ITemplateDataAccess templateDataAccess = Mockito.mock(ITemplateDataAccess.class);
        when(templateDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(templateDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(sameName)));

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                accountDataAccess);
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(entity.getId(), info),
                "IllegalArgumentException is not thrown.");
    }

    @Test
    public void testUpdateTemplatePositive(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        Account thirdAccount = new Account();
        thirdAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(4.6));
        TemplateEntryInfo thirdTemplateEntryInfo = new TemplateEntryInfo(thirdAccount.getId(), BigDecimal.valueOf(-10.0));

        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo, thirdTemplateEntryInfo));
        info = new TemplateInfo(parent.getId()
                ,"Template"
                , "Description for Test"
                ,true
                , infors);

        IAccountDataAccess accountDataAccess = TemplateTestsCommon.createAccountDataAccess();
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount, thirdAccount);

        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity);
        ArgumentCaptor<Template> argumentCaptor = ArgumentCaptor.forClass(Template.class);
        doNothing().when(templateDataAccess).update(argumentCaptor.capture());

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                accountDataAccess);
        service.update(entity.getId(), info);
        Template capturedTemplate = argumentCaptor.getValue();

        Assertions.assertEquals(info.getName(), capturedTemplate.getName(), "Name is not passed into updated entity");
        Assertions.assertEquals(info.getDescription(), capturedTemplate.getDescription(), "Description is not passed into updated entity");
        Assertions.assertEquals(info.getParentId(), capturedTemplate.getParent().getId(), "Parent is not passed into updated entity");
        Assertions.assertEquals(info.getEntries().size(), capturedTemplate.getEntries().size(), "Entries are not passed into updated entity");
        Assertions.assertTrue(capturedTemplate.getIsFavorite());
    }





}

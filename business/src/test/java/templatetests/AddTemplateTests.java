package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class AddTemplateTests {
    private TemplateGroup parent;
    private TemplateInfo info;
    private final int MAX_ORDER = 7;
    @BeforeEach
    public void setUpTest(){
        parent = TemplateTestsCommon.createTemplateGroupForTest();
    }
    @AfterEach
    public void tearDownTest(){
        parent = null;
        info = null;
    }

    @Test
    public void testAddTemplateCheckParameterForNullException(){
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(null),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckStringForNullException(){
        info = new TemplateInfo(
                parent.getId(),
                null,
                TemplateTestsCommon.createTemplateEntryInfoList());
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckUuidForNullException(){
        info = new TemplateInfo(
                null,
                "TemplateName",
                TemplateTestsCommon.createTemplateEntryInfoList());
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckTransactionEntriesCountNullException(){
        info = new TemplateInfo(
                parent.getId(),
                "TemplateName",
                null);
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckTransactionEntriesCountLessTwoException(){
        info = new TemplateInfo(
                parent.getId(),
                "TemplateName",
                new ArrayList<>(List.of(new TemplateEntryInfo(UUID.randomUUID(), BigDecimal.valueOf(15.5)))));
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckAccountUuidForNullException(){
        info = new TemplateInfo(
                parent.getId(),
                "TemplateName",
                new ArrayList<>(List.of(
                        new TemplateEntryInfo(null, BigDecimal.valueOf(15.5)),
                        new TemplateEntryInfo(null, BigDecimal.valueOf(-15.5)))));
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(info),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckAndGetEntityByIdAccountException(){
        info = new TemplateInfo(
                parent.getId(),
                "TemplateName",
                TemplateTestsCommon.createTemplateEntryInfoList());
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccess(),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccessReturnAccount(null));
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckAndGetEntityByIdParentException(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(-5.4));


        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo));
        info = new TemplateInfo(parent.getId(),"TemplateName", infors);

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount);

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccess(),
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(null),
                accountDataAccess);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.add(info),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testAddTemplateCheckEntityWithSameNameException(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());

        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(-5.4));

        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo));
        info = new TemplateInfo(parent.getId(),"TemplateName", infors);

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount);

        Template sameNameTemplate = TemplateTestsCommon.createTemplateForTest();
        sameNameTemplate.setName(info.getName());
        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccess();
        when(templateDataAccess
                .getByName(Mockito.any(UUID.class), Mockito.any(String.class)))
                .thenReturn(new ArrayList<>(List.of(sameNameTemplate)));

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(parent),
                accountDataAccess);
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(info),
                "IllegalArgumentException is not thrown.");
    }
    @Test
    public void testAddTemplatePositive(){
        Account firstAccount = new Account();
        firstAccount.setId(UUID.randomUUID());
        Account secondAccount = new Account();
        secondAccount.setId(UUID.randomUUID());
        TemplateEntryInfo firstTemplateEntryInfo = new TemplateEntryInfo(firstAccount.getId(), BigDecimal.valueOf(5.4));
        TemplateEntryInfo secondTemplateEntryInfo = new TemplateEntryInfo(secondAccount.getId(), BigDecimal.valueOf(-5.4));
        ArrayList<TemplateEntryInfo> infors = new ArrayList<>(List.of(firstTemplateEntryInfo, secondTemplateEntryInfo));

        info = new TemplateInfo(parent.getId()
                ,"TemplateName"
                , "Template descriiption"
                ,true
                , infors);

        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.get(Mockito.any(UUID.class))).thenReturn(firstAccount, secondAccount);

        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccess();
        when(templateDataAccess
                .getMaxOrder(Mockito.any(UUID.class)))
                .thenReturn(MAX_ORDER);
        ArgumentCaptor<Template> argumentCaptor = ArgumentCaptor.forClass(Template.class);
        doNothing().when(templateDataAccess).add(argumentCaptor.capture());

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccessReturnParent(parent),
                accountDataAccess);

        service.add(info);
        Template capturedEntity = argumentCaptor.getValue();

        Assertions.assertEquals(info.getName(), capturedEntity.getName());
        Assertions.assertEquals(info.getDescription(), capturedEntity.getDescription());
        Assertions.assertEquals(info.getParentId(), capturedEntity.getParent().getId());
        Assertions.assertEquals(info.getEntries().size(), capturedEntity.getEntries().size());
        Assertions.assertTrue(capturedEntity.getIsFavorite());
    }
}

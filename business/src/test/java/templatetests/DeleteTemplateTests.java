package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteTemplateTests {
    private Template entity;
    private TemplateGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = TemplateTestsCommon.createTemplateGroupForTest();
        entity = TemplateTestsCommon.createTemplateForTest();
        entity.setOrder(2);
        entity.setParent(parent);
        parent.getChildren().add(entity);
    }

    @AfterEach
    public void tearDownTest(){
        entity = null;
        parent = null;
    }

    @Test
    public void testDeleteTemplateCheckUuidForNullException(){
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testDeleteTemplateCheckAndGetEntityByIdException(){
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(null),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());

        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(entity.getId()),
                "NoSuchElementException is not thrown.");
    }

    @Test
    public void testDeleteTemplatePositive(){
        Template first = TemplateTestsCommon.createTemplateForTest();
        first.setOrder(1);
        first.setName("Holiday");
        first.setParent(parent);
        Template third = TemplateTestsCommon.createTemplateForTest();
        third.setOrder(3);
        third.setName("Weekend");
        third.setParent(parent);
        parent.getChildren().add(first);
        parent.getChildren().add(third);

        int sizeGetChildrenBefore = parent.getChildren().size();
        ITemplateDataAccess templateDataAccess = TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity);
        ArgumentCaptor<Template> argumentCaptor = ArgumentCaptor.forClass(Template.class);
        doNothing().when(templateDataAccess).delete(argumentCaptor.capture());

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                templateDataAccess,
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        service.delete(entity.getId());
        Template templateWithChangedOrder = null;
        Template capturedEntity = argumentCaptor.getValue();
        for (Template entry: parent.getChildren()) {
            if(entry.getId().equals(third.getId())){
                templateWithChangedOrder = entry;
                break;
            }
        }

        Assertions.assertEquals(
                sizeGetChildrenBefore - 1,
                parent.getChildren().size(),
                "Entity is not deleted from list parent.Children list");
        Assertions.assertEquals(
                2,
                templateWithChangedOrder.getOrder(),
        "ReOrdered is not done in the list of parent.Children()");
        Assertions.assertEquals(
                entity.getId(),
                capturedEntity.getId(),
                "Parameter is mot passed into delete method.");

    }


}

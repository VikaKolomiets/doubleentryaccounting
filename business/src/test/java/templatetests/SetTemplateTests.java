package templatetests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import java.util.*;

public class SetTemplateTests {
    private Template entity;
    private final int ORDER = 2;
    @BeforeEach
    public void setUpTest(){
        entity = TemplateTestsCommon.createTemplateForTest();
        entity.setIsFavorite(false);
        entity.setOrder(2);
    }
    @AfterEach
    public void tearDownTest(){
        entity = null;
    }
    @Test
    public void testSetFavoriteTemplateCheckUuidForNullException(){
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setFavoriteStatus(null, true),
        "NullPointerException is not thrown");
    }
    @Test
    public void testSetFavoriteTemplateCheckAndGetEntityByIdException(){
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(null),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setFavoriteStatus(entity.getId(), true),
                "NoSuchElementException is not thrown");
    }
    @Test
    public void testSetFavoriteTemplatePositive(){

        Boolean isFavoriteBefore = entity.getIsFavorite();
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        service.setFavoriteStatus(entity.getId(), true);

        Assertions.assertTrue(entity.getIsFavorite(), "Isfavorute status doesn't change to true");
        Assertions.assertNotEquals(isFavoriteBefore, entity.getIsFavorite(), "There isn't change in IsFavorite status");
    }

    @Test
    public void testSetOrderTemplateCheckUuidForNullException(){
        TemplateService service = TemplateTestsCommon.createTemplateServiceForTest();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setOrder(null, ORDER),
                "NullPointerException is not thrown");
    }
    @Test
    public void testSetOrderTemplateCheckAndGetEntityByIdException(){
        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(null),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setOrder(entity.getId(), ORDER),
                "NoSuchElementException is not thrown");
    }
    @ParameterizedTest
    @ValueSource(ints = {1, 3})
    public void testSetOrderTemplatePositive(int order){
        TemplateGroup parent = TemplateTestsCommon.createTemplateGroupForTest();
        entity.setParent(parent);
        Template first = TemplateTestsCommon.createTemplateForTest();
        first.setOrder(1);
        first.setParent(parent);
        Template third = TemplateTestsCommon.createTemplateForTest();
        third.setOrder(3);
        third.setParent(parent);
        parent.getChildren().add(first);
        parent.getChildren().add(third);
        parent.getChildren().add(entity);

        TemplateService service = new TemplateService(
                TemplateTestsCommon.createMockGlobalDataAccess(),
                TemplateTestsCommon.createMockTemplateDataAccessReturnTemplate(entity),
                TemplateTestsCommon.createMockTemplateGroupDataAccess(),
                TemplateTestsCommon.createAccountDataAccess());
        service.setOrder(entity.getId(), order);

        Assertions.assertEquals(order, entity.getOrder(), "Order is not changed.");
        if (order == 1){
            Assertions.assertTrue(first.getOrder()==2);
        }
        if (order == 3){
            Assertions.assertTrue(third.getOrder() == 2);
        }
    }

}

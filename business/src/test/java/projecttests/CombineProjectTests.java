package projecttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CombineProjectTests {

    private Project primaryEntity;
    private Project secondaryEntity;
    private ProjectGroup parent;
    private final int ORDER = 4;

    @BeforeEach
    public void setUpTest(){
        parent = ProjectTestsCommon.createProjectGroupWithChildren();
        primaryEntity = ProjectTestsCommon.createProjectForTest();
        primaryEntity.setName("Primary");
        primaryEntity.setOrder(ORDER);
        primaryEntity.setParent(parent);
        secondaryEntity = ProjectTestsCommon.createProjectForTest();
        secondaryEntity.setName("Secondary");
        secondaryEntity.setParent(parent);
        parent.getChildren().add(primaryEntity);
        parent.getChildren().add(secondaryEntity);
    }

    @AfterEach
    public void tearDownTest(){
        parent = null;
        primaryEntity = null;
        secondaryEntity = null;
    }

    @Test
    public void testCombineProjectCheckUuidForNullPrimaryException(){
        ProjectService service = ProjectTestsCommon.createProjectServiceForTest(primaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(null, secondaryEntity.getId()),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testCombineProjectCheckUuidForNullSecondaryException(){
        ProjectService service = ProjectTestsCommon.createProjectServiceForTest(secondaryEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), null),
        "NullPointerException is not thrown.");
    }
    @Test
    public void testCombineProjectCheckAndGetPrimaryByIdException(){
        ProjectService service = new ProjectService(
                ProjectTestsCommon.createGlobalDataAccess(),
                ProjectTestsCommon.createProjectDataAccess(null),
                ProjectTestsCommon.createProjectGroupDataAccess(),
                ProjectTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
        "NoSuchElementException is not thrown.");
    }
    @Test
    public void testCombineProjectCheckAndGetSecondaryByIdException(){
        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, (Project) null);
        when(projectDataAccess.getType()).thenReturn(Project.class);
        ProjectService service = new ProjectService(
                ProjectTestsCommon.createGlobalDataAccess(),
                projectDataAccess,
                ProjectTestsCommon.createProjectGroupDataAccess(),
                ProjectTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId()),
                "NoSuchElementException is not thrown.");
    }
    @Test
    public void testCombineProjectPositive(){
        int sizeChildren = parent.getChildren().size();

        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(primaryEntity, secondaryEntity);
        ArgumentCaptor<Project> deleteEntityCaptor = ArgumentCaptor.forClass(Project.class);
        doNothing().when(projectDataAccess).delete(deleteEntityCaptor.capture());

        ArrayList<Account> accounts = ProjectTestsCommon.createAccountForTest(secondaryEntity);
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByProject(Mockito.any(Project.class))).thenReturn(accounts);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountCaptor.capture());

        ProjectService service = new ProjectService(
                ProjectTestsCommon.createGlobalDataAccess(),
                projectDataAccess,
                ProjectTestsCommon.createProjectGroupDataAccess(),
                accountDataAccess);
        service.combineTwoEntities(primaryEntity.getId(), secondaryEntity.getId());

        Project deletedEntityCaptored = deleteEntityCaptor.getValue();
        Account updatedAccountCaptored = accountCaptor.getValue();

        Assertions.assertEquals(sizeChildren - 1, parent.getChildren().size(), "Parent doesn't delete any child.");
        Assertions.assertEquals(secondaryEntity.getId(), deletedEntityCaptored.getId(), "Parameter for deleting is not equl to secondary entity.");
        Assertions.assertEquals(primaryEntity.getId(), updatedAccountCaptored.getProject().getId(), "Account doesn't change project field to primary entity.");
        Assertions.assertNotEquals(ORDER, parent.getChildren().get(2).getOrder(), "Reodered mothod doen't work ");
    }
}

package projecttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class ProjectTestsCommon {
    public static Project createProjectForTest(){
        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName("Test");
        project.setOrder(2);
        return project;
    }

    public static ProjectGroup createProjectGroupWithChildren(){
        ProjectGroup group = new ProjectGroup();
        group.setId(UUID.randomUUID());
        Project first = new Project();
        first.setId(UUID.randomUUID());
        first.setName("Holiday");
        first.setOrder(1);
        first.setParent(group);
        Project third = new Project();
        third.setId(UUID.randomUUID());
        third.setName("Vacation");
        third.setOrder(3);
        third.setParent(group);
        group.getChildren().add(first);
        group.getChildren().add(third);
        return group;
    }

    public static IProjectGroupDataAccess createProjectGroupDataAccess(){
        IProjectGroupDataAccess projectGroupDataAccess = Mockito.mock(IProjectGroupDataAccess.class);
        return projectGroupDataAccess;
    }

    public static IGlobalDataAccess createGlobalDataAccess(){
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }

    public static IAccountDataAccess createAccountDataAccess(){
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        return accountDataAccess;
    }

    public static IProjectDataAccess createProjectDataAccess(Project entity){
        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(entity);
        when(projectDataAccess.getType()).thenReturn(Project.class);
        return projectDataAccess;
    }

    public static ProjectService createProjectServiceForTest(Project entity){
        ProjectService projectService = new ProjectService(
                createGlobalDataAccess(),
                createProjectDataAccess(entity),
                createProjectGroupDataAccess(),
                createAccountDataAccess());
        return projectService;
    }

    public static ArrayList<Account> createAccountForTest(Project entity){
        ArrayList<Account> accounts = new ArrayList<>();
        Account accountFirst = new Account();
        accountFirst.setName("Sister");
        accountFirst.setProject(entity);
        accounts.add(accountFirst);
        Account accountSecond = new Account();
        accountSecond.setName("Brother");
        accountSecond.setProject(entity);
        accounts.add(accountSecond);
        return accounts;
    }
}

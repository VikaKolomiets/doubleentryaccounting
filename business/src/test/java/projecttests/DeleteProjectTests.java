package projecttests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteProjectTests {
    private Project deleteEntity;
    private ProjectGroup parent;

    @BeforeEach
    public void setUpTest(){
        parent = ProjectTestsCommon.createProjectGroupWithChildren();
        deleteEntity = ProjectTestsCommon.createProjectForTest();
        deleteEntity.setName("Delete entity");
        deleteEntity.setParent(parent);
        parent.getChildren().add(deleteEntity);
    }
    @AfterEach
    public void tearDownTest(){
        parent = null;
        deleteEntity = null;
    }

    @Test
    public void testDeleteProjectCheckUuidForNullException(){
        ProjectService service = ProjectTestsCommon.createProjectServiceForTest(deleteEntity);
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
                "NullPointerException is not thrown.");
    }

    @Test
    public void testDeleteProjectCheckAndGetEntityByIdException(){
        ProjectService service = new ProjectService(
                ProjectTestsCommon.createGlobalDataAccess(),
                ProjectTestsCommon.createProjectDataAccess(null),
                ProjectTestsCommon.createProjectGroupDataAccess(),
                ProjectTestsCommon.createAccountDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(deleteEntity.getId()),
        "NoSuchElementException is not thrown.");
    }

    @Test
    public void testDeleteProjectPositive(){
        int sizeChildrenBefore = parent.getChildren().size();

        IProjectDataAccess projectDataAccess = Mockito.mock(IProjectDataAccess.class);
        when(projectDataAccess.get(Mockito.any(UUID.class))).thenReturn(deleteEntity);
        ArgumentCaptor<Project> entityCapture = ArgumentCaptor.forClass(Project.class);
        doNothing().when(projectDataAccess).delete(entityCapture.capture());

        ArrayList<Account> accounts = ProjectTestsCommon.createAccountForTest(deleteEntity);
        IAccountDataAccess accountDataAccess = Mockito.mock(IAccountDataAccess.class);
        when(accountDataAccess.getAccountsByProject(Mockito.any(Project.class))).thenReturn(accounts);
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        doNothing().when(accountDataAccess).update(accountCaptor.capture());

        ProjectService service = new ProjectService(
                ProjectTestsCommon.createGlobalDataAccess(),
                projectDataAccess,
                ProjectTestsCommon.createProjectGroupDataAccess(),
                accountDataAccess);
        service.delete(deleteEntity.getId());
        Project entityCaptured = entityCapture.getValue();
        Account accountCaptured = accountCaptor.getValue();
        Assertions.assertEquals(sizeChildrenBefore - 1,
                parent.getChildren().size(),
                "Size of Children is not changed");
        Assertions.assertEquals(deleteEntity.getId(),
                entityCaptured.getId(),
                "Parameter in delete method is not of deleteEntity");
        Assertions.assertNull(accountCaptured.getProject(),"Project field is not changed for null");
        Assertions.assertEquals(2, parent.getChildren().get(1).getOrder(),
                "Reodered after Delete is not work");

    }
}

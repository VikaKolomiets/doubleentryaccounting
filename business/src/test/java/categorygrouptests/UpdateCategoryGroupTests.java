package categorygrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class UpdateCategoryGroupTests {
    private CategoryGroup updateEntity;
    private GroupEntityInfo groupInfo;

    @BeforeEach
    public void setUpTest(){
        updateEntity = CategoryGroupTestsCommon.createCategoryGroupEmpty();
        groupInfo = new GroupEntityInfo("Holiday", "As Needed", false);
    }

    @AfterEach
    public void tearDown(){
        updateEntity = null;
        groupInfo = null;
    }

    @Test
    public void testUpdateCategoryGroupCheckParameterForNullException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update( updateEntity.getId(), null),
                "NullPointerException is not thrown");
    }

    @Test
    public void testUpdateCategoryGroupCheckUuidForNullException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update( null, groupInfo),
                "NullPointerException is not thrown");
    }

    @Test
    public void testUpdateCategoryGroupCheckStringForNullException(){
        groupInfo = new GroupEntityInfo(null);

        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.update(updateEntity.getId(), groupInfo),
                "NullPointerException is not thrown");
    }

    @Test
    public void testUpdateCategoryGroupCheckEntityWithSameNameException(){
        CategoryGroup sameNameEntity = CategoryGroupTestsCommon.createCategoryGroupEmpty();
        sameNameEntity.setName(groupInfo.getName());

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(updateEntity);
        when(categoryGroupDataAccess.getByName(Mockito.any(String.class))).thenReturn(new ArrayList<CategoryGroup>(List.of(sameNameEntity)));
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.update(updateEntity.getId(), groupInfo),
                "IllegalArgumentException is not thrown");
    }

    @Test
    public void testUpdateCategoryGroupCheckAndGetEntityByIdException(){
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                CategoryGroupTestsCommon.createCategoryGroupDataAccess(null),
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.update(updateEntity.getId(),groupInfo),
                "NoSuchElementException is not thrown");
    }

    @Test
    public void testUpdateCategoryGroupPositive() {
        ICategoryGroupDataAccess groupCategoryDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(groupCategoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(updateEntity);
        ArgumentCaptor<CategoryGroup> entityCaptor = ArgumentCaptor.forClass(CategoryGroup.class);
        doNothing().when(groupCategoryDataAccess).update(entityCaptor.capture());

        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                groupCategoryDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());

        service.update(updateEntity.getId(), groupInfo);

        CategoryGroup entityCaptured = entityCaptor.getValue();

        Assertions.assertEquals(groupInfo.getName(), entityCaptured.getName(), "Name is not changed");
        Assertions.assertEquals(groupInfo.getDescription(), entityCaptured.getDescription(), "Descpription is not changed");
        Assertions.assertEquals(groupInfo.getIsFavorite(), entityCaptured.getIsFavorite(), "IsFavorite is not changed");
    }
}

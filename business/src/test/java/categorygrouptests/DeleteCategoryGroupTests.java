package categorygrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class DeleteCategoryGroupTests {
    private CategoryGroup deletedEntity;
    private final int SIZE_CHILDREN_QUERY = 3;

    @BeforeEach
    public void setUpTest(){
        deletedEntity = CategoryGroupTestsCommon.createCategoryGroupEmpty();
    }
    @AfterEach
    public void tearDown(){
        deletedEntity = null;
    }

    @Test
    public void testDeleteCategoryGroupNullEntityException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(null),
        "NullPointerException is not thrown");
    }

    @Test
    public void testDeleteCategoryGroupCheckAndGetEntityByIdException(){
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                CategoryGroupTestsCommon.createCategoryGroupDataAccess(null),
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.delete(deletedEntity.getId()),
                "NoSuchElementException is not thrown");
    }

    @Test
    public void testDeleteCategoryGroupCheckExistedChildrenException(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        when(categoryDataAccess.getCount(Mockito.any(UUID.class))).thenReturn(SIZE_CHILDREN_QUERY);
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                CategoryGroupTestsCommon.createCategoryGroupDataAccess(deletedEntity),
                categoryDataAccess);
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.delete(deletedEntity.getId()),
                "IllegalArgumentException is not thrown");
    }
    @Test
    public void testDeleteCategoryGroupPositive(){
        ArrayList<CategoryGroup> categoryGroups = (ArrayList<CategoryGroup>) CategoryGroupTestsCommon.createListCategoryGroupForTest();
        categoryGroups.get(0).setOrder(1);
        categoryGroups.get(1).setOrder(5);
        String groupName = categoryGroups.get(1).getName();

        ICategoryGroupDataAccess categoryGroupDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(categoryGroupDataAccess.get(Mockito.any(UUID.class))).thenReturn(deletedEntity);
        when(categoryGroupDataAccess.getList()).thenReturn(categoryGroups);

        ArgumentCaptor<CategoryGroup> entityCapture = ArgumentCaptor.forClass(CategoryGroup.class);
        doNothing().when(categoryGroupDataAccess).delete(entityCapture.capture());

        ArgumentCaptor<List<CategoryGroup>> listCapture = ArgumentCaptor.forClass(List.class);
        doNothing().when(categoryGroupDataAccess).updateList((ArrayList<CategoryGroup>) listCapture.capture());

        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());
        service.delete(deletedEntity.getId());

        ArrayList<CategoryGroup> listCaptured = (ArrayList<CategoryGroup>) listCapture.getValue();
        CategoryGroup entityOrdered = null;
        for(CategoryGroup group: listCaptured){
            if(group.getName().equals(groupName)){
                entityOrdered = group;
                break;
            }
        }
        CategoryGroup entityCaptured = entityCapture.getValue();

        Assertions.assertEquals(deletedEntity.getId(), entityCaptured.getId(), "DeletedEntity is not a parameter in method delete()");
        Assertions.assertEquals(2, entityOrdered.getOrder(), "ReOdered method after delete is not change the entity order");
    }



}

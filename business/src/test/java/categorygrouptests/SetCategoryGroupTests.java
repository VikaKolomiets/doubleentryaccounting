package categorygrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class SetCategoryGroupTests {

    private CategoryGroup categoryGroup;

    @BeforeEach
    public void setUpTest(){
        categoryGroup = CategoryGroupTestsCommon.createCategoryGroupEmpty();
    }

    @AfterEach
    public void tearDownTest(){
        categoryGroup = null;
    }

    @Test
    public void testSetFavoriteStatusCategoryGroupNullUUIDException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setFavoriteStatus(null, true),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testSetFavoriteStatusCategoryGroupNullEntityException(){
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                CategoryGroupTestsCommon
                        .createCategoryGroupDataAccess(null),
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setFavoriteStatus(categoryGroup.getId(), true),
                "NoSuchElementException is not thrown.");
    }
    @ParameterizedTest()
    @ValueSource(booleans = true)
    public void testSetFavoriteStatusCategoryGroupPositive(boolean isFavoriteNew){
        ICategoryGroupDataAccess categoryGroupDataAccess  = CategoryGroupTestsCommon
                .createCategoryGroupDataAccess(categoryGroup);

        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Boolean statusBefore = categoryGroup.getIsFavorite();
        service.setFavoriteStatus(categoryGroup.getId(), isFavoriteNew);

        Assertions.assertEquals(isFavoriteNew, categoryGroup.getIsFavorite(), "IsFavorite is not changed" );
        Assertions.assertNotEquals(statusBefore, categoryGroup.getIsFavorite(), "IsFavorite is not changed");
    }
    @Test
    public void testSetOrderCategoryGroupNullUUIDException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.setOrder(null, 5),
                "NullPointerException is not thrown.");
    }
    @Test
    public void testSetOrderCategoryGroupNullEntityException(){
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                CategoryGroupTestsCommon.createCategoryGroupDataAccess(null),
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> service.setOrder(categoryGroup.getId(), 5),
                "NoSuchElementException is not thrown.");
    }
    @ParameterizedTest()
    @ValueSource(ints = {3,1})
    public void testSetOrderCategoryGroupPositive(int order){
        List<CategoryGroup> categoryGroups = CategoryGroupTestsCommon.createListCategoryGroupForTest();
        categoryGroup.setOrder(2);
        int orderBefore = categoryGroup.getOrder();
        categoryGroups.add(categoryGroup);

        ICategoryGroupDataAccess categoryGroupDataAccess  = CategoryGroupTestsCommon
                .createCategoryGroupDataAccess(categoryGroup);
        when(categoryGroupDataAccess.getList()).thenReturn((ArrayList<CategoryGroup>) categoryGroups);
        ArgumentCaptor<ArrayList<CategoryGroup>> entityCaptor = ArgumentCaptor.forClass(ArrayList.class);
        doNothing().when(categoryGroupDataAccess).updateList(entityCaptor.capture());

        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());

        service.setOrder(categoryGroup.getId(), order);

        ArrayList<CategoryGroup> entities = new ArrayList<>();
        entities.addAll(entityCaptor.getValue());

        int actualOrder = 100;
        for (CategoryGroup entity : entities) {
            if (entity.getId().equals(categoryGroup.getId())){
                actualOrder = entity.getOrder();
                break;
            }
        }

        Assertions.assertEquals(order, actualOrder, "Order is not changed");
        Assertions.assertNotEquals(orderBefore, actualOrder, "Order is not changed");
    }
}

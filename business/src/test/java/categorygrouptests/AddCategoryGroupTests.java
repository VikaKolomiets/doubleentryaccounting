package categorygrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.infos.*;
import doubleentryaccounting.common.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class AddCategoryGroupTests {
    private GroupEntityInfo groupInfor;
    private final int MAX_ORDER = 7;


    @BeforeEach
    public void setUpTests(){
        groupInfor = new GroupEntityInfo("Vacation","Once per year", true);
    }
    @AfterEach
    public void tearDown(){
        groupInfor = null;
    }

    @Test
    public void testAddCategoryGroupCheckParameterForNullException(){
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(null),
        "NullPointerException is not thrown");
    }
    @Test
    public void testAddCategoryGroupCheckStringForNullExseption(){
        groupInfor = new GroupEntityInfo(null,"Once per year", true);
        CategoryGroupService service = CategoryGroupTestsCommon.createMockCategoryGroupServiceForTests();
        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.add(groupInfor),
                "NullPointerException is not thrown");
    }
    @Test
    public void testAddCategoryGroupCheckEntityWithSameNameExseption(){
        groupInfor = new GroupEntityInfo("Vacation","Once per year", true);
        CategoryGroup sameNameGroup = CategoryGroupTestsCommon.createCategoryGroupEmpty();
        sameNameGroup.setName(groupInfor.getName());

        ICategoryGroupDataAccess categoryGroupDataAccess =  CategoryGroupTestsCommon.createCategoryGroupDataAccess();
        when(categoryGroupDataAccess.getByName(Mockito.any(String.class))).thenReturn(new ArrayList<>(List.of(sameNameGroup)));
        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> service.add(groupInfor),
                "IllegalArgumentException is not thrown");
    }

    @Test
    public void testAddCategoryGroupPositive(){
        groupInfor = new GroupEntityInfo("Vacation","Once per year", true);
        ICategoryGroupDataAccess categoryGroupDataAccess =  CategoryGroupTestsCommon.createCategoryGroupDataAccess();
        when(categoryGroupDataAccess.getMaxOrder()).thenReturn(MAX_ORDER);
        ArgumentCaptor<CategoryGroup> entityCaptire = ArgumentCaptor.forClass(CategoryGroup.class);
        doNothing().when(categoryGroupDataAccess).add(entityCaptire.capture());

        CategoryGroupService service = new CategoryGroupService(
                CategoryGroupTestsCommon.createGlobalDataAccess(),
                categoryGroupDataAccess,
                CategoryGroupTestsCommon.createCategoryDataAccess());
        UUID idAddedGroup = service.add(groupInfor);
        CategoryGroup categoryGroupCaptured = entityCaptire.getValue();
        Assertions.assertEquals(groupInfor.getName(), categoryGroupCaptured.getName(), "Parameter GroupInfo is not passed to method add()");
        Assertions.assertEquals(MAX_ORDER + 1, categoryGroupCaptured.getOrder(), "MAX_ORDER is not set to AddedCategoryGroup");
    }

}

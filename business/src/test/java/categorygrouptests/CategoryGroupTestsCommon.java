package categorygrouptests;

import doubleentryaccounting.business.services.*;
import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CategoryGroupTestsCommon {

    public static CategoryGroup createCategoryGroupEmpty() {
        CategoryGroup parent = new CategoryGroup();
        parent.setId(UUID.randomUUID());
        parent.setName("Parent");
        parent.setDescription("Cashes");
        parent.setIsFavorite(false);
        return parent;
    }

    public static CategoryGroupService createMockCategoryGroupServiceForTests(){
        CategoryGroupService service = new CategoryGroupService(
                createGlobalDataAccess(),
                createCategoryGroupDataAccess(),
                createCategoryDataAccess());
        return service;
    }

    public static IGlobalDataAccess createGlobalDataAccess(){
        IGlobalDataAccess globalDataAccess = Mockito.mock(IGlobalDataAccess.class);
        return globalDataAccess;
    }
    public static ICategoryGroupDataAccess createCategoryGroupDataAccess(CategoryGroup groupEntity){
        ICategoryGroupDataAccess groupCategoryDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        when(groupCategoryDataAccess.get(Mockito.any(UUID.class))).thenReturn(groupEntity);
        when(groupCategoryDataAccess.getType()).thenReturn(CategoryGroup.class);
        return groupCategoryDataAccess;
    }
    public static ICategoryGroupDataAccess createCategoryGroupDataAccess(){
        ICategoryGroupDataAccess groupCategoryDataAccess = Mockito.mock(ICategoryGroupDataAccess.class);
        return groupCategoryDataAccess;
    }
    public static ICategoryDataAccess createCategoryDataAccess(){
        ICategoryDataAccess categoryDataAccess = Mockito.mock(ICategoryDataAccess.class);
        return categoryDataAccess;
    }

    public static List<CategoryGroup> createListCategoryGroupForTest(){
        List<CategoryGroup> groups = new ArrayList<>();
        CategoryGroup first = createCategoryGroupEmpty();
        first.setOrder(1);
        first.setName("Mom");;
        CategoryGroup third = createCategoryGroupEmpty();
        third.setOrder(3);
        third.setName("Dad");
        groups.add(first);
        groups.add(third);

        return groups;
    }
}

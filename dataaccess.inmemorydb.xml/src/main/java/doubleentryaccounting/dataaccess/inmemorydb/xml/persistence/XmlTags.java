package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

public class XmlTags {

    public final static String Ledger = "Ledger";

    public final static String ProjectSection = "ProjectSection";
    public final static String ProjectGroup = "ProjectGroup";
    public final static String Project = "Project";
    public final static ReferenceUnit ProjectUnit = new ReferenceUnit(ProjectSection, ProjectGroup, Project);

    public final static String CorrespondentSection = "CorrespondentSection";
    public final static String CorrespondentGroup = "CorrespondentGroup";
    public final static String Correspondent = "Correspondent";
    public final static ReferenceUnit CorrespondentUnit = new ReferenceUnit(CorrespondentSection, CorrespondentGroup, Correspondent);

    public final static String CategorySection = "CategorySection";
    public final static String CategoryGroup = "CategoryGroup";
    public final static String Category = "Category";
    public final static ReferenceUnit CategoryUnit = new ReferenceUnit(CategorySection, CategoryGroup, Category);

    public final static String CurrencySection = "CurrencySection";
    public final static String Currency = "Currency";
    public final static String CurrencyRate = "CurrencyRate";

    public final static String AccountSection = "AccountSection";
    public final static String AccountGroup = "AccountGroup";
    public final static String AccountSubGroup = "AccountSubGroup";
    public final static String Account = "Account";

    public final static String TemplateSection = "TemplateSection";
    public final static String TemplateGroup = "TemplateGroup";
    public final static String Template = "Template";
    public final static String TemplateEntry = "TemplateEntry";

    public final static String TransactionSection = "TransactionSection";
    public final static String Transaction = "Transaction";
    public final static String TransactionEntry = "TransactionEntry";

    public final static String AddedIds = "AddedIds";
    public final static String UpdatedIds = "UpdatedIds";
    public final static String DeletedIds = "DeletedIds";
    public final static String Entity = "Entity";
    public final static String EntityId = "Id";

    public final static String SystemConfig = "SystemConfig";
    public final static String MainCurrency = "MainCurrency";
    public final static String MinDateTime = "MinDateTime";
    public final static String MaxDateTime = "MaxDateTime";

    public final static String UserConfig = "UserConfig";

    public final static String Id = "a_id";
    public final static String TimeStamp = "b_timestamp";
    public final static String Name = "c_name";
    public final static String Order = "f_order";
    public final static String IsFavorite = "g_isfavorite";
    public final static String Description = "m_description";

    public final static String IsoCode = "d_isocode";
    public final static String Symbol = "e_symbol";
    public final static String Date = "h_date";
    public final static String DateTime = "i_datetime";
    public final static String Amount = "j_amount";
    public final static String Rate = "k_rate";
    public final static String State = "l_state";
    public final static String Comment = "m_comment";

    public final static String CurrencyId = "h_currencyid";
    public final static String AccountId = "h_accountid";
    public final static String ProjectId = "i_projectid";
    public final static String CorrespondentId = "i_correspondentid";
    public final static String CategoryId = "i_categoryid";

    public static class ReferenceUnit {

        public ReferenceUnit(String section, String group, String entity) {
            Section = section;
            Group = group;
            Entity = entity;
        }

        public final String Section;
        public final String Group;
        public final String Entity;
    }
}

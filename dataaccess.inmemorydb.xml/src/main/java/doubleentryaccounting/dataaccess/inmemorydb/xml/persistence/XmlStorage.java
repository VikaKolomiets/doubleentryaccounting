package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;

public class XmlStorage implements IXmlStorage {

    public String filePath;

    public XmlStorage(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public Document load() {
        Document doc = null;
        try {
            File file = new File(this.filePath);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new IllegalArgumentException(e);
        }
        return doc;
    }


    public void save(Document doc) {
        try {
            File file = new File(this.filePath);
            file.createNewFile();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (FileOutputStream output = new FileOutputStream(filePath, false)) {

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);

            StreamResult streamResult = new StreamResult(output);
            transformer.transform(source, streamResult);

        } catch (IOException | TransformerException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

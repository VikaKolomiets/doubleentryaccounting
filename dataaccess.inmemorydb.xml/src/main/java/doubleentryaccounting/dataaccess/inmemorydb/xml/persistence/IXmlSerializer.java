package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

import doubleentryaccounting.dataaccess.inmemorydb.*;
import org.w3c.dom.*;

public interface IXmlSerializer {
    Document serialize(Ledger ledger);
    Ledger deserialize(Document doc);

}

package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

import org.w3c.dom.*;

public interface IXmlValidator {
    void Validate(Document doc);
}

package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

import org.w3c.dom.*;

public interface IXmlStorage {
    Document load();
    void save(Document doc);

}

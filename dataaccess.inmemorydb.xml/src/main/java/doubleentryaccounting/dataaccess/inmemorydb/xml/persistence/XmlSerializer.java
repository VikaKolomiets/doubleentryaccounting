package doubleentryaccounting.dataaccess.inmemorydb.xml.persistence;

import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.common.models.config.*;
import doubleentryaccounting.common.models.enums.*;
import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.utils.helpers.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import java.math.*;
import java.time.*;
import java.util.*;

public class XmlSerializer implements IXmlSerializer {

    //region serialize

    public Document serialize(Ledger ledger) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        Document doc = builder.newDocument();

        Element root = doc.createElement(XmlTags.Ledger);
        doc.appendChild(root);

        createReferenceSection(ledger.getProjectGroups(), doc, root, XmlTags.ProjectSection, XmlTags.ProjectGroup, XmlTags.Project);
        createReferenceSection(ledger.getCorrespondentGroups(), doc, root, XmlTags.CorrespondentSection, XmlTags.CorrespondentGroup, XmlTags.Correspondent);
        createReferenceSection(ledger.getCategoryGroups(), doc, root, XmlTags.CategorySection, XmlTags.CategoryGroup, XmlTags.Category);

        createCurrencySection(ledger.getCurrencies(), doc, root);

        createAccountSection(ledger.getAccountGroups(), doc, root);

        createTemplateGroups(ledger.getTemplateGroups(), doc, root);

        createTransactions(ledger.getTransactions(), doc, root);

        createIdHistoryCollection(ledger.getAddedIds(), doc, root, XmlTags.AddedIds);
        createIdHistoryCollection(ledger.getAddedIds(), doc, root, XmlTags.UpdatedIds);
        createIdHistoryCollection(ledger.getAddedIds(), doc, root, XmlTags.DeletedIds);

        createSystemConfig(ledger, doc, root);
        createUserConfig(ledger, doc, root);

        return doc;
    }

    //endregion

    //region serialize collections

    private static <TParent extends IReferenceParentEntity<TChild>, TChild extends IReferenceChildEntity<TParent>>
        void createReferenceSection(
            ArrayList<TParent> groups, Document doc, Element root,
            String sectionName, String groupName, String elementName) {

        Element sectionElement = doc.createElement(sectionName);
        root.appendChild(sectionElement);

        for (TParent group : groups) {
            Element groupElement = createReferenceElement(group, doc, groupName);
            sectionElement.appendChild(groupElement);
            ArrayList<TChild> entities = group.getChildren();
            for (TChild entity : entities) {
                Element entityElement = createReferenceElement(entity, doc, elementName);
                groupElement.appendChild(entityElement);
            }
        }
    }

    private static void createCurrencySection(ArrayList<Currency> currencies, Document doc, Element root) {

        Element currencySectionElement = doc.createElement(XmlTags.CurrencySection);
        root.appendChild(currencySectionElement);

        for (Currency currency : currencies) {
            Element currencyElement = createCurrency(currency, doc);
            currencySectionElement.appendChild(currencyElement);

            ArrayList<CurrencyRate> currencyRates = currency.getRates();
            for (CurrencyRate currencyRate : currencyRates) {
                Element currencyRateElement = createRateCurrency(currencyRate, doc);
                currencyElement.appendChild(currencyRateElement);
            }
        }
    }

    private static void createAccountSection(ArrayList<AccountGroup> groups, Document doc, Element root) {

        Element accountSectionElement = doc.createElement(XmlTags.AccountSection);
        root.appendChild(accountSectionElement);

        for (AccountGroup group : groups) {
            Element groupElement = createReferenceElement(group, doc, XmlTags.AccountGroup);
            accountSectionElement.appendChild(groupElement);

            ArrayList<AccountSubGroup> subGroups = group.getChildren();
            for (AccountSubGroup subGroup : subGroups) {
                Element subGroupElement = createReferenceElement(subGroup, doc, XmlTags.AccountSubGroup);
                groupElement.appendChild(subGroupElement);

                ArrayList<Account> accounts = subGroup.getChildren();
                for (Account account : accounts) {
                    Element element = createAccount(account, doc);
                    subGroupElement.appendChild(element);
                }
            }
        }
    }

    private static void createTemplateGroups(ArrayList<TemplateGroup> groups, Document doc, Element root) {

        Element templateSectionElement = doc.createElement(XmlTags.TemplateSection);
        root.appendChild(templateSectionElement);

        for (TemplateGroup group : groups) {
            Element groupElement = createReferenceElement(group, doc, XmlTags.TemplateGroup);
            templateSectionElement.appendChild(groupElement);

            ArrayList<Template> templates = group.getChildren();
            for (Template template : templates) {
                Element templateElement = createReferenceElement(template, doc, XmlTags.Template);
                groupElement.appendChild(templateElement);

                ArrayList<TemplateEntry> entries = template.getEntries();
                for (TemplateEntry templateEntry : entries) {
                    Element templateEntryElement = createTemplateEntry(templateEntry, doc);
                    templateElement.appendChild(templateEntryElement);
                }
            }
        }
    }

    private static void createTransactions(ArrayList<Transaction> transactions, Document doc, Element root) {

        Element transactionSectionElement = doc.createElement(XmlTags.TransactionSection);
        root.appendChild(transactionSectionElement);

        for (Transaction transaction : transactions) {
            Element transactionElement = createTransaction(transaction, doc);
            transactionSectionElement.appendChild(transactionElement);

            ArrayList<TransactionEntry> entries = transaction.getEntries();
            for (TransactionEntry transactionEntry : entries) {
                Element transactionEntryElement = createTransactionEntry(transactionEntry, doc);
                transactionElement.appendChild(transactionEntryElement);
            }
        }
    }

    private static void createIdHistoryCollection(ArrayList<UUID> entityIds, Document doc, Element root, String sectionName) {
        Element sectionElement = doc.createElement(sectionName);
        root.appendChild(sectionElement);
        for (UUID entityId : entityIds) {
            Element entityIdElement = doc.createElement(XmlTags.Entity);
            entityIdElement.setAttributeNode(createAttribute(doc, XmlTags.EntityId, UuidHelper.convertToString(entityId)));
            sectionElement.appendChild(entityIdElement);
        }
    }

    private static void createSystemConfig(Ledger ledger, Document doc, Element root) {
        Element systemConfigElement = doc.createElement(XmlTags.SystemConfig);
        root.appendChild(systemConfigElement);

        Element mainCurrencyElement = doc.createElement(XmlTags.MainCurrency);
        mainCurrencyElement.setTextContent(ledger.getSystemConfig().getMainCurrencyIsoCode());
        systemConfigElement.appendChild(mainCurrencyElement);

        Element minDateTimeElement = doc.createElement(XmlTags.MinDateTime);
        minDateTimeElement.setTextContent(DateTimeHelper.convertToString(ledger.getSystemConfig().getMinDateTime()));
        systemConfigElement.appendChild(minDateTimeElement);

        Element maxDateTimeElement = doc.createElement(XmlTags.MaxDateTime);
        maxDateTimeElement.setTextContent(DateTimeHelper.convertToString(ledger.getSystemConfig().getMaxDateTime()));
        systemConfigElement.appendChild(maxDateTimeElement);
    }

    private static void createUserConfig(Ledger ledger, Document doc, Element root) {
        Element userConfigElement = doc.createElement(XmlTags.UserConfig);
        root.appendChild(userConfigElement);
    }

    //endregion

    //region serialize elements

    private static Element createReferenceElement(
            IReferenceEntity entity,
            Document doc,
            String elementName) {
        Element element = doc.createElement(elementName);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(entity.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Name, entity.getName()));
        if (!StringHelper.isStringNullOrEmpty(entity.getDescription())) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Description, entity.getDescription()));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, entity.getTimeStamp()));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(entity.getOrder())));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(entity.getIsFavorite())));
        return element;
    }

    private static Element createCurrency(
            Currency currency,
            Document doc) {
        Element element = doc.createElement(XmlTags.Currency);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(currency.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsoCode, currency.getIsoCode()));
        element.setAttributeNode(createAttribute(doc, XmlTags.Symbol, currency.getSymbol()));
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, currency.getTimeStamp()));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(currency.getOrder())));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(currency.getIsFavorite())));
        return element;
    }

    private static Element createRateCurrency(
            CurrencyRate currencyRate,
            Document doc) {
        Element element = doc.createElement(XmlTags.CurrencyRate);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(currencyRate.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Rate, BigDecimalHelper.convertToString(currencyRate.getRate())));
        if (!StringHelper.isStringNullOrEmpty(currencyRate.getComment())) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Comment, currencyRate.getComment()));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, currencyRate.getTimeStamp()));
        element.setAttributeNode(createAttribute(doc, XmlTags.Date, DateHelper.convertToString(currencyRate.getDate())));
        return element;
    }

    private static Element createAccount(
            Account account,
            Document doc) {
        Element element = doc.createElement(XmlTags.Account);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(account.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Name, account.getName()));
        if (!StringHelper.isStringNullOrEmpty(account.getDescription())) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Description, account.getDescription()));
        }
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, account.getTimeStamp()));
        element.setAttributeNode(createAttribute(doc, XmlTags.Order, String.valueOf(account.getOrder())));
        element.setAttributeNode(createAttribute(doc, XmlTags.IsFavorite, String.valueOf(account.getIsFavorite())));
        element.setAttributeNode(createAttribute(doc, XmlTags.CurrencyId, UuidHelper.convertToString(account.getCurrency().getId())));

        if (account.getCorrespondent() != null) {
            element.setAttributeNode(createAttribute(doc, XmlTags.CorrespondentId, UuidHelper.convertToString(account.getCorrespondent().getId())));
        }
        if (account.getCategory() != null) {
            element.setAttributeNode(createAttribute(doc, XmlTags.CategoryId, UuidHelper.convertToString(account.getCategory().getId())));
        }
        if (account.getProject() != null) {
            element.setAttributeNode(createAttribute(doc, XmlTags.ProjectId, UuidHelper.convertToString(account.getProject().getId())));
        }
        return element;
    }

    private static Element createTransaction(
            Transaction transaction,
            Document doc) {
        Element element = doc.createElement(XmlTags.Transaction);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(transaction.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.TimeStamp, transaction.getTimeStamp()));
        element.setAttributeNode(createAttribute(doc, XmlTags.DateTime, DateTimeHelper.convertToString(transaction.getDateTime())));
        element.setAttributeNode(createAttribute(doc, XmlTags.State, String.valueOf(transaction.getState().toString())));
        if (!StringHelper.isStringNullOrEmpty(transaction.getComment())) {
            element.setAttributeNode(createAttribute(doc, XmlTags.Comment, transaction.getComment()));
        }
        return element;
    }

    private static Element createTransactionEntry(TransactionEntry entry, Document doc) {
        Element element = doc.createElement(XmlTags.TransactionEntry);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(entry.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.AccountId, UuidHelper.convertToString(entry.getAccount().getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Amount, BigDecimalHelper.convertToString(entry.getAmount())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Rate, BigDecimalHelper.convertToString(entry.getRate())));
        return element;
    }

    private static Element createTemplateEntry(TemplateEntry entry, Document doc) {
        Element element = doc.createElement(XmlTags.TemplateEntry);
        element.setAttributeNode(createAttribute(doc, XmlTags.Id, UuidHelper.convertToString(entry.getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.AccountId, UuidHelper.convertToString(entry.getAccount().getId())));
        element.setAttributeNode(createAttribute(doc, XmlTags.Amount, BigDecimalHelper.convertToString(entry.getAmount())));
        return element;
    }

    private static Attr createAttribute(Document doc, String name, String value) {
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        return attr;
    }

    //endregion

    //region deserialize

    public Ledger deserialize(Document doc) {
        Ledger ledger = new Ledger();

        deserializeReferenceEntitySection(
                doc, ledger.getCorrespondentGroups(), ledger.getCorrespondents(),
                CorrespondentGroup.class, Correspondent.class, XmlTags.CorrespondentUnit);
        deserializeReferenceEntitySection(
                doc, ledger.getCategoryGroups(), ledger.getCategories(),
                CategoryGroup.class, Category.class, XmlTags.CategoryUnit);
        deserializeReferenceEntitySection(
                doc, ledger.getProjectGroups(), ledger.getProjects(),
                ProjectGroup.class, Project.class, XmlTags.ProjectUnit);

        deserializeCurrencySection(ledger, doc);

        deserializeAccountSection(ledger, doc);

        deserializeTemplateSection(ledger, doc);

        deserializeTransactionSection(ledger, doc);

        deserializeHistorySection(ledger.getAddedIds(), doc, XmlTags.AddedIds);
        deserializeHistorySection(ledger.getUpdatedIds(), doc, XmlTags.UpdatedIds);
        deserializeHistorySection(ledger.getDeletedIds(), doc, XmlTags.DeletedIds);

        deserializeSystemConfig(ledger.getSystemConfig(), doc);
        deserializeUserConfig(ledger.getUserConfig(), doc);

        return ledger;
    }

    //endregion

    //region deserialize Sections

    private static <TChild extends IReferenceChildEntity<TParent>, TParent extends IReferenceParentEntity<TChild>>
    void deserializeReferenceEntitySection(
            Document doc, ArrayList<TParent> parents, ArrayList<TChild> children,
            Class<TParent> parentType, Class<TChild> childType,
            XmlTags.ReferenceUnit unit) {

        Element sectionElement = getSectionElementByTagName(doc, unit.Section);
        NodeList groupElements = sectionElement.getElementsByTagName(unit.Group);
        for (int i = 0; i < groupElements.getLength(); i++) {
            Element groupElement = (Element) groupElements.item(i);
            TParent group = createReferenceEntity(groupElement, parents, parentType);
            NodeList entityElements = groupElement.getElementsByTagName(unit.Entity);
            for (int j = 0; j < entityElements.getLength(); j++) {
                Element entityElement = (Element) entityElements.item(j);
                TChild entity = createReferenceEntity(entityElement, children, childType);
                group.getChildren().add(entity);
                entity.setParent(group);
            }
        }
    }

    private static void deserializeCurrencySection(Ledger ledger, Document doc) {

        Element currencySectionElement = getSectionElementByTagName(doc, XmlTags.CurrencySection);
        NodeList currencyElements = currencySectionElement.getElementsByTagName(XmlTags.Currency);
        for (int i = 0; i < currencyElements.getLength(); i++) {
            Element currencyElement = (Element) currencyElements.item(i);
            Currency currency = createCurrency(currencyElement, ledger);
            NodeList currencyRateElements = currencyElement.getElementsByTagName(XmlTags.CurrencyRate);
            for (int j = 0; j < currencyRateElements.getLength(); j++) {
                Element currencyRateElement = (Element) currencyRateElements.item(j);
                CurrencyRate currencyRate = createCurrencyRate(currencyRateElement, ledger);
                currencyRate.setCurrency(currency);
                currency.getRates().add(currencyRate);
            }
        }
    }


    private static void deserializeAccountSection(Ledger ledger, Document doc) {

        Element accountSectionElement = getSectionElementByTagName(doc, XmlTags.AccountSection);
        NodeList accountGroupElements = accountSectionElement.getElementsByTagName(XmlTags.AccountGroup);
        for (int i = 0; i < accountGroupElements.getLength(); i++) {
            Element accountGroupElement = (Element) accountGroupElements.item(i);
            AccountGroup accountGroup = createReferenceEntity(accountGroupElement, ledger.getAccountGroups(), AccountGroup.class);
            NodeList accountSubGroupElements = accountGroupElement.getElementsByTagName(XmlTags.AccountSubGroup);
            for (int j = 0; j < accountSubGroupElements.getLength(); j++) {
                Element accountSubGroupElement = (Element) accountSubGroupElements.item(j);
                AccountSubGroup accountSubGroup = createReferenceEntity(accountSubGroupElement, ledger.getAccountSubGroups(), AccountSubGroup.class);
                accountGroup.getChildren().add(accountSubGroup);
                accountSubGroup.setParent(accountGroup);
                NodeList accountElements = accountSubGroupElement.getElementsByTagName(XmlTags.Account);
                for (int k = 0; k < accountElements.getLength(); k++) {
                    Element accountElement = (Element) accountElements.item(k);
                    Account account = createAccount(accountElement, ledger);
                    accountSubGroup.getChildren().add(account);
                    account.setParent(accountSubGroup);
                }
            }
        }
    }

    private static void deserializeTemplateSection(Ledger ledger, Document doc) {

        Element templateSectionElement = getSectionElementByTagName(doc, XmlTags.TemplateSection);
        NodeList templateGroupElements = templateSectionElement.getElementsByTagName(XmlTags.TemplateGroup);
        for (int i = 0; i < templateGroupElements.getLength(); i++) {
            Element templateGroupElement = (Element) templateGroupElements.item(i);
            TemplateGroup templateGroup = createReferenceEntity(templateGroupElement, ledger.getTemplateGroups(), TemplateGroup.class);
            NodeList templateElements = templateGroupElement.getElementsByTagName(XmlTags.Template);
            for (int j = 0; j < templateElements.getLength(); j++) {
                Element templateElement = (Element) templateElements.item(j);
                Template template = createReferenceEntity(templateElement, ledger.getTemplates(), Template.class);
                templateGroup.getChildren().add(template);
                template.setParent(templateGroup);
                NodeList templateEntryElements = templateElement.getElementsByTagName(XmlTags.TemplateEntry);
                for (int k = 0; k < templateEntryElements.getLength(); k++) {
                    Element templateEntryElement = (Element) templateEntryElements.item(k);
                    TemplateEntry templateEntry = createTemplateEntry(templateEntryElement, ledger);
                    templateEntry.setTemplate(template);
                    template.getEntries().add(templateEntry);
                }
            }
        }
    }

    private static void deserializeTransactionSection(Ledger ledger, Document doc) {

        Element transactionSectionElement = getSectionElementByTagName(doc, XmlTags.TransactionSection);
        NodeList transactionElements = transactionSectionElement.getElementsByTagName(XmlTags.Transaction);
        for (int i = 0; i < transactionElements.getLength(); i++) {
            Element transactionElement = (Element) transactionElements.item(i);
            Transaction transaction = createTransaction(transactionElement, ledger);
            NodeList transactionEntryElements = transactionElement.getElementsByTagName(XmlTags.TransactionEntry);
            for (int j = 0; j < transactionEntryElements.getLength(); j++) {
                Element transactionEntryElement = (Element) transactionEntryElements.item(j);
                TransactionEntry transactionEntry = createTransactionEntry(ledger, transactionEntryElement);
                transactionEntry.setTransaction(transaction);
                transaction.getEntries().add(transactionEntry);
            }
        }
    }

    private static void deserializeHistorySection(ArrayList<UUID> entityIds, Document doc, String sectionName) {

        Element historySectionElement = getSectionElementByTagName(doc, sectionName);
        NodeList entityElements = historySectionElement.getElementsByTagName(XmlTags.Entity);
        for (int i = 0; i < entityElements.getLength(); i++) {
            Element entityElement = (Element) entityElements.item(i);
            UUID entityId = UuidHelper.convertFromString(entityElement.getAttribute(XmlTags.EntityId));
            entityIds.add(entityId);
        }
    }

    private static void deserializeSystemConfig(SystemConfig systemConfig, Document doc) {

        Element systemConfiElement = getSectionElementByTagName(doc, XmlTags.SystemConfig);
        systemConfig.setMainCurrencyIsoCode(getElementByTagName(systemConfiElement, XmlTags.MainCurrency).getTextContent());
        systemConfig.setMaxDateTime(DateTimeHelper.convertFromString(getElementByTagName(systemConfiElement, XmlTags.MaxDateTime).getTextContent()));
        systemConfig.setMinDateTime(DateTimeHelper.convertFromString(getElementByTagName(systemConfiElement, XmlTags.MinDateTime).getTextContent()));
    }

    private static void deserializeUserConfig(UserConfig userConfig, Document doc) {

        Element systemConfiElement = getSectionElementByTagName(doc, XmlTags.SystemConfig);
    }

    //endregion

    //region deserialize Elements

    private static <T extends IReferenceEntity> T createReferenceEntity(Element element, List<T> list, Class<T> type) {
        T entity = GenericHelper.createGenericType(type);
        restoreId(entity, element);
        restoreTimeStamp(entity, element);
        restoreNameAndDescription(entity, element);
        restoreOrder(entity, element);
        restoreIsFavorite(entity, element);
        list.add(entity);
        return entity;
    }

    private static Currency createCurrency(Element currencyElement, Ledger ledger) {
        Currency currency = new Currency();
        restoreId(currency, currencyElement);
        restoreTimeStamp(currency, currencyElement);
        restoreOrder(currency, currencyElement);
        restoreIsFavorite(currency, currencyElement);
        currency.setIsoCode(getString(currencyElement, XmlTags.IsoCode));
        currency.setSymbol(getString(currencyElement, XmlTags.Symbol));
        ledger.getCurrencies().add(currency);
        return currency;
    }

    private static CurrencyRate createCurrencyRate(Element currencyRateElement, Ledger ledger) {
        CurrencyRate currencyRate = new CurrencyRate();
        restoreId(currencyRate, currencyRateElement);
        restoreTimeStamp(currencyRate, currencyRateElement);
        currencyRate.setComment(getString(currencyRateElement, XmlTags.Comment));
        currencyRate.setDate(getDate(currencyRateElement));
        currencyRate.setRate(getRate(currencyRateElement));
        ledger.getCurrencyRates().add(currencyRate);
        return currencyRate;
    }

    private static Account createAccount(Element accountElement, Ledger ledger) {
        Account account = createReferenceEntity(accountElement, ledger.getAccounts(), Account.class);
        account.setCurrency(findRequiredEntity(ledger.getCurrencies(), accountElement, XmlTags.CurrencyId));
        account.setCorrespondent(findEntity(ledger.getCorrespondents(), accountElement, XmlTags.CorrespondentId));
        account.setCategory(findEntity(ledger.getCategories(), accountElement, XmlTags.CategoryId));
        account.setProject(findEntity(ledger.getProjects(), accountElement, XmlTags.ProjectId));
        return account;
    }

    private static TemplateEntry createTemplateEntry(Element templateEntryElement, Ledger ledger) {
        TemplateEntry templateEntry = new TemplateEntry();
        restoreId(templateEntry, templateEntryElement);
        templateEntry.setAmount(getAmount(templateEntryElement));
        templateEntry.setAccount(findRequiredEntity(ledger.getAccounts(), templateEntryElement, XmlTags.AccountId));
        ledger.getTemplateEntries().add(templateEntry);
        return templateEntry;
    }

    private static Transaction createTransaction(Element transactionElement, Ledger ledger) {
        Transaction transaction = new Transaction();
        restoreId(transaction, transactionElement);
        restoreTimeStamp(transaction, transactionElement);
        transaction.setDateTime(getDateTime(transactionElement));
        transaction.setState(getTransactionState(transactionElement));
        transaction.setComment(getString(transactionElement, XmlTags.Comment));
        ledger.getTransactions().add(transaction);
        return transaction;
    }

    private static TransactionEntry createTransactionEntry(Ledger ledger, Element transactionEntryElement) {
        TransactionEntry transactionEntry = new TransactionEntry();
        restoreId(transactionEntry, transactionEntryElement);
        transactionEntry.setAccount(findRequiredEntity(ledger.getAccounts(), transactionEntryElement, XmlTags.AccountId));
        transactionEntry.setAmount(getAmount(transactionEntryElement));
        transactionEntry.setRate(getRate(transactionEntryElement));
        ledger.getTransactionEntries().add(transactionEntry);
        return transactionEntry;
    }

    //endregion

    //region find deserialized elements

    private static <T extends IEntity> T findEntity(ArrayList<T> list, Element element, String attributeName) {
        String value = element.getAttribute(attributeName);
        if (StringHelper.isStringNullOrEmpty(value)) {
            return null;
        }
        UUID uuid = UuidHelper.convertFromString(value);
        return list.stream().filter(t -> t.getId().equals(uuid)).findFirst().orElse(null);
    }

    private static <T extends IEntity> T findRequiredEntity(ArrayList<T> list, Element element, String attributeName) {
        String value = element.getAttribute(attributeName);
        if (StringHelper.isStringNullOrEmpty(value)) {
            throw new NullPointerException("Absent " + attributeName);
        }
        UUID uuid = UuidHelper.convertFromString(value);
        return list.stream().filter(t1 -> t1.getId().equals(uuid)).findFirst().orElseThrow();
    }
    //endregion

    //region get attribute values

    private static void restoreId(IEntity entity, Element element) {
        entity.setId(UuidHelper.convertFromString(element.getAttribute(XmlTags.Id)));
    }

    private static void restoreTimeStamp(ITrackedEntity entity, Element element) {
        entity.setTimeStamp(element.getAttribute(XmlTags.TimeStamp));
    }

    private static void restoreNameAndDescription(INamedEntity entity, Element element) {
        entity.setName(element.getAttribute(XmlTags.Name));
        entity.setDescription(element.getAttribute(XmlTags.Description));
    }

    private static void restoreOrder(IOrderedEntity entity, Element element) {
        entity.setOrder(Integer.parseInt(element.getAttribute(XmlTags.Order)));
    }

    private static void restoreIsFavorite(IFavoriteEntity entity, Element element) {
        entity.setIsFavorite(Boolean.parseBoolean(element.getAttribute(XmlTags.IsFavorite)));
    }

    private static BigDecimal getRate(Element element) {
        return BigDecimalHelper.convertFromString(element.getAttribute(XmlTags.Rate));
    }

    private static BigDecimal getAmount(Element element) {
        return BigDecimalHelper.convertFromString(element.getAttribute(XmlTags.Amount));
    }

    private static LocalDate getDate(Element element) {
        return DateHelper.convertFromString(element.getAttribute(XmlTags.Date));
    }

    private static LocalDateTime getDateTime(Element element) {
        return DateTimeHelper.convertFromString(element.getAttribute(XmlTags.DateTime));
    }

    private static TransactionState getTransactionState(Element element) {
        return TransactionState.valueOf(element.getAttribute(XmlTags.State));
    }

    private static String getString(Element element, String xmlTag) {
        return element.getAttribute(xmlTag);
    }

    //endregion

    //region deserialization aux functions

    private static Element getSectionElementByTagName(Document doc, String tagName) {
        NodeList nodeList = doc.getElementsByTagName(tagName);
        Node node = nodeList.item(0);
        return (Element) node;
    }

    private static Element getElementByTagName(Element element, String tagName) {
        NodeList nodeList = element.getElementsByTagName(tagName);
        Node node = nodeList.item(0);
        return (Element) node;
    }

    //endregion
}

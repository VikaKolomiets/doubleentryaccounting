package doubleentryaccounting.dataaccess.inmemorydb.comparison;

import doubleentryaccounting.dataaccess.inmemorydb.*;

import java.util.*;

public interface ILedgerComparer {

    ArrayList<String> Compare(Ledger first, Ledger second);
}

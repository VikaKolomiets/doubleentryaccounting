package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.Currency;
import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;
import java.util.stream.*;

public class MemoryDbCurrencyDataAccess
        extends MemoryDbEntityDataAccess<Currency>
        implements ICurrencyDataAccess {
    public MemoryDbCurrencyDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    public ArrayList<Currency> getByIsoCode(String isoCode) {
        return this.getEntitiesStream().filter(g -> g.getIsoCode().equalsIgnoreCase(isoCode)).collect(Collectors.toCollection(ArrayList::new));
    }

    public int getMaxOrder() {
        Optional<Currency> currency = this.getEntitiesStream().max(Comparator.comparing(IOrderedEntity::getOrder));
        if(currency.isPresent()){
            return currency.get().getOrder();
        }
        return 0;
    }

    public int getCount() {
        return this.getEntities().size();
    }

    public ArrayList<Currency> getList() {
        return this.getEntities();
    }

    protected ArrayList<Currency> getEntities() {
        return this.getLedger().getCurrencies();
    }

    public Class<Currency> getType() {
        return Currency.class;
    }
}

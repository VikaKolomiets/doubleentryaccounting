package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;
import java.util.stream.*;

public class MemoryDbAccountDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<Account, AccountSubGroup>
        implements IAccountDataAccess {
    public MemoryDbAccountDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    public void loadCurrency(Account account) {
        //Only for Relational Db
    }

    public ArrayList<Account> getAccountsByCorrespondent(Correspondent correspondent) {
        return this.getEntitiesStream().filter(e -> e.getCorrespondent().getId() == correspondent.getId()).collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<Account> getAccountsByCategory(Category category) {
        return this.getEntitiesStream().filter(e -> e.getCategory().getId() == category.getId()).collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<Account> getAccountsByProject(Project project) {
        return this.getEntitiesStream().filter(e -> e.getProject().getId() == project.getId()).collect(Collectors.toCollection(ArrayList::new));
    }

    protected ArrayList<Account> getEntities() {
        return this.getLedger().getAccounts();
    }

    public Class<Account> getType() {
        return Account.class;
    }
}

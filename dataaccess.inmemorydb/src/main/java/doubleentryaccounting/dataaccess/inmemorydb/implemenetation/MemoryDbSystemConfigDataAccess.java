package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;

import java.time.*;

public class MemoryDbSystemConfigDataAccess
        implements ISystemConfigDataAccess {
    private final ILedgerFactory factory;

    public MemoryDbSystemConfigDataAccess(ILedgerFactory factory) {
        this.factory = factory;
    }

    public String getMainCurrencyIsoCode() {
        return factory.get().getSystemConfig().getMainCurrencyIsoCode();
    }

    public LocalDateTime getMinDateTime() {
        return factory.get().getSystemConfig().getMinDateTime();
    }

    public LocalDateTime getMaxDateTime() {
        return factory.get().getSystemConfig().getMaxDateTime();
    }
}

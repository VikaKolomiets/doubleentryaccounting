package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCorrespondentGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<CorrespondentGroup>
        implements ICorrespondentGroupDataAccess {
    public MemoryDbCorrespondentGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<CorrespondentGroup> getEntities() {
        return this.getLedger().getCorrespondentGroups();
    }

    public Class<CorrespondentGroup> getType() {
        return CorrespondentGroup.class;
    }
}

package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbAccountSubGroupDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<AccountSubGroup, AccountGroup>
        implements IAccountSubGroupDataAccess {
    public MemoryDbAccountSubGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    public void loadChildren(AccountSubGroup entity) {
        //Only for Relational Db
    }

    protected ArrayList<AccountSubGroup> getEntities() {
        return this.getLedger().getAccountSubGroups();
    }

    public Class<AccountSubGroup> getType() {
        return AccountSubGroup.class;
    }
}

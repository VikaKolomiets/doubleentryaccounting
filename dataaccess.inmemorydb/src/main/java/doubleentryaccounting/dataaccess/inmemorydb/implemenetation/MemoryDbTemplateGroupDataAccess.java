package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbTemplateGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<TemplateGroup>
        implements ITemplateGroupDataAccess {
    public MemoryDbTemplateGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<TemplateGroup> getEntities() {
        return this.getLedger().getTemplateGroups();
    }

    public Class<TemplateGroup> getType() {
        return TemplateGroup.class;
    }
}

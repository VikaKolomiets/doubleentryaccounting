package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbProjectDataAccess extends MemoryDbReferenceChildEntityDataAccess<Project, ProjectGroup> implements IProjectDataAccess {
    public MemoryDbProjectDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Project> getEntities() {
        return this.getLedger().getProjects();
    }

    public Class<Project> getType() {
        return Project.class;
    }
}

package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCurrencyRateDataAccess
        extends MemoryDbEntityDataAccess<CurrencyRate>
        implements ICurrencyRateDataAccess {
    public MemoryDbCurrencyRateDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<CurrencyRate> getEntities() {
        return this.getLedger().getCurrencyRates();
    }

    public Class<CurrencyRate> getType() {
        return CurrencyRate.class;
    }
}

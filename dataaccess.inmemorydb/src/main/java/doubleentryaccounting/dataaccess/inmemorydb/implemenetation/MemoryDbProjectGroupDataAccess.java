package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbProjectGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<ProjectGroup>
        implements IProjectGroupDataAccess {
    public MemoryDbProjectGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<ProjectGroup> getEntities()  {
        return this.getLedger().getProjectGroups();
    }

    public Class<ProjectGroup> getType() {
        return ProjectGroup.class;
    }
}

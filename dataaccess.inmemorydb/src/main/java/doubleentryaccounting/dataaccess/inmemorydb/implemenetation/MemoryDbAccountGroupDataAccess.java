package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbAccountGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<AccountGroup>
        implements IAccountGroupDataAccess {

    public MemoryDbAccountGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<AccountGroup> getEntities() {
         return this.getLedger().getAccountGroups();
    }

    public Class<AccountGroup> getType() {
        return AccountGroup.class;
    }
}

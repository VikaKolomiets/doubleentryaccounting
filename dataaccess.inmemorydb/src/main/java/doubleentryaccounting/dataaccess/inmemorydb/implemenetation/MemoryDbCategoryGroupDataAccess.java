package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCategoryGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<CategoryGroup>
        implements ICategoryGroupDataAccess {

    public MemoryDbCategoryGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<CategoryGroup> getEntities() {
        return this.getLedger().getCategoryGroups();
    }

    public Class<CategoryGroup> getType() {
        return CategoryGroup.class;
    }
}

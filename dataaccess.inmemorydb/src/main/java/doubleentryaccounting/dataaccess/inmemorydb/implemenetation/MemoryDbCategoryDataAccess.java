package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCategoryDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<Category, CategoryGroup>
        implements ICategoryDataAccess {
    public MemoryDbCategoryDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Category> getEntities() {
        return this.getLedger().getCategories();
    }

    public Class<Category> getType() {
        return Category.class;
    }
}

package doubleentryaccounting.dataaccess.inmemorydb.implemenetation;

import doubleentryaccounting.common.dataaccess.*;
import doubleentryaccounting.common.models.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;
import doubleentryaccounting.dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCorrespondentDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<Correspondent, CorrespondentGroup>
        implements ICorrespondentDataAccess {
    public MemoryDbCorrespondentDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Correspondent> getEntities() {
        return this.getLedger().getCorrespondents();
    }

    public Class<Correspondent> getType() {
        return Correspondent.class;
    }
}

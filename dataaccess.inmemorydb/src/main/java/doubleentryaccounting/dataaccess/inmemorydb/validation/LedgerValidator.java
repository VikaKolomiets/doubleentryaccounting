package doubleentryaccounting.dataaccess.inmemorydb.validation;

import doubleentryaccounting.common.models.interfaces.*;
import doubleentryaccounting.common.utils.validation.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;

import java.util.*;

public class LedgerValidator implements ILedgerValidator{

    public ArrayList<ValidationError> Validate(Ledger ledger) {
        ArrayList<ValidationError> errors = new ArrayList<>();

        validateMissingEntityId(ledger, errors);
        validateDuplicatedEntityId(ledger, errors);

        //Existing and > 0 Name for all named element
        //Existing and Correct TimeStamp for all Tracked elements
        //Existing of children collection
        //Existing of children in common collection
        //Existing of parents
        //Existing of parents in common collection
        //Correct Order of all ordered elements
        //Unique Name in collection
        //DateTime in the range
        //Correct rate > 0
        //Correct rate for home  currency in transaction
        //correct transactions


        return errors;
    }

    private static void validateMissingEntityId(Ledger ledger, ArrayList<ValidationError> errors) {
        for (ArrayList<? extends IEntity> entities : ledger.getAllLists()) {
            for (IEntity entity : entities) {
                if(entity.getId() == null){
                    errors.add(new ValidationError(ValidationErrorType.MissedId, entity.getClass(), null));
                }
            }
        }
    }

    private static void validateDuplicatedEntityId(Ledger ledger, ArrayList<ValidationError> errors) {
        HashSet<UUID> set = new HashSet<>();
        for (ArrayList<? extends IEntity> entities : ledger.getAllLists()) {
            for (IEntity entity : entities) {
                if(set.contains(entity.getId())){
                    errors.add(new ValidationError(ValidationErrorType.DuplicatedId, entity.getClass(), entity.getId()));
                }
                set.add(entity.getId());
            }
        }
    }
}

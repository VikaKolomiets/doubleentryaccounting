package doubleentryaccounting.dataaccess.inmemorydb.validation;

import doubleentryaccounting.common.utils.validation.*;
import doubleentryaccounting.dataaccess.inmemorydb.*;

import java.util.*;

public interface ILedgerValidator {
    ArrayList<ValidationError> Validate(Ledger ledger);
}
